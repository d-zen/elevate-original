import { Component, OnInit, ViewChild, OnChanges, OnDestroy } from '@angular/core';
import {
  Nav,
  Platform,
  ModalController,
  ToastController,
  AlertController,
  Events,
  MenuController
} from '@ionic/angular';
import { Storage } from '@ionic/storage';

import * as firebase from 'firebase';
import { AuthService } from '../services/auth.service';

import { environment as ENV } from '../environments/environment';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { HttpClient } from '@angular/common/http';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { filter } from 'rxjs/operators';

const config = {
  apiKey: 'AIzaSyCUzInLJGc5HCGxc7Gr6-4wnVUL5OlAJTY',
  authDomain: 'elevather-959ba.firebaseapp.com',
  databaseURL: 'https://elevather-959ba.firebaseio.com',
  projectId: 'elevather-959ba',
  storageBucket: 'elevather-959ba.appspot.com',
  messagingSenderId: '791140748667'
};

declare var cordova: any;

@Component({
  templateUrl: 'app.html'
})
export class MyAppComponent implements OnInit {

  @ViewChild(Nav, {static: false}) nav: Nav;

  rootPage: any;
  id: any;
  name: any;
  img: any;
  count: any;
  options: any;
  subscription: any;

  showMenu: boolean;


  version: any;

  pages: Array<{ title: string; component: any }>;

  constructor(
    public platform: Platform,
    public splashScreen: SplashScreen,
    public storage: Storage,
    public http: HttpClient,
    public events: Events,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public menuCtrl: MenuController,
    private auth: AuthService,
    private appVersion: AppVersion
  ) {

    this.platform.ready().then(() => {
      this.appVersion.getVersionNumber().then((res) => {
        if (this.platform.is('ios')) {
          this.http
            .get(ENV.BASE_URL + 'api/appversion?device=ios')
            .subscribe(result => {
              const resultServe = JSON.parse(result['_body']);
              // const resultServe = JSON.parse(result.['_body']);
              console.log('version = ', res);
              console.log('server version = ', resultServe.version );
              if (resultServe.version === res) {
                console.log('load', true);
                this.initializeApp();
                this.showMenu = true;
              } else {
                this.splashScreen.hide();
                console.log('load', false);
                this.auth.logout();
                this.nav.setRoot('HomePage');
                this.updateBox();
                this.showMenu = false;
              }
            });
          } else {
            this.http
            .get(ENV.BASE_URL + 'api/appversion?device=android')
            .subscribe(result => {
              const resultServe = JSON.parse(result['_body']);
              if (resultServe.version === res) {
                this.initializeApp();
                this.showMenu = true;
              } else {
                this.splashScreen.hide();
                this.auth.logout();
                this.nav.setRoot('HomePage');
                this.updateBox();
                this.showMenu = false;
              }
            });
        }
      }, (err) => {
        console.log(err);
      });
    });
  }

  initializeApp() {
    this.splashScreen.hide();
    this.auth.setLoggedIn(false);
    this.storage
      .get('token')
      .then(token => {
        if (token !== null) {
          return this.auth.setToken(token);
        }
        return Promise.reject(new Error('Token not set'));
      })
      .catch(err => {
        console.log(err);
        this.nav.setRoot('HomePage');
        this.nav.popToRoot();
      });

    this.auth.isLoggedIn.subscribe(state => {
      console.log('state:', state);
      if (!state) {
        this.rootPage = 'HomePage';
        this.showMenu = false;
      } else {
        this.rootPage = 'ProfilesPage';
        this.showMenu = true;
      }
    });

    this.auth.token.pipe(
      filter(token => !!token)
    )
    .subscribe(token => {
      console.log('token', token);
      this.auth.exchangeToken(token).subscribe(user => {
        this.auth.setUser(user);
        this.auth.setLoggedIn(true);
      });
    });

    this.auth.me().subscribe(user => {
      this.id = user.id;
      this.img = user.img;
      this.name = user.firstname;
      console.log(user)
    });


    this.auth.isCommunityAdmin.subscribe(isAdmin => {
      const admin = isAdmin[0];
      if (!admin) {
        this.pages = [
          { title: 'Suggested For You', component: 'ProfilesPage' },
          { title: 'Community', component: 'QnaPage' },
          { title: 'Connections', component: 'ConnectionsPage' },
          { title: 'Messages', component: 'chatListPage' },
          { title: 'Events', component: 'EventsPage' },
          { title: 'Resources', component: 'CareerAdvicePage' }
        ];
        return;
      } else {
        this.pages = [
          { title: 'Suggested For You', component: 'ProfilesPage' },
          { title: 'Community', component: 'QnaPage' },
          { title: 'Connections', component: 'ConnectionsPage' },
          { title: 'Messages', component: 'chatListPage' },
          { title: 'Events', component: 'EventsPage' },
          { title: 'Resources', component: 'CareerAdvicePage' },
          { title: 'Manage Community', component: 'ManageGroupsPage' }
        ];
      }
    });

    firebase.initializeApp(config);
    const that = this;

    // window.plugins.OneSignal.startInit(
    //   '7a29a388-d406-4c4e-bcc5-ef8b585f9d3d',
    //   '761814483810'
    // )
    //   .handleNotificationOpened( jsonData => {
    //     that.nav.setRoot('chatListPage');
    //   })
    //   .endInit();

    // window.plugins.OneSignal.startInit(
    //   '7a29a388-d406-4c4e-bcc5-ef8b585f9d3d',
    //   '761814483810'
    // )
    //   .inFocusDisplaying(
    //     window.plugins.OneSignal.OSInFocusDisplayOption.None
    //   )
    //   .endInit();

    // this.events.subscribe('notification', () => {
    //   window.plugins.OneSignal.sendTag('userId', this.id);
    // });
  }

  updateBox() {
    this.alertCtrl
      .create({
        title: 'UPDATE!',
        subTitle:
          'We have updated the app, you will need to download the latest version before you can continue',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'UPDATE',
            role: 'update',
            handler: () => {
              if (this.platform.is('ios')) {
                window.open(
                  'https://itunes.apple.com/app/id1441360188',
                  '_system',
                  'location=yes'
                );
              } else {
                window.open(
                  'https://play.google.com/store/apps/details?id=com.elevather.designs',
                  '_system',
                  'location=yes'
                );
              }
            }
          }
        ]
      })
      .present();
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  openModal(str) {
    this.modalCtrl.create('AboutUsPage', { data: str }).present();
  }

  openProfile() {
    this.menuCtrl.close();
    this.nav.setRoot('ViewProfilePage');
  }

  ngOnInit() {
    this.events.subscribe('update_count', ct => {
      this.count = ct;
      this.count >= 1 ? cordova.plugins.notification.badge.set(this.count) : cordova.plugins.notification.badge.clear();
    });
  }

}
