import { BrowserModule } from '@angular/platform-browser';

import { DOCUMENT } from '@angular/common';
import { Title } from '@angular/platform-browser';

import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from '@ionic/angular';
import { Oauth, OauthCordova } from 'ionic-cordova-oauth';

import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { InAppBrowser } from '@inpm install @ionic/angularonic-native/in-app-browser/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { AppAvailability } from '@ionic-native/app-availability/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Badge } from '@ionic-native/badge/ngx';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { IonicStorageModule } from '@ionic/storage';
import { DataProvider } from '../services/data/data';
import { SearchProvider } from '../services/search/search';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { AuthService } from '../services/auth.service';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { MyAppComponent } from './app.component';
import { AgmCoreModule } from '@agm/core';


@NgModule({
  declarations: [
    MyAppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyAppComponent, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: true,
      platforms: {
        ios: {
          backButtonText: ''
        }
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAJbhUtaf0mAtjWQF10W8YiepULbyehLpc',
      libraries: ['places']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyAppComponent
  ],
  providers: [
    AuthService,
    FileOpener,
    File,
    StatusBar,
    FileTransferObject,
    SplashScreen,
    Crop,
    ImagePicker,
    FileTransfer,
    Geolocation,
    InAppBrowser,
    AppAvailability,
    NativeGeocoder,
    NativePageTransitions,
    Network,
    Badge,
    AppVersion,
    {provide: Oauth, useClass: OauthCordova},
    // { provide: ErrorHandler, useClass: IonicErrorHandler },
    DataProvider,
    SearchProvider,
  ]
})
export class AppModule {
}
