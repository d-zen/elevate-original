import { NgModule } from '@angular/core';
import { AboutUsPage } from './about-us';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [AboutUsPage],
  imports: [IonicPageModule.forChild(AboutUsPage)],
  exports : [AboutUsPage]
})
export class AboutUsPageModule { }
