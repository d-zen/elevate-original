import { Component } from '@angular/core';
import { NavController, IonicPage, NavParams, ViewController, ToastController, LoadingController, ModalController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { environment as ENV } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-about-us',
  templateUrl: 'about-us.html',
})
export class AboutUsPage {

	about: boolean;
	contact: boolean;
	privacy: boolean;
	faq: boolean;
	cookies: boolean;
  picture: boolean;
  add: boolean;
  terms: boolean;
  del_data: boolean;
  guide: boolean;
  id: any;
  empty: boolean;
  shownGroup = null;
  loading: any;
  msg: any;
  icon_nm: any;
  options: any;
  arr: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public http: HttpClient, public storage: Storage, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public modalCtrl: ModalController, public alertCtrl: AlertController) {

  	this.storage.get('id').then((id)=>{
      this.id = id;
    });
    this.picture = false;
    this.add = false;
    this.msg = 'Processing...';
    this.icon_nm = 'image';

    let data = this.navParams.get('data');
  	if(data == 'about_us')
  		this.about = true;
  	else if(data == 'contact_us')
  		this.contact = true;
  	else if(data == 'privacy')
  		this.privacy = true;
  	else if(data == 'faq')
  		this.faq = true;
  	else if(data == 'cookies')
  		this.cookies = true;
    else if(data == 'picture')
    {
      this.picture = true;
      this.loading = this.loadingCtrl.create({
        content: 'Uploading your picture...',
        duration: 5000
      });
      this.loading.present();
    }
    else if(data == 'add_more')
    {
      let id1 = this.navParams.get('id');
      this.add = true;
      let headers = new HttpHeaders();
      headers.append('Content-Type',  'application/json');
      headers.append('Accept',  'application/json');
      this.options = {headers: headers};

      let data = JSON.stringify({id : id1});
      this.http.post( ENV.BASE_URL + 'webservices/Hobby2/view', data, this.options).subscribe(result=>{
        let user = JSON.parse(result["_body"]);
        this.arr = user.education;
        if(this.arr == null || this.arr == '')
          this.empty = true;
      });
   
    }
    else if(data == 'guide')
      this.guide = true;
    else if(data == 'terms')
      this.terms = true;
    else
      this.del_data = true;

  }

  ionViewDidLoad()
  {
    if(this.picture)
    {
      setTimeout(() => {
        this.msg = 'Your profile picture has been updated successfully';
        this.icon_nm = 'checkmark-circle';
      }, 5000);
    }
  }

  toggleGroup(group) 
  {
    if(this.isGroupShown(group)) 
      this.shownGroup = null;
    else 
      this.shownGroup = group;
  }

  isGroupShown(group) 
  {
    return this.shownGroup === group;
  }

  view_edu()
  {
    this.storage.get('id').then(id=>{
      let data = JSON.stringify({id : id});
      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/view', data, this.options).subscribe(result=>{
        this.loading.dismiss();
        let user = JSON.parse(result["_body"]);
        this.arr = user.education;
        if(this.arr == null || this.arr == '')
          this.empty = true;
      });
    });
  }

  closeModal()
  {
  	this.viewCtrl.dismiss();
  }

  del()
  {
    let headers = new HttpHeaders();
    headers.append('Content-Type',  'application/json');
    headers.append('Accept',  'application/json');
    let options = {headers: headers};
    let data1 = JSON.stringify({id: this.id});
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/add_harddelete', data1, options).subscribe(result=>{
      this.toastCtrl.create({
        message: 'Your data will be deleted successfully',
        duration: 3000
      }).present();
      this.storage.set('logged_in','0');
      this.navCtrl.setRoot('HomePage');
    });
  }

  add_more()
  {
    let modal = this.modalCtrl.create('AddEduPage');
    modal.onDidDismiss(data=>{
      if(data.opt == 'data')
      {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            duration: 10000
        });
        this.loading.present();
        this.view_edu();
      }
    });
    modal.present();
  }

  del_edu(i)
  {
    this.alertCtrl.create({
      title: 'Delete',
      subTitle: 'Are you sure you want to delete this data?',
      buttons: [
      {
        text: 'Delete',
        handler: () => {
          let data = JSON.stringify({eid: i});
          this.http.post(ENV.BASE_URL + 'webservices/Hobby2/delete_education', data, this.options).subscribe(result=>{
            this.view_edu();
            this.toastCtrl.create({
                message: 'Deleted',
                duration: 3000
              }).present();
          });
        }
      },
      {
        text: 'Cancel',
        handler: () => {}
      }
      ]
    }).present();
  }

  skip()
  {
    let hobbies = [];
    this.storage.get('hobbies_added').then(dt=>{
      if(dt == 1)
        this.navCtrl.setRoot('ProfilesPage');
      else
        this.navCtrl.push('EditHobbyPage',{field: 'add_hobby', 'info': hobbies});
    });
  }

}
