import { NgModule } from '@angular/core';
import { AddCareerPage } from './add-career';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [AddCareerPage],
  imports: [IonicPageModule.forChild(AddCareerPage)],
  exports: [AddCareerPage]
})
export class AddCareerPageModule { }
