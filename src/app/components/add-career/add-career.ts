import { Component, ElementRef, NgZone, ViewChild } from '@angular/core';
import { NavController, IonicPage, NavParams, ViewController, ToastController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FormControl } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import { environment as ENV } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { google } from '@agm/core/services/google-maps-types';

@IonicPage()
@Component({
  selector: 'page-add-career',
  templateUrl: 'add-career.html',
})
export class AddCareerPage {

  levels: any = [];
  years: any = [];
  months: any = [];
  s_levels: any = [];
  showlevel: boolean;
  f_month: string;
  f_year: any;
  t_month: any;
  t_year: any;
  company: any;
  c_size: any;
  s_level: any;
  job: any;
  level: any;
  current: any;
  max: any;
  experience: any;
  id: any;
  history: any = [];
  address: any;
  field: any;
  m1: any;
  m2: any;
  nav: boolean;
  show1: boolean;
  show2: boolean;
  show3: boolean;
  show4: boolean;
  show5: boolean;
  showfinance: boolean;
  show_b: boolean;
  ind1: any;
  ind2: any;
  ind3: any;
  invest: any = [];
  tech: any = [];
  finance: any = [];
  industry: any = [];
  industries: any = [];
  other: any;
  company_desc: any;
  finance_level: any;
  public searchControl: FormControl;
  blockchain: any = [];
  blkchain: any;
  teach: any;
  check: any = [];

  @ViewChild("search", {static: false})
  public searchElementRef: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public viewCtrl: ViewController, public storage: Storage, private mapsAPILoader: MapsAPILoader, private ngZone: NgZone, public toastCtrl: ToastController, public modalCtrl: ModalController) {

    this.show_b = false;
    this.t_month = '';
    this.t_year = '';
    this.c_size = 'Startup';
    this.ind1 = '';
    this.ind2 = '';
    this.ind3 = '';
    this.finance_level = '';
    this.other = '';
    this.company_desc = '';
    this.field = this.navParams.get('field');
    if (this.field == 'add')
      this.nav = true;
    else
      this.nav = false;
    this.storage.get('cname').then((val) => {
      this.company = val;
    });
    this.storage.get('title').then((val) => {
      this.job = val;
    });
    this.storage.get('start_y').then((val) => {
      this.f_year = val;
    });
    this.storage.get('start_m').then(val => {
      this.f_month = val;
    });

    this.s_level = '';
    this.current = '0';
    this.check = [];
    this.teach = '';

    this.levels = ['Intern', 'Analyst', 'Team Leader/Associate/Product Specialist', 'Manager of one team', 'Manager of more than one team', 'Head of one department', 'Head of more than one department', 'CEO/CFO/COO/Board Member/Non Executive Directors'];

    this.max = new Date().getFullYear();
    var min = this.max - 68;

    for (var i = this.max; i >= min; i--)
      this.years.push(i);

    this.months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    this.s_levels = ['Internship', 'Analyst', 'Associate', 'Manager', 'Vice President/Assistant Vice President', 'Director', 'Executive Director', 'Managing Director', 'Partner', 'None'];

    this.industries = ['Founder', 'Investor', 'Tech', 'Finance', 'Insurance', 'Consultancy', 'Financial Law', 'Other']

    this.invest = ['Venture Capital', 'Angel'];

    this.tech = ['Tech', 'Fintech', 'Regtech', 'Insurtech', 'Healthtech'];

    this.finance = ['Finance', 'Asset-based finance and leasing', 'Funds management/ Superannuation', 'Hedge funds', 'Proprietary trading', 'Investment banking', 'Payment systems, clearing and settlement', 'Private banking', 'Private equity', 'Retail banking', 'Wealth Management'];

    this.blockchain = ['Happy to teach others', 'Can speak on conferences/panels', 'Can write articles/blogs'];

  }

  ngOnInit() {

    //create search FormControl
    this.searchControl = new FormControl();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["geocode"]
      });
      autocomplete.addListener("place_changed", () => {
        /*this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          this.address = place.formatted_address;

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
        });*/
      });
    });
  }

  update1(item, event) {
    if (event.checked) {
      if (item == '0') {
        if (this.check.includes('Happy to teach others'))
          return;
        else
          this.check.push('Happy to teach others');
      } else if (item == '1') {
        if (this.check.includes('Can speak on conferences/panels'))
          return;
        else
          this.check.push('Can speak on conferences/panels');
      } else if (item == '2') {
        if (this.check.includes('Can write articles/blogs'))
          return;
        else
          this.check.push('Can write articles/blogs');
      } else
        return;
    } else {
      if (item == '0')
        this.check.splice(this.check.indexOf('Happy to teach others'), 1);
      else if (item == '1')
        this.check.splice(this.check.indexOf('Can speak on conferences/panels'), 1);
      else if (item == '2')
        this.check.splice(this.check.indexOf('Can write articles/blogs'), 1);
      else
        return;
    }
  }

  changew(event) {
    if (event.checked) {
      this.current = '1';
      this.t_year = '';
      this.t_month = '';
    } else {
      this.current = '0';
    }
  }

  radioChecked(i) {
    if (i == 3)
      this.showlevel = true;
    else
      this.showlevel = false;
  }

  radioChecked1(i, event) {
    if (event.checked) {
      if (this.industry.includes(this.industries[i]))
        return;
      else
        this.industry.push(this.industries[i]);

      if (i == 0) {
        this.show5 = true;
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.show4 = false;
        this.showfinance = false;
      } else if (i == 1) {
        this.show1 = true;
        this.show2 = false;
        this.show3 = false;
        this.show4 = false;
        this.show5 = false;
        this.showfinance = false;
      } else if (i == 2) {
        this.show2 = true;
        this.show1 = false;
        this.show3 = false;
        this.show4 = false;
        this.show5 = false;
        this.showfinance = false;
      } else if (i == 3) {
        this.show3 = true;
        this.show1 = false;
        this.show2 = false;
        this.show4 = false;
        this.show5 = false;
        this.showfinance = false;
      } else if (i == 7) {
        this.show4 = true;
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.show5 = false;
        this.showfinance = false;
      } else {
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.show4 = false;
        this.show5 = false;
        this.showfinance = false;
      }
    } else {
      this.industry.splice(this.industry.indexOf(this.industries[i]), 1);
      if (i == 0)
        this.show5 = false;
      else if (i == 1)
        this.show1 = false;
      else if (i == 2)
        this.show2 = false;
      else if (i == 3)
        this.show3 = false;
      else if (i == 7)
        this.show4 = false;
    }
  }

  radioChecked2(i) {
    this.showfinance = true;
  }

  add() {
    this.show_b = true;
    this.storage.remove('cname');
    this.storage.remove('title');
    this.storage.remove('start_m');
    this.storage.remove('start_y');

    if (this.t_year == '')
      this.experience = this.max - this.f_year;
    else
      this.experience = this.t_year - this.f_year;

    if (this.f_month == null || this.f_year == null || this.company == null || this.c_size == null || this.job == null || this.level == null || this.address == null || this.industry == null || this.teach == null || this.check == null) {
      this.toastCtrl.create({
        duration: 3000,
        message: 'Please fill in all the details'
      }).present();
    } else {
      this.storage.get('id').then(dt => {
        this.id = dt;

        let word;
        if (this.level == 'Intern')
          word = 'Internship';
        else
          word = this.level;

        let headers = new HttpHeaders();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        let options = {headers: headers};
        let data = JSON.stringify({
          id: this.id,
          company_name: this.company,
          company_size: this.c_size,
          job_title: this.job,
          level: word,
          other_level: this.s_level,
          locations: this.address,
          start_month: this.f_month,
          start_year: this.f_year,
          end_month: this.t_month,
          end_year: this.t_year,
          iscurrent: this.current,
          total_experience: this.experience,
          industry: this.industry,
          ind1: this.ind1,
          ind2: this.ind2,
          ind3: this.ind3,
          other: this.other,
          description: this.company_desc,
          finance_level: this.finance_level,
          blockchain: this.teach,
          b_array: this.check
        });

        this.http.post(ENV.BASE_URL + 'webservices/Hobby2/add_career', data, options).subscribe(result => {
        });

        if (this.nav)
          this.viewCtrl.dismiss({'page': 'addCar', 'opt': 'data'});
        else
          this.viewCtrl.dismiss({'opt': 'data', 'id': this.id});
      });
    }
  }

  closeModal() {
    if (this.field == 'add')
      this.navCtrl.pop();
    else
      this.viewCtrl.dismiss({'opt': 'none', 'id': this.id});
  }

}
