import { NgModule } from '@angular/core';
import { AddEduPage } from './add-edu';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [AddEduPage],
  imports: [IonicPageModule.forChild(AddEduPage)],
  exports: [AddEduPage]
})
export class AddEduPageModule { }
