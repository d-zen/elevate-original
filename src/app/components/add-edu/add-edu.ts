import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController, IonicPage } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import { environment as ENV } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { google } from "@agm/core/services/google-maps-types";
// import {} from 'googlemaps';

@IonicPage()
@Component({
  selector: 'page-add-edu',
  templateUrl: 'add-edu.html',
})
export class AddEduPage {
  @ViewChild("search", {static: false})

  years: any = [];
  degree: any;
  uni: any;
  ed_year: any;
  id: any;
  history: any = [];
  address: any;
  field: any;
  show: boolean;
  show_b: boolean;

  public searchControl: FormControl;
  public searchElementRef: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public http: HttpClient,
    public storage: Storage,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    public toastCtrl: ToastController
  ) {
    this.field = this.navParams.get('field');
    this.show_b = false;

    this.show = this.field == 'incomplete';

    this.storage.get('id').then(data => {
      this.id = data;
    });

    var max = new Date().getFullYear();
    var min = max - 68;
    for (var i = max; i >= min; i--)
      this.years.push(i);

  }

  ngOnInit() {

    //create search FormControl
    this.searchControl = new FormControl();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["geocode"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
         /* //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          this.address = place.formatted_address;
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }*/
        });
      });
    });
  }

  closeModal() {
    this.viewCtrl.dismiss({'opt': 'none'});
  }

  add() {
    this.show_b = true;
    if (this.degree == null || this.uni == null || this.ed_year == null || this.address == null) {
      this.toastCtrl.create({
        duration: 3000,
        message: 'Please fill in all the details'
      }).present();
    } else {

      let str = this.degree;
      this.history.push(str);
      this.storage.get('edu_history').then(data => {
        if (data) {
          let arr = [];
          for (let i = 0; i < data.length; i++)
            arr.push(data[i]);
          arr.push(str);
          this.storage.set('edu_history', arr);
        } else
          this.storage.set('edu_history', this.history);
      });

      let headers = new HttpHeaders();
      headers.append('Content-Type', 'application/json');
      headers.append('Accept', 'application/json');
      let options = {headers: headers};
      let data = JSON.stringify({
        id: this.id,
        degree: this.degree,
        university: this.uni,
        year: this.ed_year,
        country: this.address
      });

      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/add_education', data, options).subscribe(result => {
        if (!this.show)
          this.viewCtrl.dismiss({'opt': 'data'});
        else
          this.viewCtrl.dismiss({'page': 'addEdu'});
      });
    }
  }

  skip() {
    //this.navCtrl.popToRoot();
    this.viewCtrl.dismiss({'page': 'end'});
  }

}
