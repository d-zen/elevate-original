import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { environment as ENV } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@IonicPage()
@Component({
  selector: 'page-admin-message',
  templateUrl: 'admin-message.html',
})
export class AdminMessagePage {

  answer: any;
  disable: boolean;
  sid: any;
  id: any;
  fid: any;
  str: any;
  img: any;
  fname: any;
  lname: any;
  acc: boolean;
  options: any;
  threadId: any;
  show_c: boolean;
  show_a: boolean;
  show_button: boolean;
  logo: any;
  desc: any;
  comm_id: any;
  cid: any;
  cname: any;
  email: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public http: HttpClient, public storage: Storage, public toastCtrl: ToastController) {

    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    this.options = { '{headers}': headers };
    this.disable = false;
    this.sid = this.navParams.get('sid');
    this.cid = this.navParams.get('cid');
    this.comm_id = this.navParams.get('comm_id');
    this.fid = this.navParams.get('fid');
    this.id = this.navParams.get('id');
    this.str = this.navParams.get('app');
    this.img = this.navParams.get('img');
    this.fname = this.navParams.get('fname');
    this.lname = this.navParams.get('lname');
    this.cname = this.navParams.get('cname');
    this.email = this.navParams.get('email');
    this.desc = this.navParams.get('desc');
    this.logo = this.navParams.get('logo');
    let page = this.navParams.get('page');
    if (page == 'community')
      this.show_c = true;
    else
      this.join_group();
    if (this.str == 'yes')
      this.acc = true;
    else
      this.acc = false;

  }

  closeModal() {
    this.viewCtrl.dismiss({ 'dt': 'no' });
  }

  join_group() {
    this.show_a = true;
    this.storage.get('id').then(id => {
      this.id = id;
      let data = JSON.stringify({ uid: this.id, cid: this.cid, tid: this.comm_id });
      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/get_user_request', data, this.options).subscribe(result => {
        let user = JSON.parse(result["_body"]);
        if (user == 0)
          this.show_button = true;
        else {
          this.show_button = false;
          this.toastCtrl.create({
            message: 'Admin to accept',
            duration: 2000
          }).present();
        }
      });
    });
  }

  request_to_join() {
    let data1 = JSON.stringify({ uid: this.id, cid: this.cid, tid: this.comm_id, uname: this.fname, lname: this.lname, img: this.img, comp: this.cname, email: this.email });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/add_comp_request', data1, this.options).subscribe(result => {
      this.viewCtrl.dismiss();
      this.toastCtrl.create({
        message: 'Admin to accept',
        duration: 2000
      }).present();
    });
  }

  send() {
    let data = JSON.stringify({ sid: this.sid, rid: this.fid, uid: this.id, msg: this.answer, img: this.img });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/approve_user', data, this.options).subscribe(result => {
      this.threadId = JSON.parse(result["_body"]);
      this.message();
      this.viewCtrl.dismiss({ 'dt': 'yes' });
    });
  }

  reject() {
    let data = JSON.stringify({ sid: this.sid, rid: this.fid, uid: this.id, msg: this.answer, img: this.img });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/reject_user', data, this.options).subscribe(result => {
      this.threadId = JSON.parse(result["_body"]);
      this.message();
      this.viewCtrl.dismiss({ 'dt': 'yes' });
    });
  }

  message() {
    var d = new Date();
    let y = d.toString().slice(11, 15);
    let d1 = d.toString().slice(8, 10);
    let m = d.toString().slice(4, 7);
    let t = d.toString().slice(16, 24);

    if (m == 'Jan')
      m = '01';
    else if (m == 'Feb')
      m = '02';
    else if (m == 'Mar')
      m = '03';
    else if (m == 'Apr')
      m = '04';
    else if (m == 'May')
      m = '05';
    else if (m == 'Jun')
      m = '06';
    else if (m == 'Jul')
      m = '07';
    else if (m == 'Aug')
      m = '08';
    else if (m == 'Sep')
      m = '09';
    else if (m == 'Oct')
      m = '10';
    else if (m == 'Nov')
      m = '11';
    else
      m = '12';

    const datey = y + '-' + m + '-' + d1 + ' ' + t;

    const reptRef = firebase.database().ref();
    reptRef.once('value', personSnapshot => {

      const myPerson = personSnapshot.val();
      const obj1 = myPerson.list[this.id];
      const len = obj1.length;
      const reportRef1 = firebase.database().ref('/list/' + this.id + '/');
      const usersRef1 = reportRef1.child(len);
      usersRef1
        .set({
          message: this.answer,
          firstname: this.fname,
          friends: 'yes',
          img: this.img,
          lastname: this.lname,
          sender_id: this.id,
          date: datey,
          read_status: "0",
          receiver_id: this.fid,
          thread_id: this.threadId
        });

      const obj2 = myPerson.list[this.fid];
      const len1 = obj2.length;
      const reportRef2 = firebase.database().ref('/list/' + this.fid + '/');
      const usersRef2 = reportRef2.child(len1);
      usersRef2
        .set({
          message: this.answer,
          firstname: this.fname,
          friends: 'yes',
          img: this.img,
          lastname: this.lname,
          sender_id: this.id,
          date: datey,
          read_status: '0',
          receiver_id: this.fid,
          thread_id: this.threadId
        });
    });

    const reportRef = firebase.database().ref('/messages/' + this.threadId + '/');
    const usersRef = reportRef.child('0');
    usersRef
      .set({
        message: this.answer,
        sender_id: this.id,
        date: datey,
        delete_status: "0",
        read_status: "0",
        receiver_id: this.fid,
        user_id: this.id,
        thread_id: this.threadId
      });
  }

}
