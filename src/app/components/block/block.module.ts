import { NgModule } from '@angular/core';
import { BlockPage } from './block';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [BlockPage],
  imports: [IonicPageModule.forChild(BlockPage)],
  exports: [BlockPage]
})
export class BlockPageModule { }
