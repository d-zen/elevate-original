import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Events, IonicPage } from '@ionic/angular';
import { environment as ENV } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-block',
  templateUrl: 'block.html',
})
export class BlockPage {

	answer: any;
  uid: any;
  fid: any;
  options: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public http: HttpClient, public events: Events) {

  this.uid = this.navParams.get('uid');
  this.fid = this.navParams.get('fid');

  const headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('Accept', 'application/json');
  this.options = { headers: headers };

  }

  closeModal()
  {
  	this.viewCtrl.dismiss({'dt':'no'});
  }

  block()
  {
    let data = JSON.stringify({uid: this.uid, fid: this.fid});
    this.http.post( ENV.BASE_URL + 'webservices/Hobby2/block',data,this.options).subscribe(result=>{
      this.events.publish('blocked');
    });
    this.viewCtrl.dismiss({'dt': 'yes'});
  }

  send()
  {
    let data = JSON.stringify({ uid: this.uid, fid: this.fid, msg: this.answer });
    this.http.post( ENV.BASE_URL + 'webservices/Hobby2/block_mail', data, this.options).subscribe(result => {
      this.events.publish('blocked');
    });
    this.viewCtrl.dismiss({'dt': 'yes'});
  }

}
