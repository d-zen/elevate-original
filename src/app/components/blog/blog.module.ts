import { NgModule } from '@angular/core';
import { BlogPage } from './blog';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [BlogPage],
  imports: [IonicPageModule.forChild(BlogPage)],
  exports: [BlogPage]
})
export class BlogPageModule { }
