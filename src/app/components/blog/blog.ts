import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ToastController, LoadingController, Content, IonicPage } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { environment as ENV } from '../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
 
@IonicPage()
@Component({
  selector: 'page-blog',
  templateUrl: 'blog.html',
})
export class BlogPage {

  blog_id: any;
  blog: any = {};
  cmts: any = [];
  comment: any;
  uid: any;
  empty: boolean;
  options: any;
  loading: any;
  video: any;
  detail: any;
  newsletter: boolean;
  name: any;
  @ViewChild(Content, {static: false}) content: Content;

  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, public http: HttpClient, public storage: Storage, public toastCtrl: ToastController, public loadingCtrl: LoadingController, private iab: InAppBrowser) {  

    this.newsletter = false;
    this.storage.get('id').then((id)=>{ this.uid = id; });

    let headers = new HttpHeaders;
    headers.append('Content-Type',  'application/json');
    headers.append('Accept',  'application/json');
    this.options = {headers: headers};

    this.blog_id = this.navParams.get('id');
    let field = this.navParams.get('field');
    if(field == 'newsletter')
    {
      this.newsletter = true;
      this.detail = this.navParams.get('content');
      this.name = 'Newsletter';
    }
    else
    {
      this.newsletter = false;
      this.name = 'Blog';

      this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      this.loading.present();

      let data = JSON.stringify({id : this.blog_id});
      this.http.post( ENV.BASE_URL + 'webservices/Hobby2/single_blog', data, this.options).subscribe(result=>{
        this.hideLoading();
        let bl = JSON.parse(result["_body"]);
        this.blog = bl[0].blog[0];
        this.cmts = bl[0].comment;
        this.video = bl[0].url;
        if(this.cmts.length < 1)
          this.empty=true;
      });
    }
  }

  hideLoading() {
    this.loading.dismiss();
  }

  scroll_page_up()
  {
    setTimeout(() => {
      this.content.scrollToTop(500);
    }, 200);
  }

  share()
  {
  	// let actionSheet = this.actionSheetCtrl.create({
    //  title: 'Share via',
    //  buttons: [
    //    {
    //      icon: 'logo-whatsapp',
    //      text: 'WhatsApp',
    //      cssClass: 'green_c',
    //      handler: () => {
    //        console.log('Whatsapp');
    //        let title = this.blog.title;
    //        let message = 'Just read this interesting article:\n' + title + ' on ElevatHer \n';
    //        let image = null;
    //        let url = 'Android: https://play.google.com/store/apps/details?id=com.elevather.designs \n IOS: https://itunes.apple.com/app/id1441360188';
    //        this.socialSharing.canShareVia('com.whatsapp', message, 'Blog', image, url).then(()=>{
    //         this.socialSharing.shareViaWhatsApp(message, image, url).then(()=>{
    //           console.log('Shared');
    //         });
    //        });
    //      }
    //    }
       /*,
       {
         icon: 'logo-facebook',
         text: 'Facebook',
         handler: () => {
           console.log('Facebook');
         }
       },
       {
         icon: 'logo-twitter',
         text: 'Twitter',
         handler: () => {
           console.log('Twitter');
         }
       }*/
    //  ]
  //  });

  //  actionSheet.present();
  }

  addComment()
  {
    if(this.comment != null)
    {
      let data2 = JSON.stringify({bid : this.blog_id, uid: this.uid, comment: this.comment});
      this.http.post( ENV.BASE_URL + 'webservices/Hobby2/comment', data2, this.options).subscribe(result=>{
        this.comment = '';
        this.toastCtrl.create({
          message: 'Comment added succesfully. It will be displayed after being reviewed by the admin.',
          duration: 3000
        }).present();
      });
    }
  }

  open_browser()
  {
    if(this.video != '')
    {
    const options: InAppBrowserOptions = {
      zoom: 'no',
      toolbarcolor: '#7d0d3f',
      hidenavigationbutton: 'yes',
      hideurlbar: 'yes',
      location: 'yes',
      closebuttoncolor: '#ffffff',
      toolbarposition: 'top'
    }
    var target = "_blank";
    this.iab.create(this.video, target, options);
    }
  }

}
