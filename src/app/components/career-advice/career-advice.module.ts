import { NgModule } from '@angular/core';
import { IonicPageModule } from '@ionic/angular';
import { CareerAdvicePage } from './career-advice';

@NgModule({
  declarations: [CareerAdvicePage],
  imports: [IonicPageModule.forChild(CareerAdvicePage)],
  exports: [CareerAdvicePage]
})
export class CareerAdvicePageModule {}
