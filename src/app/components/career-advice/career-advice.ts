import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from '@ionic/angular';

@IonicPage()
@Component({
  selector: 'page-career-advice',
  templateUrl: 'career-advice.html',
})
export class CareerAdvicePage {

	res: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  	this.res = ['Articles','Inspirational Women','Newsletters','Podcast']

  }

  openPage(str)
  {
  	this.navCtrl.push('ListPage',{field: str});
  }

}
