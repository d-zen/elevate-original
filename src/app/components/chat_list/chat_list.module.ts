import { NgModule } from '@angular/core';
import { chatListPage } from './chat_list';
import { IonicPageModule } from '@ionic/angular';
import { PipesModule } from 'src/pipes/pipes.module';

@NgModule({
  declarations: [chatListPage],
  imports: [
    IonicPageModule.forChild(chatListPage),
    PipesModule
  ],
  exports: [chatListPage],
})
export class chatListPageModule {
}
