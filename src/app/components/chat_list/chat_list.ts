import { Component, ViewChild } from '@angular/core';
import {
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  MenuController,
  Events,
  Content,
  Platform,
  IonicPage
} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network/ngx';
import * as firebase from 'firebase';
import { SearchProvider } from '../../../services/search/search';
import { ArraySortPipe } from '../../../pipes/filter-by-name.pipe';
import { AuthService } from '../../../services/auth.service';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@IonicPage()
@Component({
  selector: 'page-chat_list',
  templateUrl: 'chat_list.html',
  providers: [ ArraySortPipe ]
})
export class chatListPage {
  rooms = [];
  loading: any;
  threads: any = [];
  fetchedData: any = [];
  fetched: any;
  fetched1: any = [];
  uid: any;
  noshow: boolean;
  yest: any;
  today: any;
  subscription: any;
  time: any = [];
  read_stat: any = [];
  img: any = [];
  firstname: any = [];
  lastname: any = [];
  friends: any = [];
  search: boolean;
  showdiv: boolean;
  is_ios: boolean;
  items: any = [];
  myInput: any;
  @ViewChild(Content, {static: false}) content: Content;
  private subscriptions: Subscription[] = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public alertCtrl: AlertController,
    private network: Network,
    public menuCtrl: MenuController,
    public events: Events,
    public searchService: SearchProvider,
    private auth: AuthService,
    public plt: Platform) {


    if (this.plt.is('ios'))
      this.is_ios = true;
    else
      this.is_ios = false;

    this.menuCtrl.close();
    this.search = false;
    this.showdiv = false;
    this.uid = localStorage.getItem('id');
    if (this.network.type == 'none') {
      this.noshow = true;
      console.log('Not connected');
      this.alertCtrl.create({
        title: 'No internet connection',
        subTitle: 'Get connected to the internet in order to use the app',
        buttons: ['OK']
      }).present();
    }
      

  }

  ngOnInit() {
    this.subscriptions.push(
      this.auth.me()
        .subscribe(user => {
          this.uid = user.id;
          this.getMessages();
        })
    )
    this.auth.token.pipe(
      filter(token => !!token)
    )
    .subscribe(token => {
      // this.token = token;
    });
    }

  getMessages() {
    this.showdiv = false;
    this.search = false;
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      duration: 10000
    });
    this.loading.present();
    var reportRef = firebase.database().ref();
    reportRef.on('value', personSnapshot => {
      let obj = personSnapshot.val().list;
      let user_obj = obj[this.uid];
      this.fetchedData = Object.keys(user_obj).map(item => user_obj[item]);
      console.log(this.fetchedData)
      this.loading.dismiss();
    });
  }

  setFilteredItems() {
    this.items = this.searchService.filterNames(this.myInput);
    if (this.items.length > 0)
      this.showdiv = true;
    else
      this.showdiv = false;
  }

  srch() {
    this.search = true;
  }

  scroll_page_up() {
    setTimeout(() => {
      this.content.scrollToTop(500);
    }, 500);
  }

  joinRoom(senderId, receiverId, firstName, lastName, tid) {
    this.update_stat(senderId, tid);
    this.update_stat(receiverId, tid);
    this.storage.get('id').then(id => {
      if (senderId == id) {
        this.navCtrl.push('chatRoomPage', {
          receiverId: receiverId,
          firstName: firstName,
          lastName: lastName,
          threadId: tid
        });
      }
      else {
        this.navCtrl.push('chatRoomPage', {
          receiverId: senderId,
          firstName: firstName,
          lastName: lastName,
          threadId: tid
        });
      }
    });
  }

  update_stat(id, tid) {
    var reptRef = firebase.database().ref();
    reptRef.once('value', personSnapshot => {

      let myPerson = personSnapshot.val();
      let obj = myPerson.list;

      let fetched1 = obj[id];
      for (var i in fetched1) {
        if (fetched1[i] != null) {
          if (fetched1[i].thread_id == tid && fetched1[i].receiver_id == this.uid) {
            var repRef = firebase.database().ref('/list/' + id + '/' + i + '/');
            repRef
              .update({
                read_status: '1'
              });
          }
        }
      }
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(item => item.unsubscribe());
  }

}
