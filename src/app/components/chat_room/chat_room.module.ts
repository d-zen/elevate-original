import { NgModule } from '@angular/core';
import { chatRoomPage } from './chat_room';
import { IonicPageModule } from '@ionic/angular';
import { EmojiPickerModule } from '@ionic-tools/emoji-picker';

@NgModule({
  declarations: [chatRoomPage],
  imports: [
  	EmojiPickerModule,
  	IonicPageModule.forChild(chatRoomPage)
  	],
  exports: [chatRoomPage]
})
export class chatRoomPageModule { }
