import { Component, ViewChild, OnInit } from '@angular/core';
import {
  NavController,
  NavParams,
  Content,
  LoadingController,
  ToastController,
  ModalController,
  IonicPage
} from '@ionic/angular';
import { Storage } from '@ionic/storage';

import { PopoverController, Events } from '@ionic/angular';
import * as firebase from 'firebase';
import { environment as ENV } from "../../../environments/environment";
import { AuthService } from '../../../services/auth.service';
import { Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-chat_room',
  templateUrl: 'chat_room.html'
})
export class chatRoomPage implements OnInit {
  @ViewChild(Content, {static: false}) content: Content;
  data = { type: '', nickname: '', message: '' };
  chats = [];
  show: boolean;
  show_button: boolean;
  receiverId: any;
  nickname: any;
  fn: string;
  ln: string;
  offStatus: boolean = false;
  loading: any;
  user_id: any;
  fetchedMsg: any;
  threadId: any;
  getNickName: any;
  showMsg: string;
  options: any;
  subscription: any;
  blocked: any;
  showPop: boolean;
  dates: any = [];
  dts: any = [];
  first: any;
  chat_len: any;
  datey: any;
  toggled: boolean = false;
  new_date: boolean = false;
  today: any;
  private subscriptions: Subscription[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpClient,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    private auth: AuthService,
    public events: Events) {
  }

  ngOnInit() {

    this.subscriptions.push(
      this.auth.me()
        .subscribe(user => {
          this.user_id = user.id;
          this.getNickName = user.firstname;
          this.receiverId = this.navParams.get("receiverId");
          this.threadId = this.navParams.get("threadId");
          this.fn = this.navParams.get("firstName");
          this.ln = this.navParams.get("lastName");
          this.user_id = localStorage.getItem('id');
          this.nickname = this.fn + ' ' + this.ln;
          this.data.type = 'message';
          this.first = 0;

          var reportRef = firebase.database().ref();
          reportRef.once('value', personSnapshot => {
            let myPerson = personSnapshot.val();
            let obj = myPerson.messages;
            this.chats = obj[this.threadId];
            console.log(this.chats)
            setTimeout(() => {
              this.content.scrollToBottom(300);
            }, 1000);
            this.chat_len = this.chats.length;

            if (this.first == 0) {
              for (let i = 0; i < this.chat_len; i++) {
                if (this.chats[i] != null) {
                  let date = this.chats[i].date.slice(0, 10);
                  if (i == 0) {
                    this.dts.push('1');
                    this.dates.push(this.chats[i].date.slice(0, 10));
                  }
                  else if (date != this.chats[i - 1].date.slice(0, 10)) {
                    this.dts.push('1');
                    this.dates.push(date);
                  }
                  else {
                    this.dts.push('0');
                    this.dates.push('none');
                  }
                }
              }
              this.first = 1;
            }
            if (this.new_date) {
              this.dates.push(this.today);
              this.dts.push('1');
              this.new_date = false;
            }

          });

          firebase.database().ref().child('messages/' + this.threadId).on('child_added', (snap) => {
            setTimeout(() => {
              this.content.scrollToBottom(300);
            }, 1000);
          });

          let headers = new HttpHeaders();
          headers.append('Content-Type', 'application/json');
          headers.append('Accept', 'application/json');
          this.options = { headers: headers };
          this.block_unbl();

          this.events.subscribe('blocked', () => {
            this.block_unbl();
          });

          this.getMessages();
          this.showMsg = 'Type a message';
        })
    )
  }

  block_unbl() {
    let data = JSON.stringify({ uid: this.user_id, fid: this.receiverId });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/block_unblock', data, this.options)
      .subscribe(result => {
        let data = JSON.parse(result["_body"]);
        if (data.ans == '0') {
          this.blocked = '0';
          this.show_button = false;
          this.showMsg = 'Type a message';
        }
        else {
          this.blocked = '1';
          this.show_button = true;
          this.showMsg = 'You have blocked this user';
        }
      });
  }

  presentPopover(myEvent, fid, name, i) {
    if (this.showPop) {
      let popover = this.popoverCtrl.create('PopoverPage', { 'fid': fid, 'name': name, 'block': i, 'page': 'messaging', 'field': 'yes', sh_button: 'no' });
      popover.present({
        ev: myEvent
      });
    }
    else {
      let popover = this.popoverCtrl.create('PopoverPage', { 'fid': fid, 'name': name, 'block': i, 'page': 'messaging', 'field': 'no' });
      popover.present({
        ev: myEvent
      });
    }
  }

  getMessages() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      duration: 10000
    });
    this.loading.present();

    this.storage.get('id').then(user => {
      this.user_id = user;

      let headers = new HttpHeaders();
      headers.append('Content-Type', 'application/json');
      headers.append('Accept', 'application/json');
      this.options = { headers: headers };
      let data = JSON.stringify({ sender_id: this.user_id, receiver_id: this.receiverId });
      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/chatbyid', data, this.options)
        .subscribe(result => {
          this.hideLoading();

          let data = JSON.parse(result["_body"]);
          if (data[0].senderid != this.user_id) {
            if (data[0].accept_reject[0].status == '0')
              this.show = true;
          }

          if (data[0].senderid == this.user_id) {
            if (data[0].msg_that_accept_reject[0].read_status == '1') {
              if (data[0].pending == '0') {
                this.show_button = true;
                this.showMsg = 'Request Not Accepted';
              }
              else {
                this.show_button = false;
                if (data[0].msg.length > 1)
                  this.showMsg = 'Type a message';
                else
                  this.showMsg = 'Request Accepted';
              }
            }
            else {
              this.showMsg = 'Request Pending';
              this.show_button = true;
            }
          }
          if (data[0].friend == 'yes')
            this.showPop = true;
          else
            this.showPop = false;
          if (data[0].block == 'true')
            this.showMsg = 'You have blocked this user';

          setTimeout(() => {
            //if (this.offStatus === false) {
            if (this.content._scroll)
              this.content.scrollToBottom(300);
            //}
          }, 1000);

          if (data[0].pending == '2') {
            this.showMsg = 'You are no longer connected';
            this.show_button = true;
          }
          else if (data[0].pending == '3') {
            this.show_button = true;
            this.showMsg = 'Request Not Accepted';
          }
          else
            return;

        });
    });
    this.data.message = '';
  }

  hideLoading() {
    this.loading.dismiss();
  }

  sendMessage() {

    var d = new Date();
    let y = d.toString().slice(11, 15);
    let d1 = d.toString().slice(8, 10);
    let m = d.toString().slice(4, 7);
    let t = d.toString().slice(16, 24);

    if (m == 'Jan')
      m = '01';
    else if (m == 'Feb')
      m = '02';
    else if (m == 'Mar')
      m = '03';
    else if (m == 'Apr')
      m = '04';
    else if (m == 'May')
      m = '05';
    else if (m == 'Jun')
      m = '06';
    else if (m == 'Jul')
      m = '07';
    else if (m == 'Aug')
      m = '08';
    else if (m == 'Sep')
      m = '09';
    else if (m == 'Oct')
      m = '10';
    else if (m == 'Nov')
      m = '11';
    else
      m = '12';

    this.today = y + '-' + m + '-' + d1;
    this.datey = y + '-' + m + '-' + d1 + ' ' + t;

    if (this.dates.includes(this.today))
      this.new_date = false;
    else
      this.new_date = true;

    let data = JSON.stringify({
      sender_id: this.user_id,
      receiver_id: this.receiverId,
      thread_id: this.threadId,
      message: this.data.message
    });
    if (this.data.message != '') {
      var reportRef = firebase.database().ref('/messages/' + this.threadId + '/');
      var usersRef = reportRef.child(this.chat_len);
      usersRef
        .set({
          message: this.data.message,
          sender_id: this.user_id,
          date: this.datey
        });

      this.update_fire(this.user_id);
      this.update_fire(this.receiverId);

      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/chat_message', data, this.options)
        .subscribe(result => {
        });
    }
    this.data.message = '';
  }

  update_fire(id) {
    var reptRef = firebase.database().ref();
    reptRef.once('value', personSnapshot => {

      let myPerson = personSnapshot.val();
      let obj = myPerson.list;
      let fetched = obj[id];
      for (var i in fetched) {
        if (fetched[i].thread_id == this.threadId) {
          var repRef = firebase.database().ref('/list/' + id + '/' + i + '/');
          repRef
            .update({
              message: this.data.message,
              date: this.datey,
              sender_id: this.user_id,
              receiver_id: this.receiverId,
              read_status: '0'
            });
          return;
        }
      }
    });
  }

  ionViewWillLeave() {
    let data = JSON.stringify({ sender_id: this.user_id, receiver_id: this.receiverId });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/chatbyid', data, this.options).subscribe(data => {
    });

    var reptRef = firebase.database().ref();
    reptRef.once('value', personSnapshot => {

      let myPerson = personSnapshot.val();
      let obj = myPerson.list;

      let fetched1 = obj[this.user_id];
      for (var i in fetched1) {
        if (fetched1[i] != null) {
          if (fetched1[i].thread_id == this.threadId && fetched1[i].receiver_id == this.user_id) {
            var repRef = firebase.database().ref('/list/' + this.user_id + '/' + i + '/');
            repRef
              .update({
                read_status: '1'
              });
          }
        }
      }
    });
  }

  update_friend(id, tid) {
    var reptRef = firebase.database().ref();
    reptRef.once('value', personSnapshot => {

      let myPerson = personSnapshot.val();
      let obj = myPerson.list;
      let fetched = obj[id];
      for (var i in fetched) {
        if (fetched[i] != null) {
          if (fetched[i].thread_id == tid) {
            var repRef = firebase.database().ref('/list/' + id + '/' + i + '/');
            repRef
              .update({
                friends: 'no'
              });
            return;
          }
        }
      }
    });
  }

  accept() {
    let data = JSON.stringify({ receiver_id: this.receiverId, sender_id: this.user_id });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/accept_request', data, this.options)
      .subscribe(result => {
        this.show = false;
      });
    this.show_button = false;
  }

  handleSelection(event) {
    this.data.message += event.char;
  }

  reject() {
    this.http.get(ENV.BASE_URL + 'webservices/Hobby2/thread?uid=' + this.user_id + '&fid=' + this.receiverId).subscribe(result => {
      let res = JSON.parse(result["_body"]);
      let thread = res.thread_id;

      let data = JSON.stringify({ sender_id: this.user_id, receiver_id: this.receiverId, tid: thread });
      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/reject_request', data, this.options)
        .subscribe(result => {

          this.update_friend(this.user_id, thread);
          this.update_friend(this.receiverId, thread);

          this.show = false;
          this.toastCtrl.create({
            message: 'You have rejected the request from this user',
            duration: 3000
          }).present();
        });
    });
  }

  openPage(id) {
    this.modalCtrl.create('PersonPage', { id: id, connect: 'no' }).present();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(item => item.unsubscribe());
  }

}
