import { Component, ViewChild, ElementRef, NgZone, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, AlertController, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { FormControl } from '@angular/forms';
import { environment as ENV } from '../../../environments/environment';
import { AuthService } from '../../../services/auth.service';
import { Subscription } from 'rxjs';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { filter } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-community',
  templateUrl: 'community.html',
})
export class CommunityPage implements OnInit, OnDestroy {

  options: any;
  feat: any;
  id: any;
  obj: any;
  approve: boolean;
  show_users: boolean;
  empty: boolean;
  channel: boolean;
  comp: boolean;
  loading: any;
  cid: any;
  comm_id: any;
  description: any;
  logo: any;
  picture: any;
  select: boolean;
  time: any;
  events: boolean;
  address: any;
  city: any;
  title: any;
  summary: any;
  s_date: any;
  s_time: any;
  e_date: any;
  e_time: any;
  link: any;
  code: any;
  img_text: any;
  blogs: boolean;
  podcast: boolean;
  type: any;
  items: any = [];
  section: any = [];
  resource: boolean;
  public searchControl: FormControl;
  private subscriptions: Subscription[] = [];

  img: any;
  name: any;
  token = '';

  userList: any;
  communityList = [];
  allUsersList: any;

  resourceType: any;

  community_id: any;
  communityInfo: any;

  communityTitle = '';
  communityDescription = '';
  communityImage: any;

  disable: boolean;

  communityIds = [];

  @ViewChild("search", {static: false})
  public searchElementRef: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpClient,
    public storage: Storage,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public popoverCtrl: PopoverController,
    public toastCtrl: ToastController,
    private imagePicker: ImagePicker,
    public cropService: Crop,
    private transfer: FileTransfer,
    private auth: AuthService
  ) {

  }

  ngOnInit() {
    this.subscriptions.push(
      this.auth.me()
        .subscribe(user => {
          this.id = user.id;
          this.img = user.img;
          this.name = user.firstname;
          this.community_id = user.communities[0].community_id;
          user.communities.map( r => {
            if( r.role == "admin") {
              this.communityIds.push(r.community_id)
            }
          });
        })
    )
    this.auth.token.pipe(
      filter(token => !!token)
    )
    .subscribe(token => {
      this.token = token;
    }); 
    this.approve = false;
    this.channel = false;
    this.select = true;
    this.events = false;
    this.img_text = 'Select';
    this.blogs = false;
    this.link = '';
    let headers = new Headers();
    headers.append('Authorization', this.token);
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    this.options = { headers: headers };
    this.feat = this.navParams.get('feat');


    switch (this.feat) {
      case 'Add Company Details':
        this.editCommunityDetails();
        break;
      case 'Approve users':
        this.fetch_users();
        break;
      case 'Manage channels':
        this.fetch_channels();
        break;
      case 'Manage users':
        this.getAllUsers();
        break;
      case 'Add event (Coming Soon)':
        this.events = true;
        break;
      case 'Add Resources':
        this.resource = true;
        break;
      default:
        return null;
    }

  }

  fetch_users() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait',
      duration: 1000
    });
    this.loading.present();
    // let data = JSON.stringify({ uid: this.id });
    this.http.get(ENV.BASE_URL + 'api/communities/requests', this.options).subscribe(result => {
      this.userList = JSON.parse(result["_body"]);
      this.approve = true;
    }, error => {
      console.log(error);
    });
  }

  approveUser(user) {
    this.http.put(ENV.BASE_URL + 'api/communities/requests/'+ user.request_id +'/accept', this.options).subscribe(r => {
      this.presentAlert(user.firstname, 'User has been added');
      this.fetch_users();
    });
  }

  declineUser(user) {
    this.http.put(ENV.BASE_URL + 'api/communities/requests/'+ user.request_id +'/decline', this.options).subscribe(r => {
      this.presentAlert(user.firstname, 'User has been declined');
      this.fetch_users();
    });
  }

  removeUserFromComm(user) {

    this.http.delete(ENV.BASE_URL + 'api/communities/users/' + user.id, this.options).subscribe(r => {
      let alert = this.alertCtrl.create({
        title: name,
        subTitle: 'User has been deleted',
      });
      alert.present();
      this.getAllUsers();
    });

  }

  presentAlert(name, text) {
    let alert = this.alertCtrl.create({
      title: name,
      subTitle: text,
    });
    alert.present();
  }

  fetch_channels() {
    this.communityList = [];
    this.loading = this.loadingCtrl.create({
      content: 'Please wait',
      duration: 1000
    });
    this.loading.present();
    this.http.get(ENV.BASE_URL + 'api/communities/', this.options).subscribe(result => {
      this.channel = true;
      // this.communityList = JSON.parse(result["_body"]);
      let communityList = JSON.parse(result["_body"]);
      communityList.map( (r, i) => {
        r.communities.map( (el, j) => {
          if( el != null) {
            this.communityIds.map( ids => {
              if( el.id == ids) {
                this.communityList.push(el);
              };
            });
          };
        });
      });
    }, error => {
      console.log(error);
    });
    this.showAdminCommunity()
  }

  createChannel() {
    let profileModal = this.modalCtrl.create('CreateChannelModal', { comm_id: this.community_id });
    profileModal.onDidDismiss(data => {
      if (data != null) { // check if null in case user quits the page and dont select a city
        this.fetch_channels();
      }
    });
    profileModal.present();
  }

  removeChannel(id) {
    this.http.delete(ENV.BASE_URL + 'api/channels/' + id, this.options).subscribe(result => {
      let alert = this.alertCtrl.create({
        title: name,
        subTitle: 'Channel has been deleted',
      });
      alert.present();
      this.fetch_channels();
    }, error => {
      console.log(error);
    });
  }

  editChannel(id) {
    let profileModal = this.modalCtrl.create('EditChannelModal', { community_id: this.community_id, channel_id: id });
    profileModal.onDidDismiss(data => {
      if (data != null) { // check if null in case user quits the page and dont select a city
        this.fetch_channels();
      }
    });
    profileModal.present();
  }


  showAdminCommunity() {
  }

  editCommunityDetails() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait',
      duration: 1000
    });
    this.loading.present();
    this.comp = true;
  }

  // getImage() {
  //   var options = {
  //     maximumImagesCount: 1,
  //     width: 400,
  //     height: 400,
  //     quality: 80,
  //     outputType: 1
  //   };
  //   this.imagePicker.getPictures(options)
  //     .then((results) => {
  //       console.log(results)
  //       this.communityImage = 'data:image/jpeg;base64,' + results;
  //       let alert = this.alertCtrl.create({
  //           title: name,
  //           subTitle: 'Image uploaded',
  //         });
  //         alert.present();
  //     }, (err) => {
  //       this.communityImage = null;
  //      });
  // }

  getImage() {
    this.select = true;
    this.imagePicker.requestReadPermission().then(() => {
      var options = {
        maximumImagesCount: 1,
        width: 800,
        height: 800,
        quality: 80,
      };
      this.imagePicker.hasReadPermission().then(hasReadPermission => {
        if (hasReadPermission) {
          this.imagePicker.getPictures(options)
            .then((results) => {
              this.reduceImages(results).then((croppedImage) => {
                if (croppedImage != null) {
                  let d = new Date();
                  this.time = d.getTime();
                  this.logo = ENV.BASE_URL + 'public/logo/' + this.time + '.jpg';
                  this.picture = croppedImage;
                  this.communityImage = croppedImage;
                  this.img_text = 'Selected';
                  let alert = this.alertCtrl.create({
                    title: name,
                    subTitle: 'Image uploaded',
                  });
                  alert.present();
                }
                else
                  return;

              });
            });
        }
      }, (err) => {
      });

    }, (err) => {
    });

  }

  reduceImages(selected_pictures: any): any {
    return selected_pictures.reduce((promise: any, item: any) => {
      return promise.then((result) => {
        return this.cropService.crop(item, { quality: 80 })
          .then(cropped_image => cropped_image);
      });
    }, Promise.resolve());
  }

  transfer_img(path: any): any {
    const fileTransfer: FileTransferObject = this.transfer.create();
    let options_f: FileUploadOptions = {
      fileKey: 'file',
      mimeType: "multipart/form-data",
      fileName: this.time + '.jpg',
      chunkedMode: false,
    };
    let url = '';
    if (this.resource) {
      url = ENV.BASE_URL + 'upload_blogImage.php';
    } else {
      url = ENV.BASE_URL + 'upload_logo.php';
    }
    fileTransfer.upload(path, url, options_f)
      .then((data) => {
      });
  }

  editCommunity() {
    let modal = this.modalCtrl.create('EditCommunityModal', {
      community_id: this.community_id,
      communityImage: this.communityImage,
      communityTitle: this.communityTitle,
      communityDescription: this.communityDescription
    });
    modal.onDidDismiss(data => {
      let alert = this.alertCtrl.create({
        title: name,
        subTitle: 'Community info edited',
      });
      alert.present();
    });
    modal.present();
  }


  getAllUsers() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait',
      duration: 1000
    });
    this.loading.present();
    this.http.get(ENV.BASE_URL + 'api/communities/' + this.community_id + '/users', this.options).subscribe(result => {
      let community = JSON.parse(result["_body"]);
      this.allUsersList = community.users;
      this.show_users = true;
    }, error => {
      console.log(error);
    });
  }

  // add_event() {
  //   if (this.picture == null || this.title == null || this.summary == null || this.address == null || this.s_date == null || this.s_time == null || this.e_date == null || this.e_time == null) {
  //     this.toastCtrl.create({
  //       message: 'Please fill in all the details',
  //       duration: 3000
  //     }).present();
  //   }
  //   else {
  //     this.transfer_img(this.picture);
  //     let img = this.time + '.jpg';
  //     this.storage.get('id').then(id => {
  //       this.id = id;
  //       let data = JSON.stringify({ id: this.id, title: this.title, summary: this.summary, image: img, city: this.city, address: this.address, s_date: this.s_date, s_time: this.s_time, e_date: this.e_date, e_time: this.e_time, link: this.link, lat: this.lat, long: this.long, code: this.code });
  //       this.http.post(ENV.BASE_URL + 'webservices/Hobby2/add_event', data, this.options).subscribe(result => {
  //         this.toastCtrl.create({
  //           message: 'The event has been added',
  //           duration: 2000
  //         }).present();
  //         this.navCtrl.pop();
  //       });
  //     });
  //   }
  // }

  addResource() {
    this.resource = true;
  }

  toggleSelect(e) {
   
  }

  add_blog() {

    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let options = { headers: headers };

    if (this.picture == null || this.title == null || this.summary == null || this.description == null) {
      this.toastCtrl.create({
        message: 'Please fill in all the details',
        duration: 1000
      }).present();
    }
    else {
      this.transfer_img(this.picture);
      let img = ENV.BASE_URL + 'public/featureofmonth_images/' + this.time + '.jpg';

        let data = JSON.stringify({ id: this.id, img: img, title: this.title, short_description: this.summary, content: this.description, link: this.link, type: this.type });
        this.http.post(ENV.BASE_URL + 'webservices/Hobby2/add_blog', data, options).subscribe(result => {
          this.toastCtrl.create({
            message: 'It has been added under career advice',
            duration: 2000
          }).present();
          this.navCtrl.pop();
      });
    }
  }

  add_podcast() {

    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let options = { headers: headers };

    let data = JSON.stringify({ title: this.title, link: this.link });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/add_podcast', data, options).subscribe(result => {
      this.toastCtrl.create({
        message: 'The podcast has been added',
        duration: 2000
      }).present();
      this.navCtrl.pop();
    });
  }

  add_newsletter() {

    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let options = { headers: headers };

    let data = JSON.stringify({ title: this.title, content: this.summary });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/add_newsletter', data, options).subscribe(result => {
      this.toastCtrl.create({
        message: 'The newsletter has been added',
        duration: 2000
      }).present();
      this.navCtrl.pop();
    });
  }

  openProfile(id1) {
    // let modal = this.modalCtrl.create('PersonPage', { id: id1 });
    // modal.onDidDismiss(data => {

    // });
    // modal.present();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(item => item.unsubscribe());
  }

}
