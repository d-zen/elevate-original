import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, ModalController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { environment as ENV } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';


@IonicPage()
@Component({
  selector: 'page-company',
  templateUrl: 'company.html',
})
export class CompanyPage {

  comm_id: any;
  comm_name: any;
  companies: any = [];
  request: any = [];
  id: any;
  lname: any;
  uname: any;
  uimg: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public http: HttpClient, public storage: Storage, public toastCtrl: ToastController, public modalCtrl: ModalController) {

    this.comm_id = this.navParams.get('comm_id');
    this.comm_name = this.navParams.get('comm_name');
    this.storage.get('uname').then(u=>{
      this.uname = u;
    });
    this.storage.get('lname').then(u=>{
      this.lname = u;
    });
    this.storage.get('uimg').then(u=>{
      this.uimg = u;
    });

    var reptRef = firebase.database().ref();
    reptRef.on('value', personSnapshot => {
      let myData = personSnapshot.val();
      let obj = myData.community;
      for(let i=0;i<obj.length;i++)
      {
        if(obj[i].comm_id == this.comm_id)
          this.companies = obj[i].companies;
      }
      this.companies.sort(function(a, b){
          if(a.company < b.company) { return -1; }
          if(a.company > b.company) { return 1; }
            return 0;
      });
      this.storage.get('id').then(id=>{
        this.id = id;
        for(let k=0;k<this.companies.length;k++)
        {
          let cid = this.companies[k].cid;
          let headers = new HttpHeaders();
          headers.append('Content-Type', 'application/json');
          headers.append('Accept', 'application/json');
          let options = { headers: headers };
          let data = JSON.stringify({ uid: this.id, cid: cid, tid: this.comm_id });
          this.http.post(ENV.BASE_URL + 'webservices/Hobby2/get_user_request', data, options).subscribe(result => {
            let user = JSON.parse(result["_body"]);
            if(user == 0)
              this.request[k] = '';
            else
              this.request[k] = 'Requested';
          });
        }
      });
    });
  }

  company_in(cid, cname, desc, logo, email)
  {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let options = { headers: headers };

    this.storage.get('id').then(id=>{
      let data1 = JSON.stringify({ uid: id, cid: cid, tid: this.comm_id });
      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/get_user_comp', data1, options).subscribe(result => {
        let user = JSON.parse(result["_body"]);
        if(user != 0)
        {
          this.navCtrl.push('GroupsPage', {cid: cid, cname: cname, comm_id: this.comm_id});
        }
        else
        {
          let modal = this.modalCtrl.create('AdminMessagePage',{page: 'qna', desc: desc, logo: logo, comm_id: this.comm_id, cid: cid, fname: this.uname, lname: this.lname, img: this.uimg, cname: cname, email: email});
          modal.present();
        }
      });
    });    
  }

}
