import { NgModule } from '@angular/core';
import { ConnectPage } from './connect';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [ConnectPage],
  imports: [IonicPageModule.forChild(ConnectPage)],
  exports: [ConnectPage]
})
export class ConnectPageModule { }
