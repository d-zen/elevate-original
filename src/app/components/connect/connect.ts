import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController, IonicPage } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { environment as ENV } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-connect',
  templateUrl: 'connect.html',
})
export class ConnectPage {

	name: any;
  rid: any;
  sid: any;
  answer: string;
  disable: boolean;
  filter: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public http: HttpClient, public toastCtrl: ToastController, public storage: Storage ) {

    this.name = this.navParams.get('name');
    this.rid = this.navParams.get('rid');
    this.sid = this.navParams.get('sid');
    this.filter = this.navParams.get('filter');
    this.disable = false;
    if(this.filter == 'career')
      this.answer = 'I would love to connect and build on my career knowledge.';
    else if(this.filter == 'hobby')
      this.answer = 'I would love to connect and organise (hobby) together.'; 
    else if(this.filter == 'proximity')
      this.answer = 'You are around the corner from me, let\'s go for a coffee/drink?';
    else
      this.answer = 'I would like to connect with you.';

  }

  send()
  {
    var d = new Date();
    let date = d.toString().slice(4,15);
    this.storage.get(date).then(dt=>{
      if(dt == null || dt == '1' || dt == '2' || this.sid == '809')
      {
        let no = dt+1;
        this.storage.set(date,no);

        this.disable = true;
        let headers = new HttpHeaders();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        let options = {headers: headers};
        if(this.answer == '' || this.answer == null)
          this.answer = 'I would like to connect with you.';
        else
          this.storage.set('message',this.answer);
          
        let data = JSON.stringify({sid: this.sid, rid: this.rid, msg: this.answer});

        this.http.post(ENV.BASE_URL + 'webservices/Hobby2/connection',data,options).subscribe(result=>{
          let res = JSON.parse(result["_body"]);
          if(res.msg == 'not')
          {
            this.toastCtrl.create({
              message: 'The user may be a friend. Please check your messages',
              duration: 3000
            }).present();
            this.viewCtrl.dismiss({'dt': 'no'});
          }
          else
          {
            this.viewCtrl.dismiss({'dt': 'msg'});
          }
        });
      }
      else if(dt == '3' && this.sid != '809')
      {
        this.toastCtrl.create({
          message: 'You have reached the quota of connections you can request for the day',
          duration: 3000
        }).present();
      }
    });
  }

  closeModal()
  {
  	this.viewCtrl.dismiss({'dt': 'no'});
  }

}
