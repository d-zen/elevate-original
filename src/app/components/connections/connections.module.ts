import { NgModule } from '@angular/core';
import { ConnectionsPage } from './connections';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [ConnectionsPage],
  imports: [IonicPageModule.forChild(ConnectionsPage)],
  exports: [ConnectionsPage]
})
export class ConnectionsPageModule { }
