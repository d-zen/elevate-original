import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, ToastController, AlertController, Content, Platform, PopoverController, Events, IonicPage } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { SearchProvider } from '../../../services/search/search';
import * as firebase from 'firebase';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../services/auth.service';
import { environment as ENV } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Network } from '@ionic-native/network/ngx';

@IonicPage()
@Component({
  selector: 'page-connections',
  templateUrl: 'connections.html',
})
export class ConnectionsPage implements OnInit, OnDestroy {

  users: any = [];
  options: any;
  id: any;
  items: any = [];
  myInput: any;
  loading: any;
  is_ios: boolean;
  nouser: boolean;
  search: boolean;
  showdiv: boolean;
  current: boolean;
  pending: boolean;
  requested: boolean;
  @ViewChild(Content, {static: false}) content: Content;
  private subscriptions: Subscription[] = [];

  constructor(private auth: AuthService, public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public storage: Storage, public modalCtrl: ModalController, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public alertCtrl: AlertController, private network: Network, public popoverCtrl: PopoverController, public events: Events, public searchService: SearchProvider, public plt: Platform) {

    if (this.plt.is('ios'))
      this.is_ios = true;
    else
      this.is_ios = false;

    if (this.network.type == 'none') {
      this.nouser = true;
      console.log('Not connected');
      this.alertCtrl.create({
        title: 'No internet connection',
        subTitle: 'Get connected to the internet in order to use the app',
        buttons: ['OK']
      }).present();
    }
    this.search = false;
    this.current = true;

    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      duration: 10000
    });
    this.loading.present();


  }

  ngOnInit() {
    this.subscriptions.push(
      this.auth.me().subscribe(user => {
        this.id = user.id;
        this.events.subscribe('blocked', () => {
          this.get_connections();
        });

        this.loading.dismiss();
        let headers = new HttpHeaders();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        this.options = { headers: headers };
        this.get_connections();
      })
    );
  }

  set_current(str) {
    if (str == 'yes') {
      this.current = true;
      this.pending = false;
      this.requested = false;
      this.get_connections();
    }
    else if (str == 'no') {
      this.pending = true;
      this.current = false;
      this.requested = false;
      this.get_pending_requests();
    }
    else {
      this.current = false;
      this.pending = false;
      this.requested = true;
      this.get_sent_requests();
    }
  }

  get_pending_requests() {
    let data = JSON.stringify({ id: this.id });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/pending_requests', data, this.options).subscribe(result => {
      this.loading.dismiss();
      let dt = JSON.parse(result["_body"]);
      if (dt == '')
        this.nouser = true;
      else
        this.nouser = false;
      this.users = dt;
    });
  }

  get_sent_requests() {
    let data = JSON.stringify({ id: this.id });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/sent_requests', data, this.options).subscribe(result => {
      this.loading.dismiss();
      let dt = JSON.parse(result["_body"]);
      if (dt == '')
        this.nouser = true;
      else
        this.nouser = false;
      this.users = dt;
    });
  }

  srch() {
    this.search = true;
  }

  scroll_page_up() {
    setTimeout(() => {
      this.content.scrollToTop(500);
    }, 500);
  }

  presentPopover(myEvent, fid, name, i) {
    let popover = this.popoverCtrl.create('PopoverPage', { 'fid': fid, 'name': name, 'block': i, 'page': 'connections' });
    popover.present({
      ev: myEvent
    });
  }

  presentPopover1(myEvent, fid) {
    let popover = this.popoverCtrl.create('PopoverPage', { 'fid': fid, 'page': 'pending_c' });
    popover.present({
      ev: myEvent
    });
  }

  accept(fid) {
    let data = JSON.stringify({ receiver_id: fid, sender_id: this.id });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/accept_request', data, this.options)
      .subscribe(result => {
        this.set_current('yes');
        this.toastCtrl.create({
          message: 'Start messaging',
          duration: 4000
        }).present();
      });
  }

  reject(fid) {
    this.http.get(ENV.BASE_URL + 'webservices/Hobby2/thread?uid=' + this.id + '&fid=' + fid).subscribe(result => {
      let res = JSON.parse(result["_body"]);
      let thread = res.thread_id;
      let data = JSON.stringify({ sender_id: this.id, receiver_id: fid, tid: thread });
      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/reject_request', data, this.options)
        .subscribe(result => {
          this.set_current('no');
          this.update_friend(this.id, thread);
          this.update_friend(fid, thread);
          this.toastCtrl.create({
            message: 'You have rejected the request from this user',
            duration: 3000
          }).present();
        });
    });
  }

  update_friend(id, tid) {
    var reptRef = firebase.database().ref();
    reptRef.once('value', personSnapshot => {

      let myPerson = personSnapshot.val();
      let obj = myPerson.list;
      let fetched = obj[id];
      for (let i = 0; i < fetched.length; i++) {
        if (fetched[i] != null) {
          if (fetched[i].thread_id == tid) {
            var repRef = firebase.database().ref('/list/' + id + '/' + i + '/');
            repRef
              .update({
                friends: 'no'
              });
            return;
          }
        }
      }
    });
  }

  get_connections() {
    this.search = false;
    this.showdiv = false;
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      duration: 10000
    });
    this.loading.present();

    let data = JSON.stringify({ id: this.id });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/connected_users', data, this.options).subscribe(result => {
      this.loading.dismiss();
      let dt = JSON.parse(result["_body"]);
      console.log(dt);
      this.users = dt.myconnection;
      if (dt.myconnection == "")
        this.nouser = true;
      else
        this.nouser = false;
    });
  }

  view(id) {
    this.modalCtrl.create('PersonPage', { id: id, connect: 'no' }).present();
  }

  message(fid, name, lname, tid) {
    this.navCtrl.push('chatRoomPage', { receiverId: fid, firstName: name, lastName: lname, threadId: tid });
  }

  setFilteredItems() {
    this.items = this.searchService.filterItems(this.myInput);
    if (this.items.length > 0)
      this.showdiv = true;
    else
      this.showdiv = false;
  }

  search_key() {
    this.showdiv = false;
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      duration: 10000
    });
    this.loading.present();

    this.users = this.items;
    this.loading.dismiss();
  }

  openProfile(id) {
    this.modalCtrl.create('PersonPage', { id: id }).present();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(item => item.unsubscribe());
  }

}
