import { NgModule } from '@angular/core';
import { IonicPageModule } from '@ionic/angular';
import { CreateChannelModal } from './create-channel';

@NgModule({
  declarations: [CreateChannelModal],
  imports: [IonicPageModule.forChild(CreateChannelModal)],
  exports: [CreateChannelModal]
})
export class CreateChannelModalModule {}
