import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthService } from '../../../services/auth.service';
import { environment as ENV } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { filter } from 'rxjs/operators';

@IonicPage()
@Component({
  selector: 'page-create-channel',
  templateUrl: 'create-channel.html',
})
export class CreateChannelModal {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public http: HttpClient,
    public storage: Storage,
    public toastCtrl: ToastController,
    private auth: AuthService,
  ) { }

  token = '';
  comm_id: any;

  title = '';

  ngOnInit() {
    this.comm_id = this.navParams.get('comm_id');
    this.auth.token.pipe(
      filter(token => !!token)
    )
    .subscribe(token => {
      this.token = token;
    });
  }

  closeModal() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }

  inputChange(e) {
    console.log(e)
    this.title = e;
  }

  sendCreateRequest() {

    const headers = new HttpHeaders();

    headers.append('Authorization', this.token);
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    const header = { headers: headers };

    const options = {
      "community_id": this.comm_id,
      "title": this.title
    }

    this.http.post(ENV.BASE_URL + 'api/channels', options, header).subscribe(r => {
      console.log('created');
      this.closeModal();
    })

  }

}
