import { NgModule } from '@angular/core';
import { EditCareerPage } from './edit-career';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [EditCareerPage],
  imports: [IonicPageModule.forChild(EditCareerPage)],
  exports: [EditCareerPage]
})
export class EditCareerPageModule { }
