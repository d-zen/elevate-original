import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, IonicPage } from '@ionic/angular';
import { ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
// import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import { environment as ENV } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { google } from "@agm/core/services/google-maps-types";

@IonicPage()
@Component({
  selector: 'page-edit-career',
  templateUrl: 'edit-career.html',
})
export class EditCareerPage {

  id: any;
  info: any = {};
  address: any;
  company: any;
  job: any;
  c_size: any;
  level: any;
  s_level: any;
  show_level: boolean;
  levels: any = [];
  max: any;
  years: any = [];
  s_levels: any = [];
  months: any = [];
  current: any;
  t_year: any;
  f_year: any;
  t_month: any;
  f_month: any;
  curr: boolean;
  wrk: boolean;
  experience: any;
  public searchControl: FormControl;
  cid: any;
  company_desc: any;
  showfinance: boolean;
  industry: any = [];
  industries: any = [];
  show1: boolean;
  show2: boolean;
  show3: boolean;
  show4: boolean;
  show5: boolean;
  blockchain: any;
  teach: any;
  invest: any = [];
  tech: any = [];
  finance: any = [];
  ind1: any;
  ind2: any;
  ind3: any;
  finance_level: any;
  other: any;
  check: any = [];
  ch_ind: any = [];

  @ViewChild("search", {static: false})
  public searchElementRef: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpClient,
    public viewCtrl: ViewController,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) {

    this.levels = ['Intern', 'Analyst', 'Team Leader/Associate/Product Specialist', 'Manager of one team', 'Manager of more than one team', 'Head of one department', 'Head of more than one department', 'CEO/CFO/COO/Board Member/Non Executive Directors'];

    this.max = new Date().getFullYear();
    var min = this.max - 68;
    for (var i = this.max; i >= min; i--)
      this.years.push(i);

    this.months = ['January', 'Februrary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    this.s_levels = ['Internship', 'Analyst', 'Associate', 'Manager', 'Vice President/Assistant Vice President', 'Director', 'Executive Director', 'Managing Director', 'Partner', 'None'];

    this.industries = ['Founder', 'Investor', 'Tech', 'Finance', 'Insurance', 'Consultancy', 'Financial Law', 'Other']

    this.invest = ['Venture Capital', 'Angel'];

    this.tech = ['Tech', 'Fintech', 'Regtech', 'Insurtech', 'Healthtech'];

    this.finance = ['Finance', 'Asset-based finance and leasing', 'Funds management/ Superannuation', 'Hedge funds', 'Proprietary trading', 'Investment banking', 'Payment systems, clearing and settlement', 'Private banking', 'Private equity', 'Retail banking', 'Wealth Management'];

    this.blockchain = ['Happy to teach others', 'Can speak on conferences/panels', 'Can write articles/blogs'];

    this.ind1 = '';
    this.ind2 = '';
    this.ind3 = '';
    this.other = '';
    this.company_desc = '';
    this.finance_level = '';
    this.teach = '';

    this.id = this.navParams.get('id');
    this.teach = this.navParams.get('skill');
    this.check = this.navParams.get('skill_arr');
    if (this.check == '')
      this.check = [];
    this.info = this.navParams.get('info');

    this.company = this.info.company_name;
    this.job = this.info.job_title;
    this.address = this.info.locations;
    this.f_year = this.info.start_year;
    this.f_month = this.info.start_month;
    this.t_year = this.info.end_year;
    this.t_month = this.info.end_month;
    this.current = this.info.iscurrent;
    this.level = this.info.level;
    this.cid = this.info.career_id;
    this.s_level = this.info.other_level;
    this.ch_ind = this.info.industry;
    if (this.ch_ind.includes('Finance')) {
      this.industry.push('Finance');
      this.show3 = true;
      this.showfinance = true;
      this.ind3 = this.info.finance_office;
      this.finance_level = this.info.finance;
    } else if (this.ch_ind.includes('Tech')) {
      this.industry.push('Tech');
      this.show2 = true;
      this.ind2 = this.info.tech;
    } else if (this.ch_ind.includes('Investor')) {
      this.industry.push('Investor');
      this.show1 = true;
      this.ind1 = this.info.investor;
    } else if (this.ch_ind.includes('Other')) {
      this.industry.push('Other');
      this.show4 = true;
      this.other = this.info.other;
    } else if (this.ch_ind.includes('Founder')) {
      this.industry.push('Founder');
      this.show5 = true;
      this.company_desc = this.info.comp_descrip;
    }

    if (this.info.company_size == '3' || this.info.company_size == 'Institution')
      this.c_size = 'Institution';
    else if (this.info.company_size == '2' || this.info.company_size == 'Mid-level')
      this.c_size = 'Mid-level';
    else
      this.c_size = 'Startup';

    this.show_level = !!this.s_level;

/*
    if (this.s_level == null || this.s_level == '')
      this.show_level = false;
    else
      this.show_level = true;
*/

    if (this.current == '1') {
      this.curr = true;
      this.wrk = true;
    } else {
      this.curr = false;
      this.wrk = false;
    }
  }

  ngOnInit() {

    //create search FormControl
    this.searchControl = new FormControl();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        console.log('commented');
        /*this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          this.address = place.formatted_address;

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
        });*/
      });
    });
  }

  update1(item, event) {
    if (event.checked) {
      if (item == '0') {
        if (this.check.includes('Happy to teach others'))
          return;
        else
          this.check.push('Happy to teach others');
      } else if (item == '1') {
        if (this.check.includes('Can speak on conferences/panels'))
          return;
        else
          this.check.push('Can speak on conferences/panels');
      } else if (item == '2') {
        if (this.check.includes('Can write articles/blogs'))
          return;
        else
          this.check.push('Can write articles/blogs');
      } else
        return;
    } else {
      if (item == '0')
        this.check.splice(this.check.indexOf('Happy to teach others'), 1);
      else if (item == '1')
        this.check.splice(this.check.indexOf('Can speak on conferences/panels'), 1);
      else if (item == '2')
        this.check.splice(this.check.indexOf('Can write articles/blogs'), 1);
      else
        return;
    }
  }

  radioChecked(i) {
    if (i == 3)
      this.show_level = true;
    else
      this.show_level = false;
  }

  radioChecked1(i, event) {
    if (event.checked) {
      if (this.industry.includes(this.industries[i]))
        return;
      else
        this.industry.push(this.industries[i]);

      if (i == 0) {
        this.show5 = true;
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.show4 = false;
        this.showfinance = false;
      } else if (i == 1) {
        this.show1 = true;
        this.show2 = false;
        this.show3 = false;
        this.show4 = false;
        this.show5 = false;
        this.showfinance = false;
      } else if (i == 2) {
        this.show2 = true;
        this.show1 = false;
        this.show3 = false;
        this.show4 = false;
        this.show5 = false;
        this.showfinance = false;
      } else if (i == 3) {
        this.show3 = true;
        this.show1 = false;
        this.show2 = false;
        this.show4 = false;
        this.show5 = false;
        this.showfinance = false;
      } else if (i == 7) {
        this.show4 = true;
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.show5 = false;
        this.showfinance = false;
      } else {
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.show4 = false;
        this.show5 = false;
        this.showfinance = false;
      }
    } else {
      this.industry.splice(this.industry.indexOf(this.industries[i]), 1);
      if (i == 0)
        this.show5 = false;
      else if (i == 1)
        this.show1 = false;
      else if (i == 2)
        this.show2 = false;
      else if (i == 3)
        this.show3 = false;
      else if (i == 7)
        this.show4 = false;
    }
  }

  radioChecked2(i) {
    this.showfinance = true;
  }

  changew(event) {
    if (event.checked) {
      this.current = '1';
      this.t_year = '';
      this.t_month = '';
      this.wrk = true;
    } else {
      this.current = '0';
      this.wrk = false;
    }
  }

  closeModal() {
    let dt = {'msg': 'no'};
    this.viewCtrl.dismiss(dt);
  }

  edit() {
    let word;
    if (this.level == 'Intern')
      word = 'Internship';
    else
      word = this.level;

    if (this.t_year == '')
      this.experience = this.max - this.f_year;
    else
      this.experience = this.t_year - this.f_year;

    if (this.c_size == 'Mid-level' || this.c_size == 'Startup')
      this.s_level = '';

    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let options = {headers: headers};
    /*let data = JSON.stringify({cid: this.cid, id: this.id, company_name: this.company, company_size: this.c_size, job_title: this.job, level: word, other_level: this.s_level, locations: this.address, start_month: this.f_month, start_year: this.f_year, end_month: this.t_month, end_year: this.t_year, iscurrent: this.current, total_experience: this.experience});*/

    let data = JSON.stringify({
      id: this.id,
      company_name: this.company,
      company_size: this.c_size,
      job_title: this.job,
      level: word,
      other_level: this.s_level,
      locations: this.address,
      start_month: this.f_month,
      start_year: this.f_year,
      end_month: this.t_month,
      end_year: this.t_year,
      iscurrent: this.current,
      total_experience: this.experience,
      industry: this.industry,
      ind1: this.ind1,
      ind2: this.ind2,
      ind3: this.ind3,
      other: this.other,
      description: this.company_desc,
      finance_level: this.finance_level,
      blockchain: this.teach,
      b_array: this.check
    });

    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/update_career', data, options).subscribe(result => {
      let dt = {'msg': 'yes'};
      this.viewCtrl.dismiss(dt);
    });

  }

}
