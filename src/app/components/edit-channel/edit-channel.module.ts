import { NgModule } from '@angular/core';
import { IonicPageModule } from '@ionic/angular';
import { EditChannelModal } from './edit-channel';

@NgModule({
  declarations: [EditChannelModal],
  imports: [IonicPageModule.forChild(EditChannelModal)],
  exports: [EditChannelModal]
})
export class EditChannelModalModule {}
