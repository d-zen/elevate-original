import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthService } from '../../../services/auth.service';
import { environment as ENV } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { filter } from 'rxjs/operators';

@IonicPage()
@Component({
  selector: 'page-edit-channel',
  templateUrl: 'edit-channel.html',
})
export class EditChannelModal {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public http: HttpClient,
    public storage: Storage,
    public toastCtrl: ToastController,
    private auth: AuthService,
  ) { }

  token = '';
  channel_id: any;
  community_id: any;

  title = '';

  ngOnInit() {
    this.channel_id = this.navParams.get('channel_id');
    this.community_id = this.navParams.get('community_id');
    this.auth.token.pipe(
      filter(token => !!token)
    )
    .subscribe(token => {
      this.token = token;
    });
  }

  closeModal() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }

  inputChange(e) {
    this.title = e;
  }

  sendEditRequest() {

    const headers = new HttpHeaders();

    headers.append('Authorization', this.token);
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    const header = { headers: headers };

    const options = {
      "title": this.title
    }

    this.http.put(ENV.BASE_URL + 'api/channels/' + this.channel_id , options, header).subscribe(r => {
      console.log('created');
      this.closeModal();
    })

  }

}
