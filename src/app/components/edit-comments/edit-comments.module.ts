import { NgModule } from '@angular/core';
import { IonicPageModule } from '@ionic/angular';
import { EditCommentsPage } from './edit-comments';

@NgModule({
  declarations: [EditCommentsPage],
  imports: [IonicPageModule.forChild(EditCommentsPage)],
  exports: [EditCommentsPage]
})
export class EditCommentsPageModule {}
