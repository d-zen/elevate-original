import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, LoadingController, Events } from '@ionic/angular';
import * as firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-edit-comments',
  templateUrl: 'edit-comments.html',
})
export class EditCommentsPage {

	comm_id: any;
	cid: any;
	gid: any;
	qid: any;
	qst: any;
  name: any;
  channel: any;
	loading: any;
  obj: any = [];
  q_obj: any = [];
	answers: any = [];
  comments: boolean;
  topic: boolean;
  len: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController, public loadingCtrl: LoadingController, public events: Events) {

  	this.comm_id = this.navParams.get('comm_id');
  	this.cid = this.navParams.get('cid');
  	this.gid = this.navParams.get('gid');
  	this.qid = this.navParams.get('qid');
  	this.qst = this.navParams.get('qst');
    this.q_obj = this.navParams.get('obj');
    let page = this.navParams.get('page');
  	if(page == 'comments')
    {
      this.name = 'Comments';
      this.comments = true;
      this.load_comments();
    }
    else
    {
      this.name = 'Select channel';
      this.topic = true;
      this.load_channels();
    }

  }

  load_comments()
  {
    this.loading = this.loadingCtrl.create({
        content: 'Please wait',
        duration: 10000
    });
    this.loading.present();

  	var reptRef = firebase.database().ref();
    reptRef.on('value', personSnapshot => {
      this.answers = [];
      let myData = personSnapshot.val();
      let obj1 = myData.questions;
      for(var i in obj1)
      {
        if(obj1[i] != null && obj1[i].id == this.qid)
        {
          let ans = obj1[i].answers;
          for(var j in ans)
            this.answers.push(ans[j]);
          console.log(this.answers);
          this.loading.dismiss();
        }
      }
    });
  }

  presentPopover(event, aid)
  {
  	let popover = this.popoverCtrl.create('PopoverPage', {'page': 'comments', 'sid': this.qid, 'comm_id': this.comm_id, 'gid': this.gid, 'cid': this.cid, 'aid': aid});
    popover.onDidDismiss(res=>{
      if(res.msg == 'done')
        this.load_comments();
    });
    popover.present();
  }

  load_channels()
  {
    var reptRef = firebase.database().ref();
    reptRef.once('value', personSnapshot => {
      let myData = personSnapshot.val();
      let obj1 = myData.community;
      for(let i=0;i<obj1.length;i++)
      {
        if(obj1[i]!=null && obj1[i].comm_id == this.comm_id)
        {
          let obj2 = obj1[i].companies;
          for(let j=0;j<obj2.length;j++)
          {
            if(obj2[j]!=null && obj2[j].cid == this.cid)
              this.obj = obj2[j].groups;
          }
        }
      }
    });
  }

  move_c()
  {
    var reptRef1 = firebase.database().ref();
    reptRef1.once('value', personSnapshot => {
      let myData = personSnapshot.val();
      let obj1 = myData.community;
      for(let i=0;i<obj1.length;i++)
      {
        if(obj1[i] != null && obj1[i].comm_id == this.comm_id)
        {
          let obj2 = obj1[i].companies;
          for(let j=0;j<obj2.length;j++)
          {
            if(obj2[j] != null && obj2[j].cid == this.cid)
            {
              let obj3 = obj2[j].groups;
              for(let k=0;k<obj3.length;k++)
              {
                if(obj3[k] != null && obj3[k].gid == this.gid)
                {
                  let obj4 = obj3[k].questions;
                  for(var l in obj4)
                  {
                    if(obj4[l] != null && obj4[l].id == this.qid)
                    {
                      var repRef = firebase.database().ref('/community/'+i+'/companies/'+j+'/groups/'+k+'/questions/'+l+'/');
                      repRef.remove();
                    }
                  }
                }
              }
            }
          }
        }
      }

      for(let i=0;i<obj1.length;i++)
      {
        if(obj1[i] != null && obj1[i].comm_id == this.comm_id)
        {
          let obj2 = obj1[i].companies;
          for(let j=0;j<obj2.length;j++)
          {
            if(obj2[j] != null && obj2[j].cid == this.cid)
            {
              let obj3 = obj2[j].groups;
              for(let k=0;k<obj3.length;k++)
              {
                if(obj3[k] != null && obj3[k].gid == this.channel)
                {
                  let obj4 = obj3[k].questions;
                  if(obj4 == null)
                  {
                    var reportRef = firebase.database().ref('/community/'+i+'/companies/'+j+'/groups/'+k+'/');
                    var usersRef = reportRef.child('questions/0');
                    usersRef
                    .set({
                      answers: this.q_obj['answers'],
                      date: this.q_obj['date'],
                      uimg: this.q_obj['uimg'],
                      id: this.q_obj['id'],
                      uname: this.q_obj['uname'],
                      qst: this.q_obj['qst'],
                      userid: this.q_obj['userid']
                    });
                  }
                  else
                  {
                      this.len=0;
                      var reportRef1 = firebase.database().ref('/community/'+i+'/companies/'+j+'/groups/'+k+'/questions/');
                      var usersRef1 = reportRef1.child(this.len);
                      usersRef1
                    .set({
                      answers: this.q_obj['answers'],
                      date: this.q_obj['date'],
                      uimg: this.q_obj['uimg'],
                      id: this.q_obj['id'],
                      uname: this.q_obj['uname'],
                      qst: this.q_obj['qst'],
                      userid: this.q_obj['userid']
                    });
                  }
                }
              }
            }
          }
        }
      }
    });
    this.events.publish('dismiss');
    this.navCtrl.pop();
  }

}
