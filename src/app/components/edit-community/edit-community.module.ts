import { NgModule } from '@angular/core';
import { IonicPageModule } from '@ionic/angular';
import { EditCommunityModal } from './edit-community';

@NgModule({
  declarations: [EditCommunityModal],
  imports: [IonicPageModule.forChild(EditCommunityModal)],
  exports: [EditCommunityModal]
})
export class EditCommunityModalModule {}
