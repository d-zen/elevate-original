import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthService } from '../../../services/auth.service';
import { environment as ENV } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { filter } from 'rxjs/operators';

@IonicPage()
@Component({
  selector: 'page-edit-community',
  templateUrl: 'edit-community.html',
})
export class EditCommunityModal {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public http: HttpClient,
    public storage: Storage,
    public toastCtrl: ToastController,
    private auth: AuthService,
  ) { }

  token = '';
  community_id: any;
  communityImage: any;
  communityTitle: any;
  communityDescription: any;

  title = '';

  ngOnInit() {
    this.community_id = this.navParams.get('community_id');
    this.communityImage = this.navParams.get('communityImage');
    this.communityTitle = this.navParams.get('communityTitle');
    this.communityDescription = this.navParams.get('communityDescription');
    this.auth.token.pipe(
      filter(token => !!token)
    )
      .subscribe(token => {
        this.token = token;
      });
  }

  closeModal() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }

  sendEditRequest() {

    const headers = new HttpHeaders();

    headers.append('Authorization', this.token);
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    const header = { headers: headers };

    const options = {
      "title": this.communityTitle,
      "description": this.communityDescription,
      "image": this.communityImage
    }

    console.log(options)

    this.http.put(ENV.BASE_URL + 'api/communities/' + this.community_id, options, header).subscribe(r => {
      console.log('edited');
      this.closeModal();
    })

  }

}
