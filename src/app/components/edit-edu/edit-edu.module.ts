import { NgModule } from '@angular/core';
import { EditEduPage } from './edit-edu';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [EditEduPage],
  imports: [IonicPageModule.forChild(EditEduPage)],
  exports: [EditEduPage]
})
export class EditEduPageModule { }
