import { Component, ElementRef, NgZone, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, IonicPage } from '@ionic/angular';
import { FormControl } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import { environment as ENV } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { google } from "@agm/core/services/google-maps-types";

@IonicPage()
@Component({
  selector: 'page-edit-edu',
  templateUrl: 'edit-edu.html',
})
export class EditEduPage {

	degree: any;
	uni: any;
	address: any;
	id: any;
	years: any = [];
	info: any = [];
	eid: any;
  ed_year: any;
  public searchControl: FormControl;

  @ViewChild("search", {static: false})
  public searchElementRef: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public viewCtrl: ViewController, private ngZone: NgZone, private mapsAPILoader: MapsAPILoader) {

  	var max = new Date().getFullYear();
    var min = max-68;

  	for(var i=max; i>=min; i--)
  		this.years.push(i);

  	this.id = this.navParams.get('id');
  	this.info = this.navParams.get('info');
  	this.uni = this.info.university;
  	this.degree = this.info.degree;
  	this.address = this.info.country;
  	this.eid = this.info.education_id;
  	this.ed_year = this.info.year;
  }

  ngOnInit() {

    //create search FormControl
    this.searchControl = new FormControl();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["geocode"]
      });
      console.log('commented');
      /*autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          this.address = place.formatted_address;
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
        });
      });*/
    });
  }

  closeModal()
  {
    this.viewCtrl.dismiss({'msg': 'no'});
  }

  edit()
  {
  	let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let options = {headers: headers};
    let data = JSON.stringify({eid: this.eid, id: this.id, degree: this.degree, university: this.uni, year: this.ed_year, country: this.address});

    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/change_education',data,options).subscribe(result=>{
      this.viewCtrl.dismiss({'msg': 'yes'});
    });
  }

}
