import { NgModule } from '@angular/core';
import { EditHobbyPage } from './edit-hobby';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [EditHobbyPage],
  imports: [IonicPageModule.forChild(EditHobbyPage)],
  exports: [EditHobbyPage]
})
export class EditHobbyPageModule { }
