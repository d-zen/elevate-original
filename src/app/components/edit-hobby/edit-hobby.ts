import { Component, ElementRef, NgZone, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController, Events, IonicPage } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FormControl } from '@angular/forms';
// import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import { NativeGeocoder, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';
import * as firebase from 'firebase';
import { environment as ENV } from "../../../environments/environment";
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { google } from "@agm/core/services/google-maps-types";

@IonicPage()
@Component({
  selector: 'page-edit-hobby',
  templateUrl: 'edit-hobby.html',
})
export class EditHobbyPage {

  hobby: any = [];
  hobby2: any = [];
  questions: any = [];
  details: any = {};
  hobbies: any;
  id: any;
  name: any;
  lname: any;
  edit_name: boolean;
  edit_qst: boolean;
  edit_hob: boolean;
  edit_pass: boolean;
  edit_loc: boolean;
  add_qst: boolean;
  add_hobby: boolean;
  edit_ans: boolean;
  edit_cquest: boolean;
  incomplete: boolean;
  open_sug: boolean;
  showdiv: boolean;
  toggle5: any;
  toggle6: any;
  answer: any;
  question: any;
  options: any;
  further_edu: any;
  industry: any;
  mother: any;
  leave: any;
  relocated: any;
  email: any;
  mentor: any;
  o_pwd: any;
  n_pwd: any;
  address: any;
  lat: any;
  long: any;
  public searchControl: FormControl;
  description: any;
  quest: any;
  comm: any;
  gid: any;
  qid: any;
  aid: any;
  tab: any;
  cid: any;
  show_b1: boolean;

  @ViewChild("search", {static: false})
  public searchElementRef: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public http: HttpClient, public storage: Storage, public toastCtrl: ToastController, private nativeGeocoder: NativeGeocoder, private mapsAPILoader: MapsAPILoader, private ngZone: NgZone, public events: Events) {

    this.questions = ['My last water cooler conversation was...', 'My most awkward conversation I had at work was...', 'My next holiday I want to go on is... ', 'When I was younger I wanted to grow up and be...', 'My unusual skill is...', 'I geek out on...', 'The most spontaneous thing I have done is...', 'A unique fact about me is...', 'This year I really want to...', 'After work you can find me...', 'The award I should be nominated for is...', 'My best travel story is...', 'I recently discovered that...'];

    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    this.options = {headers: headers};

    this.id = this.navParams.get('id');
    let field = this.navParams.get('field');
    this.address = this.navParams.get('loc');
    let other_field = this.navParams.get('other');
    this.open_sug = (other_field == 'close');
    /*
        if (other_field == 'close')
          this.open_sug = true;
        else
          this.open_sug = false;
    */

    this.edit_qst = false;
    this.edit_hob = false;
    this.edit_name = false;
    this.add_qst = false;
    this.add_hobby = false;
    this.incomplete = false;
    this.edit_pass = false;
    this.edit_loc = false;
    this.edit_ans = false;
    this.edit_cquest = false;
    this.show_b1 = false;
    if (field == 'hobby') {
      this.edit_hob = true;
      this.hobbies = this.navParams.get('info');
    } else if (field == 'add_hobby') {
      this.add_hobby = true;
      this.hobbies = this.navParams.get('info');
    } else if (field == 'incomplete') {
      this.incomplete = true;
      this.further_edu = 'not_say';
      this.industry = 'not_say';
      this.mother = '2';
      this.leave = '2';
      this.relocated = 'not_say';
    } else if (field == 'pass') {
      this.edit_pass = true;
    } else if (field == 'questions') {
      this.edit_qst = true;
      this.details = this.navParams.get('details');
      this.answer = this.details.iceanswer;
      this.question = this.details.icequestion;

      if (this.details.do_you_have_children == '0')
        this.mother = '0';
      else if (this.details.do_you_have_children == '1')
        this.mother = '1';
      else
        this.mother = '2';

      if (this.details.are_you_currently_on_maternity == '0')
        this.leave = '0';
      else if (this.details.are_you_currently_on_maternity == '1')
        this.leave = '1';
      else
        this.leave = '2';

      if (this.details.can_we_contact == 0) {
        this.email = false;
        this.toggle5 = 'No';
      } else {
        this.email = true;
        this.toggle5 = 'Yes';
      }

      if (this.details.career_change_and_moved_industry == 'no')
        this.industry = 'no';
      else if (this.details.career_change_and_moved_industry == 'yes')
        this.industry = 'yes';
      else
        this.industry = 'not_say';

      if (this.details.changed_regions_and_uprooted_family == 'no')
        this.relocated = 'no';
      else if (this.details.changed_regions_and_uprooted_family == 'yes')
        this.relocated = 'yes';
      else
        this.relocated = 'not_say';

      if (this.details.user_mentor == '0') {
        this.toggle6 = 'No';
        this.mentor = false;
      } else {
        this.toggle6 = 'Yes';
        this.mentor = true;
      }

      if (this.details.additional_qualifications == 'no')
        this.further_edu = 'no';
      else if (this.details.additional_qualifications == 'yes')
        this.further_edu = 'yes';
      else
        this.further_edu = 'not_say';
    } else if (field == 'loc') {
      this.edit_loc = true;
      this.address = this.navParams.get('location');
    } else if (field == 'h_location') {
      this.edit_loc = true;
      this.address = this.navParams.get('address');
    } else if (field == 'edit_ans') {
      this.edit_ans = true;
      this.qid = this.navParams.get('qid');
      this.aid = this.navParams.get('aid');
      this.description = this.navParams.get('ans');
      this.events.publish('change_title', 'Edit your answer');
    } else if (field == 'edit_cquest') {
      this.edit_cquest = true;
      this.qid = this.navParams.get('qid');
      this.cid = this.navParams.get('cid');
      this.quest = this.navParams.get('quest');
      this.gid = this.navParams.get('gid');
      this.tab = this.navParams.get('tab');
    } else {
      this.edit_name = true;
      this.name = this.navParams.get('name');
      this.lname = this.navParams.get('lname');
    }

    this.hobby = ['Arts & culture', 'Charity', 'Gym', 'High intensity work outs (Barrys/ Tough Mudder/Iron Woman)', 'Low intensity workout (yoga/pilates)', 'Outdoor activities (running/cycling/golf/hiking)', 'Play dates with kids', 'Reading', 'Team sports', 'Travel', 'Wine tasting', 'Winter activities (Skiing/snowboarding)'];
    this.hobby2 = ['Diversity and Inclusion advocate', 'Gender equality speaker panel', 'Women&apos;s Network'];

  }

  ngOnInit() {

    //create search FormControl
    this.searchControl = new FormControl();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["geocode"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {

          console.log('commented');
          //get the place result
          /*let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          this.address = place.formatted_address;

          this.nativeGeocoder.forwardGeocode(this.address).then((coordinates: NativeGeocoderResult[]) => {
            this.lat = coordinates[0].latitude;
            this.long = coordinates[0].longitude;
          });

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }*/
        });
      });
    });
  }

  update(i, event) {
    if (event.checked) {
      if (this.hobbies == '')
        this.hobbies[0] = i;
      else
        this.hobbies.push(i);
    } else
      this.hobbies.splice(this.hobbies.indexOf(i), 1);
  }

  update2(i, event) {
    if (event.checked) {
      if (this.hobbies == '')
        this.hobbies[0] = i;
      else
        this.hobbies.push(i);
    } else
      this.hobbies.splice(this.hobbies.indexOf(i), 1);
  }

  closeModal() {
    this.viewCtrl.dismiss({'msg': 'no'});
  }

  edit_h() {
    let data = JSON.stringify({id: this.id, hobby: this.hobbies, loc: this.address});
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/edit_hobby', data, this.options).subscribe(result => {
      this.viewCtrl.dismiss({'msg': 'yes'});
    });
  }

  add_h() {
    if (this.address == null) {
      this.toastCtrl.create({
        message: 'Please enter the location',
        duration: 2000
      }).present();
    } else {
      this.storage.get('id').then(id => {
        let data = JSON.stringify({id: id, hobby: this.hobbies, loc: this.address});
        this.http.post(ENV.BASE_URL + 'webservices/Hobby2/add_hobby', data, this.options).subscribe(result => {
          this.storage.set('hobbies_added', '1');
          this.navCtrl.pop();
        });
      });
    }
  }

  edit_n() {
    let data = JSON.stringify({id: this.id, firstname: this.name, lastname: this.lname});
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/change_name', data, this.options).subscribe(result => {
      this.viewCtrl.dismiss({'msg': 'yes', fname: this.name, lname: this.lname});
    });
  }

  tog5(event) {
    if (event.checked)
      this.toggle5 = 'Yes';
    else
      this.toggle5 = 'No';
  }

  tog6(event) {
    if (event.checked)
      this.toggle6 = 'Yes';
    else
      this.toggle6 = 'No';
  }

  edit_pro() {
    if (this.mentor)
      this.mentor = '1';
    else
      this.mentor = '0';
    if (this.email)
      this.email = '1';
    else
      this.email = '0';
    let data = JSON.stringify({
      id: this.id,
      mentor: this.mentor,
      additional_qualifications: this.further_edu,
      moved_industry: this.industry,
      regions: this.relocated,
      children: this.mother,
      maternity: this.leave,
      email: this.email,
      iceanswer: this.answer,
      icequestion: this.question
    });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/update_all_questions', data, this.options).subscribe(result => {
      if (!this.open_sug)
        this.viewCtrl.dismiss({'msg': 'yes'});
      else
        this.navCtrl.setRoot('ProfilesPage', {id: this.id});
    });
  }

  radio_change(event) {
    if (this.further_edu == 'yes')
      this.showdiv = true;
    else
      this.showdiv = false;
  }

  update_pwd() {
    let data = JSON.stringify({id: this.id, old: this.o_pwd, new: this.n_pwd});
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/change_password', data, this.options).subscribe(result => {
      let res = JSON.parse(result["_body"]);
      if (res.msg == "Your Password Updated successfully") {
        this.toastCtrl.create({
          message: 'Updated successfully',
          duration: 3000
        }).present();
        this.viewCtrl.dismiss({'msg': 'yes'});
      } else {
        this.toastCtrl.create({
          message: 'Incorrect Password',
          duration: 3000
        }).present();
      }
    });
  }

  done(pg) {
    this.show_b1 = true;
    let ment, em;
    if (this.mentor)
      ment = '1';
    else
      ment = '0';
    if (this.email)
      em = '1';
    else
      em = '0';
    let data = JSON.stringify({
      id: this.id,
      mentor: ment,
      additional_qualifications: this.further_edu,
      moved_industry: this.industry,
      regions: this.relocated,
      children: this.mother,
      maternity: this.leave,
      email: em
    });
    if (pg == 'pro') {
      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/all_questions', data, this.options).subscribe(result => {
        this.viewCtrl.dismiss({'page': 'end'});
      });
    } else {
      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/all_questions', data, this.options).subscribe(result => {
        this.viewCtrl.dismiss({'page': 'next'});
      });
    }
  }

  update_address() {
    let n = this.navParams.get('field');
    if (n == 'h_location') {
      let data = JSON.stringify({id: this.id, location: this.address});
      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/change_hobbies_location', data, this.options).subscribe(result => {
      });
      this.viewCtrl.dismiss({'msg': 'yes'});
    } else {
      let data = JSON.stringify({id: this.id, location: this.address, lat: this.lat, long: this.long});
      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/change_location', data, this.options).subscribe(result => {
      });
      this.viewCtrl.dismiss({'msg': 'yes'});
    }
  }

  closePage() {
    this.navCtrl.pop();
  }

  edit_answer() {
    var reportRef = firebase.database().ref();
    reportRef.once('value', personSnapshot => {
      let myPerson = personSnapshot.val();
      let obj = myPerson.questions;
      for (let i = 0; i < obj.length; i++) {
        if (obj[i].id == this.qid) {
          let ans = obj[i].answers;
          for (let j = 0; j < ans.length; j++) {
            if (ans[j].aid == this.aid) {
              var repRef = firebase.database().ref('/questions/' + i + '/answers/' + j + '/');
              repRef
                .update({
                  ans: this.description
                });
            }
          }
        }
      }
    });
    this.navCtrl.pop();
  }

  edit_comm_quest() {
    var reportRef = firebase.database().ref();
    reportRef.once('value', personSnapshot => {
      let myPerson = personSnapshot.val();
      let obj1 = myPerson.community;
      for (let i = 0; i < obj1.length; i++) {
        if (obj1[i] != null && obj1[i].comm_id == this.tab) {
          let obj2 = obj1[i].companies;
          for (let j = 0; j < obj2.length; j++) {
            if (obj2[j] != null && obj2[j].cid == this.cid) {
              let obj3 = obj2[j].groups;
              for (let k = 0; k < obj3.length; k++) {
                if (obj3[k] != null && obj3[k].gid == this.gid) {
                  let obj4 = obj3[k].questions;
                  for (var l in obj4) {
                    if (obj4[l].id == this.qid) {
                      var repRef = firebase.database().ref('/community/' + i + '/companies/' + j + '/groups/' + k + '/questions/' + l + '/');
                      repRef
                        .update({
                          qst: this.quest
                        });
                      this.viewCtrl.dismiss();
                    }
                  }
                }
              }
            }
          }
        }
      }
      let obj21 = myPerson.questions;
      for (let i = 0; i < obj21.length; i++) {
        if (obj21[i] != null && obj21[i].id == this.qid) {
          var repRef_o = firebase.database().ref('/questions/' + i + '/');
          repRef_o
            .update({
              question: this.quest
            });
        }
      }
    });
    this.navCtrl.pop();
  }

}
