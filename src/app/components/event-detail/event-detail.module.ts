import { NgModule } from '@angular/core';
import { EventDetailPage } from './event-detail';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [EventDetailPage],
  imports: [IonicPageModule.forChild(EventDetailPage)],
  exports: [EventDetailPage]
})
export class EventDetailPageModule { }
