import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, Content, IonicPage } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Events } from '@ionic/angular';
import { environment as ENV } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-event-detail',
  templateUrl: 'event-detail.html',
})
export class EventDetailPage {

	event: any = {};
	type: any;
	uid: any;
	event_id: any;
	link: any;
	users: any = [];
	options: any;
	going: boolean;
  loading: any;
  nouser: boolean;
  title: string;
  @ViewChild(Content, {static: false}) content: Content;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public storage: Storage, private iab: InAppBrowser, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public events: Events ) {

    this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        duration: 10000
    });
    this.loading.present();

  	this.event_id = this.navParams.get('event_id');
  	this.type = this.navParams.get('event');
  	this.going = true;

    this.web_service();

    this.events.subscribe('reloadPage',() => {
      console.log('Event published');
      this.web_service();
    });

  }

  scroll_page_up()
  {
    setTimeout(() => {
      this.content.scrollToTop(500);
    }, 200);
  }

  web_service()
  {
    let headers = new HttpHeaders();
    headers.append('Content-Type',  'application/json');
    headers.append('Accept',  'application/json');
    this.options = {headers: headers};
    this.storage.get('id').then((id)=>{
      this.uid = id;

      let data = JSON.stringify({id: this.uid, event_id: this.event_id});
      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/single_event', data, this.options).subscribe(result=>{
          this.hideLoading();
          let bl = JSON.parse(result["_body"]);
          this.event = bl[0].event;
          this.users = [];
          this.title = this.event.event_title;
          for(let i=0; i<bl[0].userattending.length; i++)
          {
            if(bl[0].userattending[i].id != this.uid)
              this.users.push(bl[0].userattending[i]);
          }
          if(this.users[0] == null)
            this.nouser = true;
          else
            this.nouser = false;
          this.link = bl[0].event.event_viewlink;
          if(bl[0].attending == 'YES')
            this.going = true;
          else
            this.going = false;

      });
      
    });
  }

  hideLoading() {
    this.loading.dismiss();
  }

  openEvents()
  {
	  this.navCtrl.pop();
  }

  openLink()
  {
  	const options: InAppBrowserOptions = {
      zoom: 'no',
      toolbarcolor: '#7d0d3f',
      hidenavigationbutton: 'yes',
      hideurlbar: 'yes',
      location: 'yes',
      closebuttoncolor: '#ffffff',
      toolbarposition: 'top'
    }
  	var target = "_blank";
  	this.iab.create(this.link, target, options);
  }

  attending()
  {
    this.loadingCtrl.create({
      content: 'Please wait...',
      duration: 2000
    }).present();
    let data = JSON.stringify({id: this.uid, evtid: this.event_id});
  	this.http.post(ENV.BASE_URL + 'webservices/Hobby2/add_attend', data, this.options).subscribe(result=>{
        this.loading.dismiss();
        this.going = true;
        this.toastCtrl.create({
          message: 'Marked as attending',
          duration: 2000
        }).present();
	this.web_service();
    });
  }

  notAttending()
  {
    this.loadingCtrl.create({
      content: 'Please wait...',
      duration: 2000
    }).present();
    let data = JSON.stringify({id: this.uid, evtid: this.event_id});
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/remove_attend', data, this.options).subscribe(result=>{
        /*let bl = JSON.parse(result["_body"]);
        this.loading.dismiss();
        console.log(bl);*/
        this.going = false;
        this.toastCtrl.create({
          message: 'You have unchecked yourself from the attending list',
          duration: 3000
        }).present();
	this.web_service();
    });
  }

  viewProfile(id, friend)
  {
    let conn;
    if(friend == 'no')
      conn = 'yes';
    else if(friend == 'yes')
      conn = 'no';
    else
      conn = friend;
    this.navCtrl.push('PersonPage', {id: id, connect: conn, page: 'events'});
  }

}
