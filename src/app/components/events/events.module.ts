import { NgModule } from '@angular/core';
import { EventsPage } from './events';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [EventsPage],
  imports: [IonicPageModule.forChild(EventsPage)],
  exports: [EventsPage]
})
export class EventsPageModule { }
