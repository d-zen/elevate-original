import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from '@ionic/angular';

@IonicPage()
@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage {

	local = 'LocalPage';
	global = 'GlobalPage';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
