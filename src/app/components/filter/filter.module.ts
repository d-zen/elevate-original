import { NgModule } from '@angular/core';
import { FilterPage } from './filter';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [FilterPage],
  imports: [IonicPageModule.forChild(FilterPage)],
  exports: [FilterPage]
})
export class FilterPageModule { }
