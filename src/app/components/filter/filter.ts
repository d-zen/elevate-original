import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, ModalController, ToastController, IonicPage } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { NativeGeocoder, NativeGeocoderOptions, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
// import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import { environment as ENV } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { google } from "@agm/core/services/google-maps-types";

@IonicPage()
@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {
  @ViewChild("search", {static: false})
  public searchElementRef: ElementRef;

  career: boolean;
	hobby: boolean;
	proximity: boolean;
  level: any;
  lat: any;
  long: any;
	criteria: any[];
	company_size: any[];
	s_criteria: any;
	c_size: any;
  distance: any;
	hobbies: any[];
	s_hobby: any;
  hob_check: any = [];
  comp_pref: any = [];
  level_pref: string;
  check: any = [];
  check1: any = [];
  checked: any = [];
  check_value1: any = [];
  check_value2: any = [];
  check_value3: any = [];
  comb_pref: any = [];
  id: any;
  location: any;
  loc: any;
  headers: any;
  options: any;
  loading: any;
  address: any;
  check_mentor: any;
  hobbies2: any = [];
  bl_array: any = [];
  teach_on: any;
  sel_industry: any;
  industries: any = [];
  all_skills: any = [];
  skill: any;
  invest: any = [];
  tech: any = [];
  finance: any = [];
  ind1: any;
  ind2: any;
  ind3: any;
  finance_level: any;
  show_ind1: boolean;
  show_ind2: boolean;
  show_ind3: boolean;
  show_finance: boolean;
  community: any;
  comm: any = [];
  public searchControl: FormControl;



  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public http: HttpClient, public storage: Storage,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private nativeGeocoder: NativeGeocoder,
    private geolocation: Geolocation,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    public toastCtrl: ToastController
  ) {
    this.id = localStorage.getItem('id');
    
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.options = {headers: this.headers};

    this.industries = ['Founder','Investor','Tech','Finance','Insurance','Consultancy','Financial Law','Other']

    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat = resp.coords.latitude;
      this.long = resp.coords.longitude;
    });

    let category = this.navParams.get('cat');
  	if(category == 'career')
  		this.career = true;
  	else if(category == 'hobby')
  		this.hobby = true;
  	else
  		this.proximity = true;

    if(this.career || this.hobby)
    {
      this.loading = this.loadingCtrl.create({
          content: 'Please wait...',
          duration: 10000
      });
      this.loading.present();
    }

    if(this.career)
    {
      this.bl_array = ['Can teach me','Can be a panelist','Can write on'];

      this.http.get(ENV.BASE_URL + 'webservices/Hobby2/fetch_skills').subscribe(result=>{
        let res = JSON.parse(result["_body"]);
        this.all_skills = res.data;
      });

      let data = JSON.stringify({id: this.id});
      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/fetch_user_groups',data,this.options).subscribe(result=>{
        let res = JSON.parse(result["_body"]);
        for(let i=0;i<res.length; i++)
          this.comm.push(res[i]);
      });
      console.log(this.comm)

      let data1 = JSON.stringify({id: this.id});
      this.http.post(ENV.BASE_URL + 'webservices/Login/validate2',data1,this.options).subscribe(result=>{
        this.hideLoading();
        let data = JSON.parse(result["_body"]);
        this.sel_industry = data[0].industry;
        this.skill = data[0].skill;
        let teach = data[0].teach_on;
        this.teach_on = teach.split(',');
        for(let i=0; i<this.teach_on.length;i++)
        {
          if(this.teach_on[i] == 'Happy to teach others')
          {
            this.check1.push(0);
            this.check_value3.push('Happy to teach others');
          }
          else if(this.teach_on[i] == 'Can speak on conferences/panels')
          {
            this.check1.push(1);
            this.check_value3.push('Can speak on conferences/panels');
          }
          else if(this.teach_on[i] == 'Can write articles/blogs')
          {
            this.check1.push(2);
            this.check_value3.push('Can write articles/blogs');
          }
          else
            this.check1.push(3);
        }

        if(data[0].mentor[0] == '1')
          this.check_mentor = true;
        else
          this.check_mentor = false;

        this.level_pref = data[0].level_preference;

        for(let i=0;i<data[0].company_preference.length;i++)
          this.comp_pref[i] = data[0].company_preference[i];

        for(let i=0;i<data[0].extra_pref.length;i++)
          this.comb_pref[i] = data[0].extra_pref[i];
  
        for(let i=0; i<this.comp_pref.length;i++)
        {
          if(this.comp_pref[i] == 'Startup')
          {
            this.check.push(0);
            this.check_value1.push('Startup');
          }
          else if(this.comp_pref[i] == 'Mid-level')
          {
            this.check.push(1);
            this.check_value1.push('Mid-level');
          }
          else if(this.comp_pref[i] == 'Institution')
          {
            this.check.push(2);
            this.check_value1.push('Institution');
          }
          else
            this.check.push(3);
        }
  
        if(this.level_pref == 'junior')
          this.level = 'junior';
        else if(this.level_pref == 'senior')
          this.level = 'senior';
        else if(this.level_pref == 'same')
          this.level = 'same';
        else
          return;

        this.loc = data[0].location_pref;

        if(this.loc == 'city')
          this.location = 'city';
        else
          this.location = 'globally';

        for(let j=0; j<this.comb_pref.length; j++)
        {
          if(this.comb_pref[j] == 'Has studied further/ extra qualifications')
          {
            this.checked.push(0);
            this.check_value2.push('Has studied further/ extra qualifications');
          }
          else if(this.comb_pref[j] == 'career change and now moved')
          {
            this.checked.push(1);
            this.check_value2.push('Has had a career move');
          }
          else if(this.comb_pref[j] == 'Is currently on maternity')
          {
            this.checked.push(2);
            this.check_value2.push('Is currently on maternity');
          }
          else if(this.comb_pref[j] == 'Is a working mother')
          {
            this.checked.push(3);
            this.check_value2.push('Is a working mother');
          }
          else if(this.comb_pref[j] == 'Is in the same role as me')
          {
            this.checked.push(4);
            this.check_value2.push('Is same role as me...');
          }
          else if(this.comb_pref[j] == 'Is in the same company as me')
          {
            this.checked.push(5);
            this.check_value2.push('Is in the same company as me');
          }
          else if(this.comb_pref[j] == 'Has relocated')
          {
            this.checked.push(6);
            this.check_value2.push('Has relocated');
          }
          else
            return;
        }

      });

    }

    if(this.hobby)
    {
      /*this.storage.get('hob_loc').then(loc=>{
        this.address = loc;
      });*/

        let data = JSON.stringify({id: this.id});
        this.http.post(ENV.BASE_URL + 'webservices/Hobby2/hobbies_location', data, this.options).subscribe(result=>{
          let res = JSON.parse(result["_body"]);
          this.address = res[0].location;
        });

      let options: NativeGeocoderOptions = {
        useLocale: true,
        maxResults: 5
      };
      this.distance = 1;
      this.geolocation.getCurrentPosition().then((resp) => {
        this.lat = resp.coords.latitude;
        this.long = resp.coords.longitude;

        this.nativeGeocoder.reverseGeocode(this.lat, this.long, options)
          .then((result: NativeGeocoderResult[]) => {
              this.address = result[0].locality + ', ' + result[0].administrativeArea + ', ' + result[0].countryName;
        });

      });

      this.http.post(ENV.BASE_URL + 'webservices/Login/validate2',data,this.options).subscribe(result=>{
        this.hideLoading();
        let data2 = JSON.parse(result["_body"]);
        this.hob_check = data2[0].hobbies_pref;
      });
    }

    if(this.proximity)
    {
      let options: NativeGeocoderOptions = {
        useLocale: true,
        maxResults: 5
      };
      this.distance = 5;
      this.geolocation.getCurrentPosition().then((resp) => {
        this.lat = resp.coords.latitude;
        this.long = resp.coords.longitude;

        this.nativeGeocoder.reverseGeocode(this.lat, this.long, options)
          .then((result: NativeGeocoderResult[]) => {
            if(result[0].locality != null && result[0].administrativeArea != null)
            {
              this.address = result[0].locality + ', ' + result[0].administrativeArea + ', ' + result[0].countryName;
            }
            else
            {
              this.address = result[0].countryName;
            }
        });

      });
      /*this.storage.get('prox_loc').then(add=>{
        this.address = add;
        console.log(this.address);
      });*/
    }

  	this.criteria = ['Has studied further/ extra qualifications','Has had a career move','Is currently on maternity','Is a working mother','Is in the same role as me','Is in the same company as me','Has relocated'];

  	this.company_size = ['Startup (under 150 employees)','Scaleup (150-2,000 employees)','Institution (Over 2,000 employees)'];

  	this.hobbies = ['Arts & culture','Charity','Gym','High intensity work outs (Barrys/ Tough Mudder/Iron Woman)','Low intensity workout (yoga/pilates)','Outdoor activities (running/cycling/golf/hiking)','Play dates with kids','Reading','Team sports','Travel','Wine tasting','Winter activities (Skiing/snowboarding)'];

    this.hobbies2 = ['Diversity and Inclusion advocate','Gender equality speaker panel','Women&apos;s Network'];

    this.invest = ['Venture Capital','Angel'];

    this.tech = ['Tech','Fintech','Regtech','Insurtech','Healthtech'];

    this.finance = ['Finance','Asset-based finance and leasing','Funds management/ Superannuation','Hedge funds','Proprietary trading','Investment banking','Payment systems, clearing and settlement','Private banking','Private equity','Retail banking','Wealth Management'];

  }

  ngOnInit() {

    //create search FormControl
    this.searchControl = new FormControl();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["geocode"],
        //types: ['(cities)'],
        componentRestrictions: {country: ["gbr","swe","can","usa"]}
      });
      autocomplete.addListener("place_changed", () => {
          console.log('commented');

/*        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          this.address = place.formatted_address;
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
        });*/
      });
    });
  }

  hideLoading() {
    this.loading.dismiss();
  }

  ind_selected()
  {
    if(this.sel_industry == 'Investor')
    {
      this.show_ind1 = true;
      this.show_ind2 = false;
      this.show_ind3 = false;
    }
    else if(this.sel_industry == 'Tech')
    {
      this.show_ind2 = true;
      this.show_ind1 = false;
      this.show_ind3 = false;
    }
    else if(this.sel_industry == 'Finance')
    {
      this.show_ind3 = true;
      this.show_ind1 = false;
      this.show_ind2 = false;
    }
    else
    {
      this.show_ind1 = false;
      this.show_ind2 = false;
      this.show_ind3 = false;
    }
  }

  radioChecked2(i)
  {
    this.show_finance = true;
  }

  update1(item, event)
  {
    if(event.checked)
    {
      if(item == '0')
      {
        if(this.check_value1.includes('Startup'))
          return;
        else
          this.check_value1.push('Startup');
      }
      else if(item == '1')
      {
        if(this.check_value1.includes('Mid-level'))
          return;
        else
          this.check_value1.push('Mid-level');
      }
      else if(item == '2')
      {
        if(this.check_value1.includes('Institution'))
          return;
        else
          this.check_value1.push('Institution');
      }
      else
        return;
    }
    else
    {
      if(item == '0')
        this.check_value1.splice(this.check_value1.indexOf('Startup'),1);
      else if(item == '1')
        this.check_value1.splice(this.check_value1.indexOf('Mid-level'),1);
      else if(item == '2')
        this.check_value1.splice(this.check_value1.indexOf('Institution'),1);
      else
        return;
    }
  }

  update2(item, event)
  {
    if(item == 0)
    {
      if(event.checked)
      {
        if(this.check_value2.includes('Has studied further/ extra qualifications'))
          return;
        else
          this.check_value2.push('Has studied further/ extra qualifications');
      }
      else
        this.check_value2.splice(this.check_value2.indexOf('Has studied further/ extra qualifications'),1);
    }
    else if(item == 1)
    {
      if(event.checked)
      {
        if(this.check_value2.includes('career change and now moved'))
          return;
        else
          this.check_value2.push('career change and now moved');
      }
      else
        this.check_value2.splice(this.check_value2.indexOf('career change and now moved'),1);
    }
    else if(item == '2')
    {
      if(event.checked)
      {
        if(this.check_value2.includes('Is currently on maternity'))
          return;
        else
          this.check_value2.push('Is currently on maternity');
      }
      else
        this.check_value2.splice(this.check_value2.indexOf('Is currently on maternity'),1);
    }
    else if(item == '3')
    {
      if(event.checked)
      {
        if(this.check_value2.includes('Is a working mother'))
          return;
        else
          this.check_value2.push('Is a working mother');
      }
      else
        this.check_value2.splice(this.check_value2.indexOf('Is a working mother'),1);
    }
    else if(item == '4')
    {
      if(event.checked)
      {
        if(this.check_value2.includes('Is in the same role as me'))
          return;
        else
          this.check_value2.push('Is in the same role as me');
      }
      else
        this.check_value2.splice(this.check_value2.indexOf('Is in the same role as me'),1);
    }
    else if(item == '5')
    {
      if(event.checked)
      {
        if(this.check_value2.includes('Is in the same company as me'))
          return;
        else
          this.check_value2.push('Is in the same company as me');
      }
      else
        this.check_value2.splice(this.check_value2.indexOf('Is in the same company as me'),1);
    }
    else if(item == '6')
    {
      if(event.checked)
      {
        if(this.check_value2.includes('Has relocated'))
          return;
        else
          this.check_value2.push('Has relocated');
      }
      else
        this.check_value2.splice(this.check_value2.indexOf('Has relocated'),1);
    }
    else
      return;
  }

  update3(item, event)
  {
    if(event.checked)
    {
      if(item == '0')
      {
        if(this.check_value3.includes('Happy to teach others'))
          return;
        else
          this.check_value3.push('Happy to teach others');
      }
      else if(item == '1')
      {
        if(this.check_value3.includes('Can speak on conferences/panels'))
          return;
        else
          this.check_value3.push('Can speak on conferences/panels');
      }
      else if(item == '2')
      {
        if(this.check_value3.includes('Can write articles/blogs'))
          return;
        else
          this.check_value3.push('Can write articles/blogs');
      }
      else
        return;
    }
    else
    {
      if(item == '0')
        this.check_value3.splice(this.check_value3.indexOf('Happy to teach others'),1);
      else if(item == '1')
        this.check_value3.splice(this.check_value3.indexOf('Can speak on conferences/panels'),1);
      else if(item == '2')
        this.check_value3.splice(this.check_value3.indexOf('Can write articles/blogs'),1);
      else
        return;
    }
  }

  update_h(hob, event)
  {
    if(event.checked)
    {
      if(this.hob_check.includes(hob))
        return;
      else
        this.hob_check.push(hob);
    }
    else
    {
      this.hob_check.splice(this.hob_check.indexOf(hob),1);
    }
  }

  change_ment(event)
  {
    if(event.checked)
      this.check_mentor = true;
    else
      this.check_mentor = false;
  }

  ap_career()
  {
    let m;
    if(this.check_mentor == true)
      m = '1';
    else
      m = '0';

    let ind;
    let f_office = '';
    if(this.ind1 != null)
      ind = this.ind1;
    else if(this.ind2 != null)
      ind = this.ind2;
    else if(this.finance_level != null)
    {
      ind = this.finance_level;
      f_office = this.ind3;
    }
    else
      ind = '';

      console.log(this.id)

    let data = JSON.stringify({id: this.id, level: this.level, location: this.location, company: this.check_value1, extra: this.check_value2, mentor: m, teach: this.check_value3, industry: this.sel_industry, skill: this.skill, industry_level: ind, community: this.community, finance_office: f_office});
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/work_change',data,this.options).subscribe(result=>{
    });
    let dt = {'option': 'career'};
    this.viewCtrl.dismiss(dt);
  }

  ap_hobby()
  {
    this.nativeGeocoder.forwardGeocode(this.address).then((coordinates: NativeGeocoderResult[]) => {
      this.lat = coordinates[0].latitude;
      this.long = coordinates[0].longitude;
      this.storage.set('hob_loc',this.address);

      let len = this.hob_check.length;

      let dt = {'option': 'hobby', 'hobby': this.hob_check, 'lat': this.lat, 'longg': this.long, 'distance': this.distance};
      this.viewCtrl.dismiss(dt);
    },(err)=>{
      
      this.lat = '51.509865';
      this.long = '-0.118092';
      this.storage.set('hob_loc',this.address);
      let dt = {'option': 'hobby', 'hobby': this.hob_check, 'lat': this.lat, 'longg': this.long, 'distance': this.distance};
      this.viewCtrl.dismiss(dt);
    
    });
  }

  ap_proxim()
  {
    this.nativeGeocoder.forwardGeocode(this.address).then((coordinates: NativeGeocoderResult[]) => {
      this.lat = coordinates[0].latitude;
      this.long = coordinates[0].longitude;
      this.storage.set('prox_loc',this.address);
      let dt = {'option': 'proxim', 'dist': this.distance, 'lat': this.lat, 'long': this.long};
      this.viewCtrl.dismiss(dt);
    },(err)=>{

      this.toastCtrl.create({
        message: 'Please enter the location',
        duration: 2000
      }).present();
    
    });
  }

  closeModal()
  {
  	let dt = {'option': 'none'};
    this.viewCtrl.dismiss(dt);
  }

}
