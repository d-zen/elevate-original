import { NgModule } from '@angular/core';
import { GlobalPage } from './global';
import { IonicPageModule } from '@ionic/angular';
import { PipesModule } from 'src/pipes/pipes.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [GlobalPage],
  imports: [
    IonicPageModule.forChild(GlobalPage),
    HttpClientModule,
    PipesModule
  ],
  exports: [GlobalPage]
})
export class GlobalPageModule { }
