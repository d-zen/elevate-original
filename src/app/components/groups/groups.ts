import { Component, ViewChild, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, Content } from '@ionic/angular';
import { environment as ENV } from '../../../environments/environment';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../services/auth.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-groups',
  templateUrl: 'groups.html',
})
export class GroupsPage implements OnDestroy, OnInit {

  groups: any = [];
  loading: any;
  cid: any;
  comm_id: any;
  cname: any;
  searchStr = '';
  role: any;
  options: any;


  comp: any;

  token: any;

  chanellTitle = '';

  adminEmail: boolean = false;

  @ViewChild(Content, {static: false}) content: Content;

  private subscriptions: Subscription[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private modalCtrl: ModalController,
    public http: HttpClient,
    private auth: AuthService
  ) {

    let headers = new HttpHeaders();
    headers.append('Authorization', this.token);
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    this.options = { headers: headers };

    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();

    this.comp = this.navParams.get('comp');
    this.comm_id = this.comp.id;
    this.role = this.navParams.get('role');
    this.groups = this.comp.channels;

    this.loading.dismiss();

  }

  ngOnInit() {
    this.subscriptions.push(
      this.auth.me()
        .subscribe(user => {
          console.log(user)
          if(user.email == "shaha28@me.com") {
            this.role = 'admin';
          }
        })
    )
  }

  open_grp(id) {
    this.navCtrl.push('SingleCommPage', { gid: id });
  }

  getChannels() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait',
      duration: 3000
    });
    this.loading.present();
      // let data = JSON.stringify({ uid: this.id });
      this.http.get(ENV.BASE_URL + 'api/communities/' + this.comm_id, this.options).subscribe(result => {
        let response = JSON.parse(result["_body"]);
        this.groups = response[0].communities[0].channels;
        console.log(response)
      }, error => {
        console.log(error);
    });
  }

  createChannel() {
    let profileModal = this.modalCtrl.create( 'CreateChannelModal', { comm_id: this.comm_id });
    profileModal.onDidDismiss(data =>{
      if(data != null) { // check if null in case user quits the page and dont select a city
        this.getChannels()
      } 
    });
    profileModal.present();
    
  }


  getItems(ev) {
    this.searchStr = ev.target.value;
  }

  cancelSearch() {
  }

  getFilteredItems() {
    let items = [];
    items = this.groups.filter((item) => {
      return item.title.toLowerCase().indexOf(this.searchStr.toLowerCase()) > -1;
    });
    return items;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(item => item.unsubscribe());
  }

}
