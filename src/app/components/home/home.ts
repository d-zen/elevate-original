import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  NavController,
  MenuController,
  ToastController,
  LoadingController,
  AlertController,
  Events,
  IonicPage
} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AuthService } from "../../../services/auth.service";
// import { UserService } from "../../providers/user.service";
import { Subscription } from "rxjs";

import { environment as ENV } from '../../../environments/environment' ;
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Network } from '@ionic-native/network/ngx';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnDestroy, OnInit {

  private toast;
  email: any;
  password: any;
  msgg: any;
  scopes: any;
  private loading: any;
  email_f: any;
  forgot: boolean;
  private subscriptions = [];

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public http: HttpClient,
    public storage: Storage,
    // public iab: InAppBrowser,
    public loadingCtrl: LoadingController,
    private network: Network,
    public alertCtrl: AlertController,
    public events: Events,
    private authService: AuthService,
    // private userService: UserService,
  ) {

    this.menuCtrl.enable(false);
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
    });
    this.storage.set('logged_in', '0');
    this.storage.remove('car_history');
    this.storage.remove('edu_history');
    this.forgot = false;
  }

  ngOnInit() {
    this.menuCtrl.close();
  }

  i_input() {
    this.forgot = false;
  }

  openPage() {
    this.navCtrl.setRoot('Signup1Page');
  }

  login() {
    if (this.network.type == 'none') {
      console.log('Not connected');
      this.alertCtrl.create({
        title: 'No internet connection',
        subTitle: 'Get connected to the internet in order to use the app',
        buttons: ['OK']
      }).present();
    }

    if (this.email == null || this.password == null) {
      const subject = this.email == null ? 'email' : 'password';
      this.toastCtrl.create({
        message: 'Please enter your ' + subject,
      }).present();
      return;
    }

    this.loading.present();

    this.subscriptions.push(
      this.authService
        .login(this.email, this.password)
        .subscribe(async token => {
          await this.authService.setToken(token);
          this.loading.dismiss();
        }, err => {
          this.loading.dismiss();
          if (err.hasOwnProperty('errors')) {
            err.errors.forEach(this.showErr.bind(this));
          }
        })
    );
  }

  forgot_pwd() {
    this.forgot = true;
  }

  send_mail() {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let options = {headers: headers};
    let data = JSON.stringify({email: this.email_f});

    this.http.post(ENV.BASE_URL + '/webservices/Hobby2/sentmail', data, options).subscribe(result => {
      this.forgot = false;
      let res = JSON.parse(result["_body"]);
      if (res.verify == 'Yes') {
        this.toastCtrl.create({
          message: 'Please check your mail. A link has been sent to reset the password',
          duration: 3000
        }).present();
      } else {
        this.toastCtrl.create({
          message: 'This email id is not registered with us',
          duration: 2000
        }).present();
      }
    });
  }

  private showErr(err) {
    try {
      this.toast.dismiss();
    } catch (e) {
    }
    this.toast = this.toastCtrl.create({
      message: err,
      duration: 3000
    }).present();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub: Subscription) => {
      sub.unsubscribe();
    })
  }
}
