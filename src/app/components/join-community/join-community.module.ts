import { NgModule } from '@angular/core';
import { IonicPageModule } from '@ionic/angular';
import { JoinCommunityModal } from './join-community';

@NgModule({
  declarations: [JoinCommunityModal],
  imports: [IonicPageModule.forChild(JoinCommunityModal)],
  exports: [JoinCommunityModal]
})
export class JoinCommunityModalModule {}
