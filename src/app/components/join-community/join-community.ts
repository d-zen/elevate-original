import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthService } from '../../../services/auth.service';
import { environment as ENV } from '../../../environments/environment';
import { Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { filter } from 'rxjs/operators';

@IonicPage()
@Component({
  selector: 'page-join-community',
  templateUrl: 'join-community.html',
})
export class JoinCommunityModal {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public http: HttpClient,
    public storage: Storage,
    public toastCtrl: ToastController,
    private auth: AuthService,
    private alertCtrl: AlertController
  ) { }

  token = '';
  comp: any;
  uid = '';
  img = '';
  name = '';

  show = false;

  canJoin: boolean;
  private subscriptions: Subscription[] = [];


  ngOnInit() {
    this.subscriptions.push(
        this.auth.me()
          .subscribe(user => {
            this.uid = user.id;
            this.img = user.img;
            this.name = user.firstname;
          }),
  
    )
    this.comp = this.navParams.get('comp');
    this.auth.token.pipe(
      filter(token => !!token)
    )
    .subscribe(token => {
      this.token = token;
    });

      this.JoinCheck();
  }

  closeModal() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Thank you for your request.',
      subTitle: 'It will be reviewed by the admin of this community.'
    });
    alert.present();
  }

  JoinCheck() {
    const headers = new HttpHeaders();

    headers.append('Authorization', this.token);
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    const header = { headers: headers };

    this.http.get( ENV.BASE_URL + 'api/communities/join/' + this.comp.id, header ).subscribe( result => {
      console.log(result)
      let admComm = JSON.parse(result["_body"]);
      this.canJoin = admComm.can_join;
      this.show = true;
      console.log(admComm)
    })
  }

  sendCreateRequest() {

    const headers = new HttpHeaders();

    headers.append('Authorization', this.token);
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    const header = { headers: headers };

    const options = {
        "community_id": this.comp.id,
        "user_id": this.uid
    }

    this.http.post(ENV.BASE_URL + 'api/communities/join', options, header).subscribe(r => {
      console.log('you are join')
      this.canJoin = true;
      this.presentAlert();
    })

  }

}
