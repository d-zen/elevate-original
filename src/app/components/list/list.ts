import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, NavParams, LoadingController, AlertController, Content, IonicPage } from '@ionic/angular';
import { DataProvider } from '../../../services/data/data';
import { Network } from '@ionic-native/network/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { environment as ENV } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {

	blogs: any = [];
  desc: string;
  sliced: string;
  someHTML: any;
  loading: any;
  search: boolean;
  myInput: any;
  empty: boolean;
  items: any = [];
  arr: any = [];
  showdiv: boolean;
  is_ios: boolean;
  articles: boolean;
  inspi_women: boolean;
  news: boolean;
  podcast: boolean;
  field: any;
  @ViewChild(Content, {static: false}) content: Content;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public loadingCtrl: LoadingController, public dataService: DataProvider, private network: Network, public alertCtrl: AlertController, public plt: Platform, private iab: InAppBrowser) {

    if (this.plt.is('ios'))
      this.is_ios = true;
    else
      this.is_ios = false;
    
    if(this.network.type == 'none')
    {
      this.empty = true;
      console.log('Not connected');
      this.alertCtrl.create({
        title: 'No internet connection',
        subTitle: 'Get connected to the internet in order to use the app',
        buttons: ['OK']
      }).present();
    }

    this.search = false;
    this.empty = false;
    this.field = this.navParams.get('field');
    if(this.field == 'Articles')
      this.open_articles();
    else if(this.field == 'Inspirational Women')
      this.open_i();
    else if(this.field == 'Newsletters')
      this.open_newsletter();
    else
      this.open_podcast();

    this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        duration: 10000
    });
    this.loading.present();
  }

  scroll_page_up()
  {
    setTimeout(() => {
      this.content.scrollToTop(500);
    }, 500);
  }

  hideLoading() {
    this.loading.dismiss();
  }

  openMag(id)
  {
    this.navCtrl.push('MagazinePage', {mid: id});
  }

  openBlog(id)
  {
  	this.navCtrl.push('BlogPage', {id: id});
  }

  srch()
  {
    this.search = true;
  }

  search_key()
  {
    this.showdiv = false;
    this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        duration: 10000
    });
    this.loading.present();

    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    let options = {headers: headers};
    let data = JSON.stringify({keyword: this.myInput});

    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/search_blog',data,options).subscribe(result=>{
      this.hideLoading();
      this.blogs = JSON.parse(result["_body"]);
      if(this.blogs == '')
        this.empty = true;
      else
        this.empty = false;
    });
  }

  openPod(link)
  {
    const options: InAppBrowserOptions = {
      zoom: 'no',
      toolbarcolor: '#7d0d3f',
      hidenavigationbutton: 'yes',
      hideurlbar: 'yes',
      location: 'yes',
      closebuttoncolor: '#ffffff',
      toolbarposition: 'top'
    }
    var target = "_blank";
    this.iab.create(link, target, options);
  }

  open_newsletter()
  {
    this.news = true;
    this.http.get(ENV.BASE_URL + 'webservices/Hobby2/newsletters').subscribe(result=>{
      this.hideLoading();
      this.arr = JSON.parse(result["_body"]);
      if(this.arr == '')
      this.empty = true;
      else
      this.empty = false;
    });
  }
  
  open_podcast()
  {
    this.podcast = true;
    this.http.get(ENV.BASE_URL + 'webservices/Hobby2/podcasts').subscribe(result=>{
      this.hideLoading();
      this.arr = JSON.parse(result["_body"]);
      if(this.arr == '')
        this.empty = true;
      else
        this.empty = false;
    });
  }

  open_articles()
  {
    this.articles = true;
    this.items = [];
    this.showdiv = false;
    this.search = false;

    this.http.get(ENV.BASE_URL + 'webservices/Hobby2/blog').subscribe(result=>{
      this.hideLoading();
      this.blogs = JSON.parse(result["_body"]);
      if(this.blogs == '')
        this.empty = true;
      else
        this.empty = false;
    });
  }

  open_i()
  {
    this.inspi_women = true;
    this.http.get(ENV.BASE_URL + 'webservices/Hobby2/fetch_magazines').subscribe(result=>{
      this.arr = JSON.parse(result["_body"]);
    });
    this.http.get(ENV.BASE_URL + 'webservices/Hobby2/inspirational_blogs').subscribe(result=>{
      this.hideLoading();
      this.empty = false;
      this.blogs = JSON.parse(result["_body"]);
    });
    if(this.arr == '' && this.blogs == '')
      this.empty = true;
    else
      this.empty = false;
  }

  setFilteredItems() 
  { 
    this.items = this.dataService.filterItems(this.myInput);
    if(this.items.length > 0)
      this.showdiv = true;
    else
      this.showdiv = false;
  }

  openLetter(str)
  {
    this.navCtrl.push('BlogPage',{content: str, field: 'newsletter'});
  }

}
