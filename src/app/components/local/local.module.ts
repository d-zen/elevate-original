import { NgModule } from '@angular/core';
import { LocalPage } from './local';
import { IonicPageModule } from '@ionic/angular';
import { PipesModule } from 'src/pipes/pipes.module';

@NgModule({
  declarations: [LocalPage],
  imports: [
    IonicPageModule.forChild(LocalPage),
    PipesModule
  ],
  exports: [LocalPage]
})
export class LocalPageModule { }
