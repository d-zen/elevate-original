import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, Content, IonicPage } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network/ngx';
import { environment as ENV } from "../../../environments/environment";
import { HttpClient } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-local',
  templateUrl: 'local.html',
})
export class LocalPage implements OnInit {

  distance: any;
  lat: any;
  long: any;
  empty;
  uid: any;
  events: any = [];
  options: any;
  img_link: any;
  loading: any;
  ctry: any;
  @ViewChild(Content, {static: false}) content: Content;

  localEvents: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, private geolocation: Geolocation, public storage: Storage, public loadingCtrl: LoadingController, private network: Network, public alertCtrl: AlertController) {

    this.uid = localStorage.getItem('id');

    if (this.network.type == 'none') {
      this.empty = true;
      console.log('Not connected');
      this.alertCtrl.create({
        title: 'No internet connection',
        subTitle: 'Get connected to the internet in order to use the app',
        buttons: ['OK']
      }).present();
    }

    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      duration: 10000
    });
    this.loading.present();
  }

  ngOnInit() {

    this.distance = 20;

    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat = resp.coords.latitude;
      this.long = resp.coords.longitude;
      console.log(this.lat, this.long);
      this.getEvents();
    }).catch((error) => {
      console.log('Error getting location', error);
    });


  }

  getEvents() {
    console.log(this.uid)
      this.http.get(ENV.BASE_URL + 'testapi/mentor/api/events').subscribe(result => {
        this.hideLoading();
        let bl = JSON.parse(result["_body"]);
        this.localEvents = bl;

        let eventsArr = this.localEvents;

        let newEventsArr: any = [];

        eventsArr.map((e, i) => {

          let eventLat = e.event_lat;
          let eventLong = e.event_long;

          let distencelocal = this.calcCrow(this.lat, this.long, eventLat, eventLong).toFixed(0);
          if (distencelocal <= this.distance) {
            newEventsArr.push(e);
          }
          this.events = newEventsArr;
        })

        if (this.events.length < 1)
          this.empty = true;
      });
  }


  toRad(Value) {
    return Value * Math.PI / 180;
  }

  calcCrow(lat1, lon1, lat2, lon2) {
    const R = 6371; // km
    let dLat = this.toRad(lat2 - lat1);
    let dLon = this.toRad(lon2 - lon1);
    let dlat1 = this.toRad(lat1);
    let dlat2 = this.toRad(lat2);

    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(dlat1) * Math.cos(dlat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
  }



  scroll_page_up() {
    setTimeout(() => {
      this.content.scrollToTop(500);
    }, 500);
  }

  hideLoading() {
    this.loading.dismiss();
  }

  openEvent(id) {
    this.navCtrl.push('EventDetailPage', { event_id: id, event: 'local' });
  }

  searchEvent() {
    let data = JSON.stringify({ id: this.uid, km: this.distance, lat: this.lat, long: this.long });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/event', data, this.options).subscribe(result => {
      let bl = JSON.parse(result["_body"]);
      this.events = bl.event;
      if (bl.event.length < 1)
        this.empty = true;
    });
  }

}
