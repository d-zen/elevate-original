import { NgModule } from '@angular/core';
import { IonicPageModule } from '@ionic/angular';
import { MagazinePage } from './magazine';
import { PinchZoomModule } from 'ngx-pinch-zoom';


@NgModule({
  declarations: [
    MagazinePage
  ],
  imports: [
  	PinchZoomModule,
    IonicPageModule.forChild(MagazinePage),
  ],
  exports: [MagazinePage]
})
export class MagazinePageModule {}
