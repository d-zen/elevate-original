import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, LoadingController, Platform, AlertController } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { environment as ENV } from "../../../environments/environment";
import { HttpClient } from '@angular/common/http';




@IonicPage({
})
@Component({
  selector: 'page-magazine',
  templateUrl: 'magazine.html',
})


export class MagazinePage implements OnInit {

  arr: any = [];
  //picture: any;
  //i: any;
  mid: any;
  clk: boolean;
  loading: any;
  currentIndex = 1;

  mainImg: any;
  @ViewChild('slides', {static: false}) slides: Slides;

  slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  constructor(
    private transfer: FileTransfer, 
    private file: File, 
    public platform: Platform, 
    public statusBar: StatusBar, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public http: HttpClient, 
    public loadingCtrl: LoadingController, 
    public plt: Platform,
    private alertCtrl: AlertController
    ) {

    /*for(let i=0; i<96; i++)
    {
      this.arr[i] = '../assets/imgs/magazine/'+(i+1)+'.jpg';
    }
  	this.picture = this.arr[0];
    this.i = 0;*/
    this.clk = false;
    this.platform.ready().then(() => {
      this.mid = this.navParams.get('mid');
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        duration: 10000
      });
      this.loading.present();
    });
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Download completed',
      subTitle: 'Check your download forlder',
      buttons: ['Ok']
    });
    alert.present();
  }


  ngOnInit() {

    this.http.get(ENV.BASE_URL + 'webservices/Hobby2/fetch_magazines').subscribe(result => {
      let res = JSON.parse(result["_body"]);
      for (let i = 0; i < res.length; i++) {
        if (res[i].id == this.mid) {
          for (let j = 1; j <= res[i].pages; j++)
            this.arr[j - 1] = ENV.BASE_URL + 'public/magazine/' + res[i].name + '/img' + j + '.jpg';
          this.loading.dismiss().then(() => {
          });
        }
      }
      this.mainImg = ENV.BASE_URL + 'public/magazine/' + this.mid + '/img1.jpg';
    });
  }


  openPdf() {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  
    loading.present();
  
    let path = null;

    if(this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.dataDirectory;
    }

    const transfer = this.transfer.create();

    const fileUrl = ENV.BASE_URL + 'August_2019_Digital_Business_Women.pdf';

    transfer.download(fileUrl, path + 'magazine.pdf').then( entry => {
      loading.dismiss();
      this.presentAlert();
      // let url = entry.toURL();
      // this.document.viewDocument(url, 'application/pdf', {});
    })

  }



  next1() {
    this.slides.slideNext();
    this.currentIndex = this.slides.getActiveIndex() + 1;
  }

  prev1() {
    this.slides.slidePrev();
    this.currentIndex = this.slides.getActiveIndex() + 1;
  }

  slideChanged() {
    this.currentIndex = this.slides.getActiveIndex() + 1;
  }

  clicked() {
    if (!this.clk)
      this.clk = true;
    else
      this.clk = false;
  }

}
