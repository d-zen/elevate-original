import { NgModule } from '@angular/core';
import { IonicPageModule } from '@ionic/angular';
import { ManageGroupsPage } from './manage-groups';

@NgModule({
  declarations: [ManageGroupsPage],
  imports: [IonicPageModule.forChild(ManageGroupsPage)],
  exports: [ManageGroupsPage]
})
export class ManageGroupsPageModule {}
