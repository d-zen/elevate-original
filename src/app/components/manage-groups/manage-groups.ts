import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from '@ionic/angular';

@IonicPage()
@Component({
  selector: 'page-manage-groups',
  templateUrl: 'manage-groups.html',
})
export class ManageGroupsPage {

  features: any = [];
  selectedIndex = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.features = [
      'Add Company Details',
      'Approve users',
      'Manage channels',
      'Manage users',
      'Add Resources',
      'Add event (Coming Soon)',
    ];

  }

  openCommunity(i, f) {
    this.selectedIndex = i;
    this.navCtrl.push('CommunityPage', { feat: f });
  }

}
