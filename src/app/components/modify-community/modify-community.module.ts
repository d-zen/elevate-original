import { NgModule } from '@angular/core';
import { IonicPageModule } from '@ionic/angular';
import { ModifyCommunityPage } from './modify-community';

@NgModule({
  declarations: [ModifyCommunityPage],
  imports: [IonicPageModule.forChild(ModifyCommunityPage)],
  exports: [ModifyCommunityPage]
})
export class ModifyCommunityPageModule {}
