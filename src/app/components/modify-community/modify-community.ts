import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, PopoverController } from '@ionic/angular';
import * as firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-modify-community',
  templateUrl: 'modify-community.html',
})
export class ModifyCommunityPage {

	opt: any;
  obj: any = [];
	cid: any;
	gid: any;
	name: any;
	comm_id: any;
	image: any;
	edit_c: boolean;
	add_c: boolean;
  empty: boolean;
  edit_post: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public popoverCtrl: PopoverController) {

  	this.empty = false;
    this.opt = this.navParams.get('opt');
  	this.cid = this.navParams.get('cid');
  	this.comm_id = this.navParams.get('comm_id');
  	this.gid = this.navParams.get('gid');
  	this.name = this.navParams.get('cname');
  	this.image = this.navParams.get('image');

  	if(this.opt == 'Edit channel')
  		this.edit_c = true;
    else if(this.opt == 'Edit posts')
      this.all_posts();
  	else
  	{
  		this.add_c = true;
  		this.image = "http://www.cndajin.com/data/wls/191/17509871.jpg";
  	}

  }

  all_posts()
  {
    this.edit_post = true;
    var reptRef = firebase.database().ref();
    reptRef.on('value', personSnapshot => {
      let myData = personSnapshot.val();
      let obj1 = myData.community;
      this.obj = [];
      for(let i=0;i<obj1.length;i++)
      {
        if(obj1[i].comm_id == this.comm_id)
        {
          let obj2 = obj1[i].companies;
          for(let j=0;j<obj2.length;j++)
          {
            if(obj2[j].cid == this.cid)
            {
              let obj3 = obj2[j].groups;
              for(let k=0;k<obj3.length;k++)
              {
                if(obj3[k].gid == this.gid)
                {
                  //this.obj = obj3[k].questions;
                  let qstn = obj3[k].questions;
                  for(var p in qstn)
                    this.obj.push(qstn[p]);
                  if(this.obj == '')
                    this.empty = true;
                }
              }
            }
          }
        }
      }
    });
  }

  view_comment(qid, qst)
  {
    this.navCtrl.push('EditCommentsPage',{page: 'comments', comm_id: this.comm_id, cid: this.cid, gid: this.gid, qid: qid, qst: qst});
  }

  edit_channel()
  {
  	var reptRef = firebase.database().ref();
    reptRef.once('value', personSnapshot => {
      let myData = personSnapshot.val();
      let obj1 = myData.community;
      for(let i=0;i<obj1.length;i++)
      {
        if(obj1[i].comm_id == this.comm_id)
        {
          let obj2 = obj1[i].companies;
          for(let j=0;j<obj2.length;j++)
          {
            if(obj2[j].cid == this.cid)
            {
              let obj3 = obj2[j].groups;
              for(let k=0;k<obj3.length;k++)
              {
                if(obj3[k].gid == this.gid)
                {
                  var repRef = firebase.database().ref('/community/'+i+'/companies/'+j+'/groups/'+k+'/');
                  repRef
                  .update({
                    title: this.name
                  });
                  this.closeModal();
                }
              }
            }
          }
        }
      }
    });
  }

  add_channel()
  {
  	var reptRef = firebase.database().ref();
    reptRef.once('value', personSnapshot => {
      let myData = personSnapshot.val();
      let obj1 = myData.community;
      for(let i=0;i<obj1.length;i++)
      {
        if(obj1[i].comm_id == this.comm_id)
        {
          let obj2 = obj1[i].companies;
          for(let j=0;j<obj2.length;j++)
          {
            if(obj2[j].cid == this.cid)
            {
              	let obj3 = obj2[j].groups;
               	let len = obj3.length;
                var repRef = firebase.database().ref('/community/'+i+'/companies/'+j+'/groups/');
                var usersRef = repRef.child(len);
                usersRef.set({
                  title: this.name,
                  gid: len
                });
                this.closeModal();
            }
          }
        }
      }
    });
  }

  presentPopover(event, id, obj)
  {
    let popover = this.popoverCtrl.create('PopoverPage', {'page': 'post', 'sid': id, 'comm_id': this.comm_id, 'gid': this.gid, 'cid': this.cid, 'obj': obj});
    popover.onDidDismiss(res=>{
      if(res.msg == 'done')
        this.all_posts();
    });
    popover.present();
  }

  closeModal()
  {
  	this.viewCtrl.dismiss();
  }

}
