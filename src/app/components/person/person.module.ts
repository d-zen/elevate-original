import { NgModule } from '@angular/core';
import { PersonPage } from './person';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [PersonPage],
  imports: [IonicPageModule.forChild(PersonPage)],
  exports: [PersonPage]
})
export class PersonPageModule { }
