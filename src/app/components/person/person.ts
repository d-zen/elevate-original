import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  NavController,
  NavParams,
  ViewController,
  LoadingController,
  ModalController,
  ToastController,
  IonicPage
} from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { Events, PopoverController } from "@ionic/angular";
import * as firebase from "firebase";
import {
  NativePageTransitions,
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions/ngx";
import { AuthService } from "../../../services/auth.service";
import { environment as ENV } from '../../../environments/environment' ;
import { HttpClient } from '@angular/common/http';

@IonicPage()
@Component({
  selector: "page-person",
  templateUrl: "person.html"
})
export class PersonPage implements OnInit, OnDestroy {
  shownGroup = null;
  id: any;
  name: string;
  lname: string;
  job: string;
  img: any;
  userid: any;
  ouser_id: any;
  user: any = {};
  career: any = [];
  education: any = [];
  hobbies: any = [];
  endtime: any = [];
  loading: any;
  show: boolean;
  msg_sent: boolean;
  show_close: boolean;
  msg: boolean;
  cancel: boolean;
  acc_rej: boolean;
  options: any;
  noshow: boolean;
  no_car: boolean;
  no_edu: boolean;
  refresh: boolean;
  threadId: any;
  filter: any;
  skill: any;
  event: boolean;

  private subscriptions = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public http: HttpClient,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public storage: Storage,
    public toastCtrl: ToastController,
    public events: Events,
    public popoverCtrl: PopoverController,
    private nativePageTransitions: NativePageTransitions,
    private auth: AuthService
  ) {
    let options: NativeTransitionOptions = {
      direction: "right",
      duration: 500
    };
    this.nativePageTransitions.slide(options);

    this.show_close = true;
    this.no_edu = false;
    this.no_car = false;
    this.noshow = false;
    this.refresh = false;
    this.event = false;
    let pg = this.navParams.get("page");
    if (pg == "events") {
      this.event = true;
      this.show_close = false;
    }

    if (pg == "comm") this.msg = true;

    if (pg == "chat") this.noshow = true;

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 10000
    });
    this.loading.present();

    this.id = this.navParams.get("id");
    this.filter = this.navParams.get("filter");
  }

  hideLoading() {
    this.loading.dismiss();
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create("PopoverPage", {
      fid: this.id,
      uid: this.userid,
      page: "person"
    });
    popover.present({
      ev: myEvent
    });
    popover.onDidDismiss(data => {
      if (data.msg == "done") this.cancel = false;
    });
  }

  message() {
    let data = JSON.stringify({ uid: this.userid, fid: this.id });
    this.http
      .post(
        ENV.BASE_URL + "webservices/Hobby2/get_threadId",
        data,
        this.options
      )
      .subscribe(result => {
        let res = JSON.parse(result["_body"]);
        this.viewCtrl.dismiss();
        this.navCtrl.push("chatRoomPage", {
          receiverId: this.id,
          firstName: this.name,
          lastName: this.lname,
          threadId: res[0].thread_id
        });
      });
  }

  closeModal() {
    if (this.msg_sent || this.refresh) this.viewCtrl.dismiss({ data: "sent" });
    else this.viewCtrl.dismiss({ data: "none" });
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  connect() {
    let modal = this.modalCtrl.create("ConnectPage", {
      name: this.name,
      rid: this.id,
      sid: this.userid,
      filter: this.filter
    });
    modal.onDidDismiss(data => {
      if (data.dt == "msg") {
        this.msg_sent = true;
        this.toastCtrl
          .create({
            message: "Invitation sent",
            duration: 3000
          })
          .present();
        this.show = false;
      }
    });
    modal.present();
  }

  accept() {
    let data = JSON.stringify({ receiver_id: this.id, sender_id: this.userid });
    this.http
      .post(
        ENV.BASE_URL + "webservices/Hobby2/accept_request",
        data,
        this.options
      )
      .subscribe(result => {
        this.acc_rej = false;
        this.msg = true;
        this.refresh = true;
        this.toastCtrl
          .create({
            message: "You have accepted the request from this user",
            duration: 3000
          })
          .present();
      });
  }

  update_friend(id, tid) {
    var reptRef = firebase.database().ref();
    reptRef.once("value", personSnapshot => {
      let myPerson = personSnapshot.val();
      let obj = myPerson.list;
      let fetched = obj[id];
      for (let i = 0; i < fetched.length; i++) {
        if (fetched[i] != null) {
          if (fetched[i].thread_id == tid) {
            var repRef = firebase.database().ref("/list/" + id + "/" + i + "/");
            repRef.update({
              friends: "no"
            });
            return;
          }
        }
      }
    });
  }

  reject() {
    this.http
      .get(
        ENV.BASE_URL + "webservices/Hobby2/thread?uid=" +
          this.userid +
          "&fid=" +
          this.id
      )
      .subscribe(result => {
        let res = JSON.parse(result["_body"]);
        let thread = res.thread_id;
        let data = JSON.stringify({
          sender_id: this.userid,
          receiver_id: this.id,
          tid: thread
        });
        this.http
          .post(
            ENV.BASE_URL + "webservices/Hobby2/reject_request",
            data,
            this.options
          )
          .subscribe(result => {
            this.update_friend(this.userid, thread);
            this.update_friend(this.id, thread);
            this.acc_rej = false;
            this.msg = true;
            this.refresh = true;
            this.toastCtrl
              .create({
                message: "You have rejected the request from this user",
                duration: 3000
              })
              .present();
          });
      });
  }

  popPage() {
    //this.navCtrl.pop();
    if (this.msg_sent || this.refresh) this.viewCtrl.dismiss({ data: "sent" });
    else this.viewCtrl.dismiss({ data: "none" });
  }

  ngOnInit() {
    this.subscriptions.push(
      this.auth.user.subscribe(user => {
        this.userid = user.id;
        let data = JSON.stringify({ uid: this.userid, fid: this.id });
        this.http
          .post(
            ENV.BASE_URL + "webservices/Hobby2/view2",
            data
          )
          .subscribe(result => {
            this.hideLoading();
            this.user = JSON.parse(result["_body"]);
            this.name = this.user.userdata.firstname;
            this.lname = this.user.userdata.lastname;
            this.skill = this.user.userdata.teach_on;
            if (this.user.career[0] != null)
              this.job = this.user.career[0].job_title;
            this.ouser_id = this.user.userdata.id;
            if (this.user.userdata.img) {
              this.img = this.user.userdata.img;
            } else {
              this.img = "../../assets/imgs/profile_pic.png";
            }

            if (this.user.career == null || this.user.career == "")
              this.no_car = true;
            else this.career = this.user.career;

            if (this.user.education == null || this.user.education == "")
              this.no_edu = true;
            else this.education = this.user.education;

            if (this.user.hobbies == null)
              this.hobbies = ["No information available"];
            else this.hobbies = this.user.hobbies.interest.split(",");

            for (let i = 0; i < this.career.length; i++) {
              if (this.career[i].iscurrent == "1") this.endtime[i] = "Present";
              else
                this.endtime[i] =
                  this.career[i].end_month + "-" + this.career[i].end_year;
            }

            /*if(this.user.userdata.friend == 'yes')
          this.msg = true;*/
            if (this.user.userdata.friend != "yes") {
              if (this.user.userdata.pending == "sent") this.cancel = true;
              else if (this.user.userdata.pending == "received")
                this.acc_rej = true;
              else this.show = true;
            }
          });
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.map(item => item.unsubscribe());
  }
}
