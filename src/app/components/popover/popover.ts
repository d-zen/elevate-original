import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController, AlertController, ModalController, Events, IonicPage } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase';
import { environment as ENV } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})
export class PopoverPage {

	id: any;
  sid: any;
	fid: any;
  cid: any;
  gid: any;
  aid: any;
  comm_id: any;
	name: any;
  lname: any;
	blocked: any;
  index: any;
	show_block: boolean;
	options: any;
  conn: boolean;
  msg: boolean;
  pro: boolean;
  blk: boolean;
  chat: boolean;
  person: boolean;
  question: boolean;
  m_user: boolean;
  edit_post: boolean;
  comments: boolean;
  pending_c: boolean;
  user_posts: boolean;
  q_obj: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public storage: Storage, public http: HttpClient, public toastCtrl: ToastController, public alertCtrl: AlertController, public modalCtrl: ModalController, public events: Events) {

    this.conn = false;
    this.msg = false;
    this.pro = false;
    this.chat = false;
    this.person = false;
    this.question = false;
    this.comments = false;
    this.pending_c = false;
    this.user_posts = false;
    this.storage.get('id').then(id=>{
  		this.id = id;
  	});
  	this.fid = this.navParams.get('fid');
  	this.name = this.navParams.get('name');
    this.lname = this.navParams.get('lname');
  	this.blocked = this.navParams.get('block');
    this.sid = this.navParams.get('sid');
    this.cid = this.navParams.get('cid');
    this.comm_id = this.navParams.get('comm_id');
    this.gid = this.navParams.get('gid');
    this.aid = this.navParams.get('aid');
    this.q_obj = this.navParams.get('obj');
    let field = this.navParams.get('field');
    if(field == 'yes')
      this.blk = true;
    else
      this.blk = false;
    let page = this.navParams.get('page');
    if(page == 'connections')
      this.conn = true;
    else if(page == 'profiles')
      this.pro = true;
    else if(page == 'person')
      this.person = true;
    else if(page == 'questions')
      this.question = true;
    else if(page == 'community')
      this.m_user = true;
    else if(page == 'post')
      this.edit_post = true;
    else if(page == 'comments')
      this.comments = true;
    else if(page == 'pending_c')
      this.pending_c = true;
    else if(page == 'single_comm')
      this.user_posts = true;
    else
      this.msg = true;

    let sh = this.navParams.get('sh_button');
    if(sh == 'no')
      this.chat = true;

  	if(this.blocked == '0')
  		this.show_block = true;
  	else
  		this.show_block = false;

    this.events.subscribe('dismiss',()=>{
      this.viewCtrl.dismiss();
    });

  	let headers = new HttpHeaders();
	  headers.append('Content-Type', 'application/json');
  	headers.append('Accept', 'application/json');
  	this.options = {headers: headers};
  }

  close()
  {
  	this.viewCtrl.dismiss();
  }

  remove()
  {
  	let data = JSON.stringify({uid: this.id, fid: this.fid});
  	this.http.post(ENV.BASE_URL + 'webservices/Hobby2/remove',data,this.options).subscribe(result=>{
      this.events.publish('blocked');
      this.toastCtrl.create({
        message: 'User removed successfully',
        duration: 5000
      }).present();
  	});
  	this.viewCtrl.dismiss({'dt': 'yes'});
  }

  block()
  {
  	this.alertCtrl.create({
      title: 'Block',
      subTitle: 'Are you sure you want to block ' + this.name + '?',
      buttons: [
        {
          text: 'Block',
          handler: () => {
            let modal = this.modalCtrl.create('BlockPage', {uid: this.id, fid: this.fid});
            modal.onDidDismiss(data=>{
              if(data.dt == 'yes')
                this.toastCtrl.create({
                  message: 'User blocked',
                  duration: 2000
                }).present();
            });
            modal.present();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    }).present();
    this.viewCtrl.dismiss({'dt': 'yes'});
  }

  unblock()
  {
    this.alertCtrl.create({
      title: 'Unblock',
      subTitle: 'Are you sure you want to unblock ' + this.name + '?',
      buttons: [
        {
          text: 'Unblock',
          handler: () => {
            let data = JSON.stringify({uid: this.id, fid: this.fid});
            this.http.post(ENV.BASE_URL + 'webservices/Hobby2/unblock',data,this.options).subscribe(result=>{
              this.events.publish('blocked');
              this.toastCtrl.create({
                message: 'User unblocked',
                duration: 2000
              }).present();
            });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    }).present();
    this.viewCtrl.dismiss({'msg': 'done'});
  }

  accept()
  {
    let data = JSON.stringify({receiver_id: this.fid, sender_id: this.id});
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/accept_request', data, this.options)
      .subscribe(result => {
        this.events.publish('refresh');
        this.toastCtrl.create({
          message: 'You have accepted the request from this user',
          duration: 3000
        }).present();
        this.viewCtrl.dismiss();
    });
  }

  update_friend(id, tid)
  {
    var reptRef = firebase.database().ref();
    reptRef.once('value', personSnapshot => {

      let myPerson = personSnapshot.val();
      let obj = myPerson.list;
      let fetched = obj[id];
      for(let i in fetched)
      {
        if(fetched[i] != null)
        {
          if(fetched[i].thread_id == tid)
          {
            var repRef = firebase.database().ref('/list/'+id+'/'+i+'/');
            repRef
            .update({
              friends: 'no'
            });
            return;
          }
        }
      }
    });
  }

  reject()
  {
    this.http.get(ENV.BASE_URL + 'webservices/Hobby2/thread?uid='+this.id+'&fid='+this.fid).subscribe(result => {
      let res = JSON.parse(result["_body"]);
      let thread = res.thread_id;
      let data = JSON.stringify({sender_id: this.id, receiver_id: this.fid, tid: thread});
      this.http.post(ENV.BASE_URL + 'webservices/Hobby2/reject_request', data, this.options)
        .subscribe(result => {
          this.update_friend(this.id, thread);
          this.update_friend(this.fid, thread);
          this.events.publish('refresh');
          this.toastCtrl.create({
            message: 'You have rejected the request from this user',
            duration: 3000
          }).present();
          this.viewCtrl.dismiss();
      });
    });
  }

  cancel_request()
  {
    this.http.get(ENV.BASE_URL + 'webservices/Hobby2/thread?uid='+this.id+'&fid='+this.fid).subscribe(result => {
      let res = JSON.parse(result["_body"]);
      let thread = res.thread_id;
    let data = JSON.stringify({ sender_id: this.id, receiver_id: this.fid });
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/cancel_request', data, this.options).subscribe(result => {
      this.person = false;
      this.update_friend(this.id, thread);
      this.update_friend(this.fid, thread);
      this.events.publish('reloadPage');
      this.toastCtrl.create({
        message: 'The request has been cancelled',
        duration: 3000
      }).present();
      this.viewCtrl.dismiss({'msg': 'done'});
    });
    });
  }

  remove_user()
  {
    this.alertCtrl.create({
      title: 'Remove',
      subTitle: 'Are you sure you want to remove this user?',
      buttons: [
      {
        text: 'Remove',
        handler: () => {
          
          let data = JSON.stringify({sid: this.sid});
          this.http.post(ENV.BASE_URL + 'webservices/Hobby2/remove_user', data, this.options).subscribe(result => {
            this.viewCtrl.dismiss({'msg': 'done'});
          });

        }
      },
      {
        text: 'Cancel',
        handler: () => {
        }
      }]
    }).present();
  }

  delete_post()
  {
    this.alertCtrl.create({
      title: 'Delete',
      subTitle: 'Are you sure you want to delete it?',
      buttons: [
      {
        text: 'Delete',
        handler: () => {
          
          var reportRef = firebase.database().ref();
          reportRef.once('value', personSnapshot => {
            let myPerson = personSnapshot.val();
            let obj1 = myPerson.community;
            for(let i=0;i<obj1.length;i++)
            {
              if(obj1[i]!=null && obj1[i].comm_id == this.comm_id)
              {
                let obj2 = obj1[i].companies;
                for(let j=0;j<obj2.length;j++)
                {
                  if(obj2[j]!=null && obj2[j].cid == this.cid)
                  {
                    let obj3 = obj2[j].groups;
                    for(let k=0;k<obj3.length;k++)
                    {
                      if(obj3[k]!=null && obj3[k].gid == this.gid)
                      {
                        let obj4 = obj3[k].questions;
                        for(let l=0;l<obj4.length;l++)
                        {
                          if(obj4[l]!=null && obj4[l].id == this.sid)
                          {
                            var repRef = firebase.database().ref('/community/'+i+'/companies/'+j+'/groups/'+k+'/questions/'+l+'/');
                            repRef.remove();
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            let obj2 = myPerson.questions;
            for(let i=0;i<obj2.length;i++)
            {
              if(obj2[i]!=null && obj2[i].id == this.sid)
              {
                var repRef_r = firebase.database().ref('/questions/'+i+'/');
                repRef_r.remove();
                this.toastCtrl.create({
                  message: 'The post has been deleted successfully',
                  duration: 3000
                }).present();
              }
            }
          });
          this.viewCtrl.dismiss({'msg': 'done'});
        }
      },
      {
        text: 'Cancel',
        handler: () => {
        }
      }]
    }).present();
  }

  move_topic()
  {
    this.navCtrl.push('EditCommentsPage', {page: 'post', qid: this.sid, comm_id: this.comm_id, gid: this.gid, cid: this.cid, obj: this.q_obj});
  }

  delete_comment()
  {
    this.alertCtrl.create({
      title: 'Delete',
      subTitle: 'Are you sure you want to delete it?',
      buttons: [
      {
        text: 'Delete',
        handler: () => {
          
          var reptRef = firebase.database().ref();
          reptRef.once('value', personSnapshot => {
            let myData = personSnapshot.val();
            let obj1 = myData.questions;
            for(var i in obj1)
            {
              if(obj1[i]!=null && obj1[i].id == this.sid)
              {
                let answers = obj1[i].answers;
                for(var j in answers)
                {
                  if(answers[j]!=null && answers[j].aid == this.aid)
                  {
                    var repRef_r = firebase.database().ref('/questions/'+i+'/answers/'+j+'/');
                    repRef_r.remove();
                    this.toastCtrl.create({
                      message: 'The comment has been deleted successfully',
                      duration: 3000
                    }).present();
                  }
                }
              } 
            }

            let obj2 = myData.community;
            for(let i=0;i<obj2.length;i++)
            {
              if(obj2[i]!=null && obj2[i].comm_id == this.comm_id)
              {
                let obj3 = obj2[i].companies;
                for(let j=0;j<obj3.length;j++)
                {
                  if(obj3[j]!=null && obj3[j].cid == this.cid)
                  {
                    let obj4 = obj3[j].groups;
                    for(let l=0;l<obj4.length;l++)
                    {
                      if(obj4[l]!=null && obj4[l].gid == this.gid)
                      {
                        let obj5 = obj4[j].questions;
                        for(let k=0;k<obj5.length;k++)
                        {
                          if(obj5[k]!=null && obj5[k].id == this.sid)
                          { 
                            let len = obj5[k].answers;
                            var repRef_o = firebase.database().ref('/community/'+i+'/companies/'+j+'/groups/'+l+'/questions/'+k+'/');
                            repRef_o
                            .update({
                              answers: (len-1)
                            });
                          }
                        }
                      }
                    } 
                  }
                }
              }
            }
          });
          this.viewCtrl.dismiss({'msg': 'done'});
        }
      },
      {
        text: 'Cancel',
        handler: () => {
        }
      }]
    }).present();
  }

  del_cquest()
  {
    this.alertCtrl.create({
      title: 'Delete',
      subTitle: 'Are you sure you want to delete it?',
      buttons: [
      {
        text: 'Delete',
        handler: () => {
          this.viewCtrl.dismiss();
        }
      },
      {
        text: 'Cancel',
        handler: () => {
        }
      }]
    }).present(); 
  }

  edit_cquest()
  {
    this.navCtrl.push('EditHobbyPage', {field: 'edit_cquest', qid: this.aid, quest: this.name, gid: this.gid, tab: this.comm_id, cid: this.cid});
  }

  message()
  {
    let data = JSON.stringify({uid: this.id, fid: this.fid});
    this.http.post(ENV.BASE_URL + 'webservices/Hobby2/get_threadId', data, this.options).subscribe(result => {
      let obj = JSON.parse(result["_body"]);
      let thread_id = obj[0].thread_id;
      this.navCtrl.push('chatRoomPage', {receiverId: this.fid, firstName: this.name, lastName: this.lname, threadId: thread_id});
    });
  }

  view()
  {
    if(this.chat || this.pending_c)
      this.navCtrl.push('PersonPage', {id: this.fid, connect: 'no', page: 'chat'});
    else
      this.navCtrl.push('PersonPage', {id: this.fid, connect: 'no', page: 'events'});
  }

  choose_name(str)
  {
    if(str == 'a')
      this.viewCtrl.dismiss({'str': 'Anonymous'});
    else
      this.viewCtrl.dismiss({'str': this.name});
  }

}
