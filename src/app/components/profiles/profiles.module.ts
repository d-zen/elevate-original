import { NgModule } from '@angular/core';
import { ProfilesPage } from './profiles';
import { IonicPageModule } from '@ionic/angular';
import { PipesModule } from 'src/pipes/pipes.module';

@NgModule({
  declarations: [ProfilesPage],
  imports: [
    IonicPageModule.forChild(ProfilesPage),
    PipesModule
  ],
  exports: [ProfilesPage],
})
export class ProfilesPageModule { }
