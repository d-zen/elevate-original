import { Component, OnInit, ViewChild } from '@angular/core';
import {
  NavController,
  NavParams,
  MenuController,
  ModalController,
  LoadingController,
  ToastController,
  AlertController,
  Events,
  PopoverController,
  Content,
  IonicPage
} from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
// import {UserService} from "../../providers/user.service";
import { AuthService } from "../../../services/auth.service";
import { BehaviorSubject, Subscription } from "rxjs";
import { ArraySortPipe } from '../../../pipes/filter-by-name.pipe';

import { environment as ENV } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Network } from '@ionic-native/network/ngx';
import { filter } from 'rxjs/operators';


const PAGE_TYPES = {
  career: 'career',
  hobby: 'hobby',
  proximity: 'proximity'
};

@IonicPage()
@Component({
  selector: 'page-profiles',
  templateUrl: 'profiles.html',
  providers: [ ArraySortPipe ]
})
export class ProfilesPage implements OnInit {

  isLoad: BehaviorSubject<boolean>;
  pageType: BehaviorSubject<string>;
  uid: any;
  lat: any;
  long: any;
  name: any;
  img: any;
  options: any;
  noinfo: boolean;
  show_questions: boolean;
  information: any = [];
  con_users: any = [];
  con: any = [];
  sent_r: any = [];
  sent: any = [];
  sent_requests: any = [];
  request: any = [];
  warning: boolean;
  private subscriptions: Subscription[] = [];
  @ViewChild(Content, {static: false}) content: Content;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public http: HttpClient,
    private geolocation: Geolocation,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public events: Events,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public platform: Platform,
    private network: Network,
    public popoverCtrl: PopoverController,
    private auth: AuthService) {

    this.storage.get('first_login').then(str => {
      if (str == '1') {
        this.navCtrl.push('WizardPage');
        this.storage.set('first_login', '0');
      }
    });

    this.events.publish('notification');

    if (this.network.type == 'none') {
      this.noinfo = true;
      console.log('Not connected');
      this.alertCtrl.create({
        title: 'No internet connection',
        subTitle: 'Get connected to the internet in order to use the app',
        buttons: ['OK']
      }).present();
    }
    this.menuCtrl.enable(true);
    this.platform.ready().then(() => {
      this.menuCtrl.open();
    });
  }

  scroll_page_up() {
    setTimeout(() => {
      this.content.scrollToTop(500);
    }, 500);
  }

  openProfile(id) {
    let modal = this.modalCtrl.create('PersonPage', { id: id, filter: this.pageType.getValue() });
    modal.onDidDismiss(data => {
      if (data.data == 'sent') {
        this.information = [];
        this.request = [];
        this.sent_requests = [];
        this.pageType.next(this.pageType.getValue());
      }
    });
    modal.present();
  }

  setCareer() {
    this.isLoad.next(true);
    this.warning = false;
    this.pageType.next(PAGE_TYPES.career);
    this.information = [];
  }

  setHobby() {
    this.isLoad.next(true);
    this.warning = false;
    this.pageType.next(PAGE_TYPES.hobby);
    this.information = [];
  }

  setProximity() {
    this.warning = false;
    this.platform.ready().then((readySource) => {
      // this.diagnostic.isLocationEnabled()
      //   .then((isAvailable) => {
      //     if (!isAvailable)
      //       this.alertCtrl.create({
      //         title: 'Unable to find your location',
      //         subTitle: 'Please turn on GPS',
      //         buttons: ['OK']
      //       }).present();
      //     else {
      //       this.isLoad.next(true);
      //       this.pageType.next(PAGE_TYPES.proximity);
      //       this.information = [];
      //       this.sent_requests = [];
      //       this.request = [];
      //     }
      //   });
    });

  }

  openConnect(name, rid) {
    let modal = this.modalCtrl.create('ConnectPage', {
      name: name,
      rid: rid,
      sid: this.uid,
      filter: this.pageType.getValue()
    });
    modal.onDidDismiss(data => {
      if (data.dt == 'msg') {
        this.toastCtrl.create({
          message: 'Invitation sent',
          duration: 3000
        }).present();

        this.information = [];

        this.pageType.next(this.pageType.getValue());
      }
    });
    modal.present();
  }

  openFilter() {
    if (this.pageType.getValue() == PAGE_TYPES.career) {
      let modal = this.modalCtrl.create('FilterPage', { cat: 'career', id: this.uid });
      modal.onDidDismiss(data => {
        if (data.option == 'career') {
          this.information = [];
          this.setCareer();
        }
      });
      modal.present();
    } else if (this.pageType.getValue() == PAGE_TYPES.hobby) {
      let modal = this.modalCtrl.create('FilterPage', { cat: 'hobby', id: this.uid });
      modal.onDidDismiss(data => {
        this.warning = false;
        if (data.option == 'hobby') {
          this.information = [];

          if (data.lat != null) {
            this.lat = data.lat;
          }

          if (data.longg != null) {
            this.long = data.longg;
          }

          let data2 = JSON.stringify({
            id: this.uid,
            hobby: data.hobby,
            lat: this.lat,
            longg: this.long,
            km: data.distance
          });
          this.isLoad.next(true);
          this.http.post(ENV.BASE_URL + '/webservices/Hobby2/hp', data2, this.options).subscribe(result => {
            this.isLoad.next(false);
            let res = JSON.parse(result["_body"]);
            this.information = res[0];
            this.con = res[1];
            this.sent_r = res[2];

            if (this.con != null) {
              for (let j = 0; j < res[1].length; j++)
                this.con_users.push(this.con[j].receiver_id);

              for (let i = 0; i < this.information.length; i++) {
                if (this.con_users.includes(this.information[i].id))
                  this.request[i] = true;
                else
                  this.request[i] = false;
              }
            }
            if (this.sent_r != null) {
              for (let i = 0; i < res[2].length; i++)
                this.sent.push(this.sent_r[i].sender_id);
              for (let j = 0; j < this.information.length; j++) {
                if (this.sent.includes(this.information[j].id))
                  this.sent_requests[j] = true;
                else
                  this.sent_requests[j] = false;
              }
            }

            if (this.information == '')
              this.noinfo = true;
            else
              this.noinfo = false;
          });
        }
      });
      modal.present();
    } else if (this.pageType.getValue() == PAGE_TYPES.proximity) {
      let modal = this.modalCtrl.create('FilterPage', { cat: 'proximity', id: this.uid });
      modal.onDidDismiss(data => {
        this.warning = false;
        if (data.option == 'proxim') {

          this.information = [];
          this.request = [];
          this.sent_requests = [];
          this.lat = data.lat;
          this.long = data.long;
          let data2 = JSON.stringify({ id: this.uid, lat: this.lat, longg: this.long, km: data.dist });
          this.http.post(ENV.BASE_URL + '/webservices/Hobby2/distance2', data2, this.options).subscribe(result => {
            let res = JSON.parse(result["_body"]);
            this.information = res.suggestion;
            this.con = res.request;
            this.sent_r = res.request2;

            if (this.con != null) {
              for (let j = 0; j < this.con.length; j++)
                this.con_users.push(this.con[j].receiver_id);

              for (let i = 0; i < this.information.length; i++) {
                if (this.con_users.includes(this.information[i].id))
                  this.request[i] = true;
                else
                  this.request[i] = false;
              }
            }
            if (this.sent_r != null) {
              for (let i = 0; i < this.sent_r.length; i++)
                this.sent.push(this.sent_r[i].sender_id);
              for (let j = 0; j < this.information.length; j++) {
                if (this.sent.includes(this.information[j].id))
                  this.sent_requests[j] = true;
                else
                  this.sent_requests[j] = false;
              }
            }

            if (this.information == '')
              this.noinfo = true;
            else
              this.noinfo = false;
          });
        }
      });
      modal.present();
    } else
      return;
  }

  getHobby() {
    this.noinfo = false;
    let data = JSON.stringify({ id: this.uid });
    this.http.post(ENV.BASE_URL + '/webservices/Hobby2/getsuggestion', data, this.options).subscribe(result => {
      this.isLoad.next(false);
      let res = JSON.parse(result["_body"]);
      if (res.complete == 0)
        this.warning = true;
      else
        this.warning = false;

      if (!this.warning) {
        this.con = res[1];
        this.information = res[0];
        this.sent_r = res[2];

        //Connected users
        if (this.con != null) {
          for (let j = 0; j < res[1].length; j++)
            this.con_users.push(this.con[j].receiver_id);

          for (let i = 0; i < this.information.length; i++) {
            if (this.con_users.includes(this.information[i].id))
              this.request[i] = true;
            else
              this.request[i] = false;
          }
        }
        if (this.sent_r != null) {
          for (let i = 0; i < res[2].length; i++)
            this.sent.push(this.sent_r[i].sender_id);
          for (let j = 0; j < this.information.length; j++) {
            if (this.sent.includes(this.information[j].id))
              this.sent_requests[j] = true;
            else
              this.sent_requests[j] = false;
          }
        }
        if (this.information == '')
          this.noinfo = true;
        else
          this.noinfo = false;
      }

    });
  }

  getProximity() {
    this.warning = false;
    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat = resp.coords.latitude;
      this.long = resp.coords.longitude;
      let data = JSON.stringify({ id: this.uid, lat: this.lat, longg: this.long, km: '20' });

      this.http.post(ENV.BASE_URL + '/webservices/Hobby2/distance2', data, this.options).subscribe(result => {
        this.isLoad.next(false);
        let res = JSON.parse(result["_body"]);
        this.information = res.suggestion;
        this.con = res.request;
        this.sent_r = res.request2;
        if (res.career_available == "No")
          this.warning = true;
        else
          this.warning = false;

        if (!this.warning) {
          if (this.con != null) {
            for (let j = 0; j < this.con.length; j++)
              this.con_users.push(this.con[j].receiver_id);
            for (let i = 0; i < this.information.length; i++) {
              if (this.con_users.includes(this.information[i].id))
                this.request[i] = true;
              else
                this.request[i] = false;
            }
          }
          if (this.sent_r != null) {
            for (let i = 0; i < this.sent_r.length; i++)
              this.sent.push(this.sent_r[i].sender_id);
            for (let j = 0; j < this.information.length; j++) {
              if (this.sent.includes(this.information[j].id))
                this.sent_requests[j] = true;
              else
                this.sent_requests[j] = false;
            }
          }
          if (this.information == '')
            this.noinfo = true;
          else
            this.noinfo = false;
        }
      });
    }, (err) => {

      this.storage.get('lat').then(lat => {
        this.lat = lat;
        this.storage.get('long').then(long => {
          this.long = long;

          let data = JSON.stringify({ id: this.uid, lat: this.lat, longg: this.long, km: '20' });

          this.http.post(ENV.BASE_URL + '/webservices/Hobby2/distance2', data, this.options).subscribe(result => {
            let res = JSON.parse(result["_body"]);

            this.information = res.suggestion;
            this.con = res.request;
            this.sent_r = res.request2;
            if (res.career_available == "No")
              this.warning = true;
            else
              this.warning = false;

            if (!this.warning) {
              if (this.con != null) {
                for (let j = 0; j < this.con.length; j++)
                  this.con_users.push(this.con[j].receiver_id);
                for (let i = 0; i < this.information.length; i++) {
                  if (this.con_users.includes(this.information[i].id))
                    this.request[i] = true;
                  else
                    this.request[i] = false;
                }
              }
              if (this.sent_r != null) {
                for (let i = 0; i < this.sent_r.length; i++)
                  this.sent.push(this.sent_r[i].sender_id);
                for (let j = 0; j < this.information.length; j++) {
                  if (this.sent.includes(this.information[j].id))
                    this.sent_requests[j] = true;
                  else
                    this.sent_requests[j] = false;
                }
              }

              if (this.information == '')
                this.noinfo = true;
              else
                this.noinfo = false;
            }
          });

        });
      });
    });
  }

  getCareer() {
    let inform = JSON.stringify({ id: this.uid });

    const headerDict = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': this.uid,
    };

    const requestOptions = {
      headers: new HttpHeaders(headerDict),
    };

    this.http.get(ENV.BASE_URL + '/api/careers', requestOptions).subscribe(r => {
      this.isLoad.next(false);
      let res = JSON.parse(r["_body"]);
      this.information = res;
      this.http.post(ENV.BASE_URL + '/webservices/Hobby2/work', inform).subscribe(result => {
        let res = JSON.parse(result["_body"]);
        if (res[0].complete == 0)
          this.warning = true;
        else
          this.warning = false;

        this.con = res[4];
        this.sent_r = res[5];
        let show_q = res[6];
        if (show_q[0].flag == '0')
          this.show_questions = true;
        else
          this.show_questions = false;
        if (this.information != '') {
          this.noinfo = false;
          if (this.con != null) {
            for (let j = 0; j < res[4].length; j++)
              this.con_users.push(this.con[j].receiver_id);

            for (let i = 0; i < this.information.length; i++) {
              if (this.con_users.includes(this.information[i].id))
                this.request[i] = true;
              else
                this.request[i] = false;
            }
          }
          if (this.sent_r != null) {
            for (let i = 0; i < res[5].length; i++)
              this.sent.push(this.sent_r[i].sender_id);
            for (let j = 0; j < this.information.length; j++) {
              if (this.sent.includes(this.information[j].id))
                this.sent_requests[j] = true;
              else
                this.sent_requests[j] = false;
            }
          }
        } else {
          this.noinfo = true;
        }
        if (this.warning) {
          this.noinfo = false;
        }
      });
    })
  }

  add_cdetails() {
    this.navCtrl.push('AddCareerPage', { field: 'add', id: this.uid });
  }

  add_cdetailsOnly() {
    let modal = this.modalCtrl.create('AddCareerPage', { field: 'none', id: this.uid });
    modal.onDidDismiss(data => {
      if (data.opt == 'data')
        this.pageType.next(PAGE_TYPES.career)
    });
    modal.present();
  }

  add_hdetails() {
    let hobbies1 = [];
    let modal = this.modalCtrl.create('EditHobbyPage', { id: this.uid, info: hobbies1, field: 'add_hobby' });
    modal.onDidDismiss(data => {
      if (data.msg == 'yes') {
        this.pageType.next(PAGE_TYPES.hobby);
      }
    });
    modal.present();
  }

  presentPopover(myEvent, fid) {
    let popover = this.popoverCtrl.create('PopoverPage', { 'fid': fid, 'page': 'profiles' });
    popover.present({
      ev: myEvent
    });
  }

  ngOnInit() {
    this.subscriptions.push(
      this.auth.me()
        .subscribe(user => {
          this.uid = user.id;
          this.img = user.img;
          this.name = user.firstname;

          this.menuCtrl.enable(true);
          this.pageType = new BehaviorSubject(PAGE_TYPES.career);
          this.isLoad = new BehaviorSubject(true);
          this.subscriptions.push(
            this.isLoad
              .subscribe(isLoad => {
                const loader = this.loadingCtrl.create({
                  content: 'Please wait while we connect you to like minded females...',
                  duration: 3000
                });
      
                isLoad ? loader.present() : loader.dismiss();
              })
          );
        }),

      this.pageType.pipe(
        filter(type => !!type)
      )
      .subscribe(type => {
        switch (type) {
          case PAGE_TYPES.career:
            this.getCareer();
            break;
          case PAGE_TYPES.hobby:
            this.getHobby();
            break;
          case PAGE_TYPES.proximity:
            this.getProximity();
            break
        }
      }),
    )
  }

  ngOnDestroy() {
    this.subscriptions.forEach(item => item.unsubscribe());
  }
}
