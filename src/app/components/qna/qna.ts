import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController, ModalController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { environment as ENV } from '../../../environments/environment' ;
import { AuthService } from '../../../services/auth.service';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-qna',
  templateUrl: 'qna.html',
})
export class QnaPage implements OnInit, OnDestroy {

  loading: any;
  community: any;
  content: any;
  searchStr = '';
  finance: any;
  tech: any;
  founder: any;
  fintech: any;
  insurance: any;
  req: any;
  uname: any;
  lname: any;
  uimg: any;
  f1: boolean;
  f2: boolean;
  f3: boolean;
  f4: boolean;
  f5: boolean;
  isItemAvailable: boolean;
  timerId = null;

  showSearch: boolean = false;

  activeIndex: number;


  isClickedOnce :boolean = false;


  communityForFilter = [];

  freshArr = [];

  uid: any;

  response: any;
  comp: any;

  adminEmail: boolean = false;
  

  private subscriptions: Subscription[] = [];
  

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCtrl: LoadingController, 
    public alertCtrl: AlertController, 
    public http: HttpClient, 
    public storage: Storage, 
    public toastCtrl: ToastController, 
    public modalCtrl: ModalController,
    private auth: AuthService
    ) {
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        duration: 5000
      });
      this.loading.present();
  }

  ngOnInit() {
    this.subscriptions.push(
      this.auth.me()
        .subscribe(user => {
          this.uid = user.id;
          this.uimg = user.img;
          this.uname = user.firstname;
          if(user.email == "shaha28@me.com") {
            this.adminEmail = true;
          }
        })
    )

    this.http.get( ENV.BASE_URL + '/api/communities').subscribe( r => {
      this.community = JSON.parse(r["_body"]);
      this.loading.dismiss();
      this.community.forEach( element => {
        element.communities.forEach( el => {
          if(el !== null) {
              this.communityForFilter.push(el);
          }
        });
      });
      this.freshArr = this.sortArr(this.communityForFilter, 'title');
    });
  }

  cancelSearch() {
    this.searchStr = '';
  }

  sortArr(originalArr, value) {
      let newArr = [];
      let searchedObj = {}

      for (var i in originalArr) {
        searchedObj[originalArr[i][value]] = originalArr[i];
      }

      for (i in searchedObj) {
        newArr.push(searchedObj[i]);
      }

      return newArr
  }

  company_in(comp) {
    console.log(comp)
    this.isClickedOnce = true;
    this.http.get( ENV.BASE_URL + '/api/communities/'+ comp.id +'/users').subscribe( r => {
      this.response = JSON.parse(r["_body"]);
      this.comp = comp;
      this.isClickedOnce = false;
      this.checkUser();
    });
  }

  checkUser() {
    // hardcode for Shenna
    if(this.response.users.length !== 0 && this.adminEmail) {
      console.log('it')
      this.navCtrl.push('GroupsPage', { comp: this.comp, role: 'admin'});
    }
    let item = this.response.users.filter(item => (item.id === this.uid) );
    if( item.length == 0 ){
      ///Join
      let profileModal = this.modalCtrl.create( 'JoinCommunityModal', { comp: this.comp });
      profileModal.present();
    } else {
      if ( item[0].id === this.uid || this.adminEmail) {
        this.navCtrl.push('GroupsPage', { comp: this.comp, role: item.role});
      } else {
        ///Join
        let profileModal = this.modalCtrl.create( 'JoinCommunityModal', { comp: this.comp });
        profileModal.present();
      }
    }
  }

  getItems(ev) {
    this.showSearch = true;
    this.searchStr = ev.target.value;
  }

  closeSearch() {
    this.searchStr = '';
    this.showSearch = false;
  }

  getFilteredItems() {
    let items = [];


    items = this.freshArr.filter((item, i) => {
        return item.title.toLowerCase().indexOf(this.searchStr.toLowerCase()) > -1;
    });

    return items;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(item => item.unsubscribe());
  }

}
