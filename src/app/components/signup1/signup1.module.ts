import { NgModule } from '@angular/core';
import { Signup1Page } from './signup1';
import { IonicPageModule } from '@ionic/angular';
import { LinkedinService } from '../../../services/linkedin.service';

// import { AngularFireModule } from '@angular/fire';

// export const firebaseConfig = {
//   apiKey: 'AIzaSyCUzInLJGc5HCGxc7Gr6-4wnVUL5OlAJTY',
//   authDomain: 'elevather-959ba.firebaseapp.com',
//   databaseURL: 'https://elevather-959ba.firebaseio.com',
//   projectId: 'elevather-959ba',
//   storageBucket: 'elevather-959ba.appspot.com',
//   messagingSenderId:'elevather-959ba'
// };

@NgModule({
  declarations: [Signup1Page],
  imports: [
    IonicPageModule.forChild(Signup1Page),
    // AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [
    LinkedinService
  ],
  exports: [Signup1Page]
})
export class Signup1PageModule { }
