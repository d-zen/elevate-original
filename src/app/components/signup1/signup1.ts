import {Component, OnDestroy} from '@angular/core';
import {
  NavController,
  NavParams,
  MenuController,
  LoadingController,
  ToastController,
  ModalController,
  AlertController,
  IonicPage,
  Events
} from '@ionic/angular';
import {ImagePicker} from '@ionic-native/image-picker/ngx';
import {Crop} from '@ionic-native/crop/ngx';
import {FileTransfer, FileUploadOptions, FileTransferObject} from '@ionic-native/file-transfer/ngx';
import {Storage} from '@ionic/storage';
import {ElementRef, NgZone, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
// import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import * as firebase from 'firebase';
import {AuthService} from "../../../services/auth.service";
import {LinkedinService} from "../../../services/linkedin.service";
import {Subscription} from "rxjs";
import { environment as ENV } from "../../../environments/environment";
import { HttpClient } from '@angular/common/http';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-signup1',
  templateUrl: 'signup1.html',
})
export class Signup1Page implements OnDestroy {

  capchaView: boolean;
  verifyPhone = false;

  disabled: boolean;
  showData: boolean;
  name: string;
  last_name: string;
  password: string;
  email: string;
  phone: string;
  questions: any[];
  check: any = [];
  photos: any;
  showPass: boolean;
  type: string;
  time: any;
  question: any;
  answer: any;
  id: any;
  showmsg: boolean;
  scopes: any;
  exists: boolean;
  loading: any;
  title: any;
  cname: any;
  start_m: any;
  start_y: any;
  mentor: any;
  address: any;
  agree: boolean;
  select: boolean;
  verify_l: boolean;
  verify_b: boolean = true;
  ctry: any;
  toast: any;
  subscriptions: Subscription[] = [];
  female: boolean;
  public recaptchaVerifier: firebase.auth.RecaptchaVerifier;
  public searchControl: FormControl;

  @ViewChild("search", {static: false})
  public searchElementRef: ElementRef;

  constructor(public events: Events,
              public navCtrl: NavController,
              public navParams: NavParams,
              public menuCtrl: MenuController,
              private imagePicker: ImagePicker,
              public cropService: Crop,
              private transfer: FileTransfer,
              public http: HttpClient,
              public storage: Storage,
              public loadingCtrl: LoadingController,
              public toastCtrl: ToastController,
              public modalCtrl: ModalController,
              public alertCtrl: AlertController,
              private linkedin: LinkedinService,
              private auth: AuthService,
  ) {

    this.storage.remove('new_id');
    this.menuCtrl.enable(false);
    this.showPass = false;
    this.showmsg = false;
    this.type = 'password';
    this.showData = false;
    this.disabled = false;
    this.mentor = 'yes';
    this.agree = false;
    this.select = true;
    this.verify_l = true;
    this.female = false;
    this.questions = ['My last water cooler conversation was...', 'My most awkward conversation I had at work was...', 'My next holiday I want to go on is...', 'When I was younger I wanted to grow up and be...', 'My unusual skill is...', 'I geek out on...', 'The most spontaneous thing I have done is...', 'A unique fact about me is...', 'This year I really want to...', 'After work you can find me...', 'The award I should be nominated for is...', 'My best travel story is...', 'I recently discovered that...'];
    this.loading = loadingCtrl.create();
    this.time = (new Date).getTime();

  }

  ionViewDidLoad() {
    //this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
  }

  ngOnInit() {

    //create search FormControl
    this.searchControl = new FormControl();

    //load Places Autocomplete
  }

  showPassword() {
    this.showPass = !this.showPass;

    if (this.showPass)
      this.type = 'text';
    else
      this.type = 'password';
  }

  changeNumber(v) {
    let number = v;
    console.log(v);
    if (number !== '' && number.length > 5) {
      this.verify_b = false;
    } else {
      this.verify_b = true;
    }
  }


  getImage() {
    this.imagePicker.requestReadPermission().then(() => {


      var options = {
        maximumImagesCount: 1,
        width: 800,
        height: 800,
        quality: 80,
      };

      this.imagePicker.hasReadPermission().then(hasReadPermission => {
        if (hasReadPermission) {
          this.imagePicker.getPictures(options)
            .then((results) => {

              this.reduceImages(results).then((croppedImage) => {

                if (croppedImage != null) {
                  this.photos = croppedImage;
                  this.showmsg = true;
                } else
                  return;

              });
            });
        }
      }, (err) => {
        this.select = false;
      });

    }, (err) => {
      this.select = false;
    });

  }

  reduceImages(selected_pictures: any): any {
    return selected_pictures.reduce((promise: any, item: any) => {
      return promise.then((result) => {
        return this.cropService.crop(item, {quality: 80})
          .then(cropped_image => cropped_image);
      });
    }, Promise.resolve());
  }

  transfer_img(path: any): any {
    const fileTransfer: FileTransferObject = this.transfer.create();

    let options_f: FileUploadOptions = {
      fileKey: 'file',
      fileName: this.time + '.jpg',
      chunkedMode: false,
      mimeType: "image/jpg",
      params: {
        filename: this.time
      }
    };

    let url = ENV.BASE_URL + 'file_upload.php';
    fileTransfer.upload(path, url, options_f)
      .then((data) => {
      });
  }

  openPage() {
    this.navCtrl.setRoot('HomePage');
  }

  check_terms(event) {
    this.disabled = false;
  }


  openNext() {
    this.disabled = true;
    if (this.name == null || this.last_name == null || this.email == null || this.password == null || this.question == null || this.answer == null || this.agree == false || this.female == false) {
      this.disabled = false;
      if (!this.agree) {
        this.toastCtrl.create({
          message: 'Please accept the terms and conditions',
          duration: 3000
        }).present();
      }
      if (!this.female) {
        this.toastCtrl.create({
          message: 'Please confirm you are a female',
          duration: 3000
        }).present();
      } else {
        this.toastCtrl.create({
          message: 'Please fill in all the details',
          duration: 3000
        }).present();
      }
    } else {
      this.verify_l = true;
      this.signup();
    }

  }

  signup() {
    this.disabled = true;
    let img = 'default.png';
    if (this.photos != null) {
      this.transfer_img(this.photos);
      img = this.time + '.jpg';
    }
    let data = {
      fname: this.name,
      lname: this.last_name,
      email: this.email,
      pass: this.password,
      iceque: this.question,
      iceans: this.answer,
      img: img
    };

    this.loading.present();

    this.subscriptions.push(
      this.auth
        .register(data)
        .subscribe(async token => {
          await this.auth.setToken(token);
          this.loading.dismiss();
          this.modalCtrl.create("WizardPage", { token: token }).present();

        }, err => {
          this.loading.dismiss();
          if (err.hasOwnProperty('errors')) {
            err.errors.forEach(this.showErr.bind(this));
          }
        })
    );
  }

  terms() {
    this.modalCtrl.create('AboutUsPage', {data: 'terms'}).present();
  }

  policy() {
    this.modalCtrl.create('AboutUsPage', {data: 'privacy'}).present();
  }

  signInPhone(phoneNumber: number) {
    this.verify_b = false;

    let d_code: string;

    if (this.ctry == 'United Kingdom') {
      d_code = '44';
    } else if (this.ctry == 'Sweden') {
      d_code = '46';
    } else if (this.ctry == 'Canada' || this.ctry == 'United States') {
      d_code = '1';
    } else {
      d_code = '';

    }

    let phoneNumberString = '+' + d_code + phoneNumber;

    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    const appVerifier = this.recaptchaVerifier;
    this.capchaView = true;
    firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
      .then(confirmationResult => {

        let prompt = this.alertCtrl.create({
          title: 'Enter the Confirmation code',
          inputs: [{name: 'confirmationCode', placeholder: 'Confirmation Code'}],
          buttons: [
            {
              text: 'Cancel',
              handler: data => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Send',
              handler: data => {
                confirmationResult.confirm(data.confirmationCode)
                  .then(result => {
                    this.verifyPhone = true;
                    this.signup();
                    this.verify_l = true;
                    this.capchaView = false;
                  }, (err) => {
                    this.toastCtrl.create({
                      message: 'Could not verify',
                      duration: 2000
                    }).present();
                  });
              }
            }
          ]
        });
        prompt.present();
      })
      .catch(function (error) {
        console.error("SMS not sent", error);
        this.capchaView = false;
        this.verifyPhone = false;
        this.toastCtrl.create({
          message: 'An error was encountered',
          duration: 2000
        }).present();
      });

    setTimeout(() => {
      this.verify_b = true;
    }, 2000);

  }

  private showErr(err) {
    try {
      this.toast.dismiss();
    } catch (e) {

    }
    this.toast = this.toastCtrl.create({
      message: err,
      duration: 3000
    }).present();
  }

  linkedinLogin() {
    this.linkedin.login().subscribe(token => {
      console.log(token);
      this.auth.setToken(token);
    })
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe())
  }
}
