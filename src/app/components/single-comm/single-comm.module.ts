import { NgModule } from '@angular/core';
import { IonicPageModule } from '@ionic/angular';
import { SingleCommPage } from './single-comm';
import { EmojiPickerModule } from '@ionic-tools/emoji-picker';


@NgModule({
  declarations: [SingleCommPage],
  imports: [IonicPageModule.forChild(SingleCommPage),
  EmojiPickerModule],
  exports: [SingleCommPage]
})
export class SingleCommPageModule {}
