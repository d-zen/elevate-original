import {
  Component,
  ViewChild,
  OnInit,
  OnDestroy,
  OnChanges,
  ElementRef,
  Renderer
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  PopoverController,
  Content,
  ModalController,
  AlertController
} from "@ionic/angular";
import * as firebase from "firebase";
import { Storage } from "@ionic/storage";
import { BehaviorSubject, Subscription, combineLatest } from "rxjs";
import { AuthService } from "../../../services/auth.service";
import { map, filter } from 'rxjs/operators';
import { ImagePicker } from "@ionic-native/image-picker/ngx";
import { environment as ENV } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as Centrifuge from "centrifuge";

// const Centrifuge = require("centrifuge");

@IonicPage()
@Component({
  selector: "page-single-comm",
  templateUrl: "single-comm.html"
})
export class SingleCommPage implements OnInit, OnDestroy {
  channel = "public:channel_";

  community: any;
  questions: any = [];
  answers: any = [];
  name: any = [];
  len: any = [];
  more_ans: any = [];
  loading: any;
  gid: any;
  cid: any;
  tab: any;
  coll: any = [];
  show_more: any = [];
  show_post: any = [];
  toggled: boolean = false;
  toggled1: boolean = false;
  post_q = "";
  post_a: any;
  id: any;
  uid: any;
  uname: any;
  img: any;
  empty: boolean = true;

  messages: any = [];
  channelId: BehaviorSubject<number> = new BehaviorSubject(0);
  newMessagesValue = "";
  showEdit = false;
  showEditReply = false;
  anonymous = false;

  messageId: any;

  imgBase = null;

  token: any;

  replyMessageId: any;

  replyBtn = false;
  replyMsg: any;

  messageIndex: any;

  anonymousNum: number = 0;

  private centrifuge = new Centrifuge(ENV.SOCKET_URL);

  adminRights: boolean = false;

  private subscriptions: Subscription[] = [];

  @ViewChild(Content, {static: false}) content: Content;
  @ViewChild("input", {static: false}) input: ElementRef;

  constructor(
    private alertCtrl: AlertController,
    private auth: AuthService,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public popoverCtrl: PopoverController,
    public storage: Storage,
    public modalCtrl: ModalController,
    public http: HttpClient,
    private imagePicker: ImagePicker,
  ) {


    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
    this.gid = this.navParams.get("gid");
  }

  ionViewDidEnter() {
    this.content.scrollToBottom(200);
  }

  ngOnInit() {
    this.subscriptions.push(
      this.auth.me().subscribe(user => {
        this.uid = user.id;
        this.img = user.img;
        this.name = user.firstname;
      })
    );
    this.auth.token.pipe(
      filter(token => !!token)
    )
      .subscribe(token => {
        this.token = token;
      });
    this.setInfo();


    combineLatest(
      this.auth.token,
      this.channelId.asObservable()
    ).pipe(
      map(([token, channelId]) => {
        return {token, channelId}
      }),
      filter(({token, channelId}) => channelId !== 0)
    ).subscribe(({token, channelId}) => {
      this.centrifuge.setToken(token);
      this.centrifuge.subscribe(this.channel + channelId, r => {
        console.log(r);
        switch (r.data.type) {
          case "CREATE_MESSAGE":
            this.messages.push(r.data.message);
            break;
          case "UPDATE_MESSAGE":
            this.messages.forEach((el, i) => {
              if (el.id == r.data.message.id) {
                el.text = r.data.message.text;
              }
            });
            break;
          case "DELETE_MESSAGE":
            let currentIndex: number;
            this.messages.forEach((el, i) => {
              if (el.id == r.data.message.id) {
                currentIndex = i;
                this.messages.splice(currentIndex, 1);
              }
            });
            break;
          case "CREATE_REPLY":
            this.messages.map((mes, i) => {
              if (mes.id == r.data.message.message_id) {
                this.messages[i].replies = this.messages[i].replies || [];
                this.messages[i].replies.push(r.data.message);
              }
            });
            break;
          case "UPDATE_REPLY":
            this.messages[this.messageIndex].replies.forEach((rep, j) => {
              if (rep.id == r.data.message.id) {
                this.messages[this.messageIndex].replies[j].text = r.data.message.text;
              }
            });
            break;
          case "DELETE_REPLY":
            this.messages[this.messageIndex].replies.forEach((rep, j) => {
              if (rep.id == r.data.message.id) {
                this.messages[this.messageIndex].replies.splice(j, 1);
              }
            });
            break;
          default:
            return null;
        }
      });
      this.centrifuge.connect();
    });
  }


  setInfo() {
    this.http.get(ENV.BASE_URL + "api/channels/" + this.gid).subscribe(r => {
      const response = JSON.parse(r["_body"]);
      this.community = response.title;
      if (response.messages.length > 0) {
        this.empty = false;
      }
      this.messages = response.messages;
      console.log(this.messages);
      this.channelId.next(response.id);
      this.loading.dismiss();
    });
  }


  ngOnDestroy() {
    this.subscriptions.forEach(item => item.unsubscribe());
  }

  presentPopoverMessageOptions(myEvent, com_id) {
    let popover = this.popoverCtrl.create("PopoverPage", {
      page: "single_comm",
      com_id: com_id
    });
    popover.present({
      ev: myEvent
    });
  }

  scroll_page_up() {
    setTimeout(() => {
      this.content.scrollToBottom(500);
    }, 500);
  }

  set_ans(i) {
    if (!this.show_post[i]) this.show_post[i] = true;
    else this.show_post[i] = false;
  }

  collapse_p(i, id) {
    var reptRef1 = firebase.database().ref();
    reptRef1.once("value", personSnapshot => {
      let myData = personSnapshot.val();
      let obj = myData.questions;
      for (var j in obj) {
        if (obj[j] != null && obj[j].id == id) {
          let an = [];
          an = obj[j].answers;
          if (an != null) {
            let ind = 0;
            this.more_ans[i] = {answer: []};
            for (let k = 0; k < an.length; k++) {
              this.more_ans[i].answer[ind] = {};
              this.more_ans[i].answer[ind].ans = an[k].ans;
              this.more_ans[i].answer[ind].name = an[k].uname;
              this.more_ans[i].answer[ind].userid = an[k].userid;
              ind++;
            }
          }
        }
      }
    });
    this.show_more[i] = true;
    this.coll[i] = false;
    this.show_post[i] = true;
  }

  hide_p(i, id) {
    this.show_more[i] = false;
    this.coll[i] = true;
    this.show_post[i] = false;
  }

  sendMessage() {
    const headers = new HttpHeaders();

    headers.append("Authorization", this.token);
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");

    const header = {headers: headers};

    let options = {};

    if (this.imgBase == null) {
      options = {
        channel_id: this.channelId.getValue(),
        anonymous: this.anonymousNum,
        text: this.post_q,
      };
    } else {
      options = {
        channel_id: this.channelId.getValue(),
        anonymous: this.anonymousNum,
        text: this.post_q,
        attachments: this.imgBase
      };
    }

    this.http
      .post(ENV.BASE_URL + "api/messages", options, header)
      .subscribe(r => {
        this.post_q = "";
        this.empty = false;
        this.imgBase = null;
        this.content.scrollToBottom(300);
      });
  }


  replyOnClick(id, message, index) {
    this.messageId = id;
    this.messageIndex = index;
    this.replyBtn = true;
    if (message.length < 40) {
      this.replyMsg = message;
    } else {
      this.replyMsg = message.substring(0, 40) + '...';
    }
  }

  closeReply() {
    this.replyBtn = false;
    this.replyMsg = '';
  }

  sendReply() {
    const headers = new HttpHeaders();

    headers.append("Authorization", this.token);
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");

    const header = {headers: headers};

    let options = {};

    if (this.imgBase == null) {
      options = {
        channel_id: this.channelId.getValue(),
        anonymous: this.anonymousNum,
        text: this.post_q,
      };
    } else {
      options = {
        channel_id: this.channelId.getValue(),
        anonymous: this.anonymousNum,
        text: this.post_q,
        attachments: this.imgBase
      };
    }

    this.http
      .post(ENV.BASE_URL + "api/replies", options, header)
      .subscribe(r => {
        this.post_q = "";
        this.replyBtn = false;
        this.imgBase = null;
      });
  }

  delReply(id, index) {
    const headers = new HttpHeaders();
    this.messageIndex = index;

    headers.append("Authorization", this.token);
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");

    const header = {headers: headers};

    this.http
      .delete(ENV.BASE_URL + "/api/replies/" + id, header)
      .subscribe(r => {
        let alert = this.alertCtrl.create({
          message: "Your reply message has been deleted"
        });
        alert.present();
      });
  }

  delMessage(id) {
    const headers = new HttpHeaders();

    headers.append("Authorization", this.token);
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");

    const header = {headers: headers};

    this.http
      .delete(ENV.BASE_URL + "/api/messages/" + id, header)
      .subscribe(r => {
        let alert = this.alertCtrl.create({
          message: "Your message has been deleted"
        });
        alert.present();
      });
  }

  editMessage(id, text) {
    // this.input.nativeElement.focus();
    this.post_q = text;
    this.messageId = id;
    this.showEdit = true;
    this.showEditReply = false;
  }

  editReply(id, text, index) {
    this.post_q = text;
    this.replyMessageId = id;
    this.showEditReply = true;
    this.showEdit = true;
    this.messageIndex = index;
  }

  sendEditMessage() {
    const headers = new HttpHeaders();
    headers.append("Authorization", this.token);
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");

    const header = {headers: headers};

    const options = JSON.stringify({
      channel_id: this.channelId.getValue(),
      anonymous: this.anonymousNum,
      text: this.post_q
    });

    this.http
      .put(ENV.BASE_URL + "/api/messages/" + this.messageId, options, header)
      .subscribe(r => {
        this.showEdit = false;
        this.post_q = "";
        let alert = this.alertCtrl.create({
          message: "Your message has been changed"
        });
        alert.present();
      });
  }

  sendEditReply() {
    const headers = new HttpHeaders();
    headers.append("Authorization", this.token);
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");

    const header = {headers: headers};

    const options = JSON.stringify({
      text: this.post_q,
      channel_id: this.channelId.getValue(),
    });

    this.http
      .put(ENV.BASE_URL + "/api/replies/" + this.replyMessageId, options, header)
      .subscribe(r => {
        this.showEdit = false;
        this.post_q = "";
        let alert = this.alertCtrl.create({
          message: "Your reply message has been changed"
        });
        alert.present();
      });
  }

  postInputChanged(event) {
    this.newMessagesValue = event;
  }

  handleSelection(event) {
    this.post_q += event.char;
  }

  handleSelection1(event) {
    this.post_a += event.char;
    this.toggled1 = false;
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create("PopoverPage", {
      page: "questions",
      name: this.uname
    });
    popover.onDidDismiss(data => {
      this.uname = data.str;
      this.img = ENV.BASE_URL + "public/userimage/default.png";
    });
    popover.present({
      ev: myEvent
    });
  }

  openProfile(rid, uname) {
    // let headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // let options = new RequestOptions({headers: headers});
    // let data = JSON.stringify({uid: this.id, fid: rid});
    // this.http
    //   .post(
    //     ENV.BASE_URL + "webservices/Hobby2/get_threadId",
    //     data,
    //     options
    //   )
    //   .subscribe(result => {
    //     let res = JSON.parse(result["_body"]);
    //     let modal;
    //     if (res == "")
    //       modal = this.modalCtrl.create("PersonPage", {id: rid});
    //     else
    //       modal = this.modalCtrl.create("PersonPage", {
    //         id: rid,
    //         page: "comm"
    //       });
    //     modal.present();
    //   });
  }

  toggleAnonymous() {
    this.anonymous = !this.anonymous;
    if (this.anonymous) {
      this.anonymousNum = 1;
    } else {
      this.anonymousNum = 0;
    }
    console.log(this.anonymousNum)
  }


  getImage() {
    var options = {
      maximumImagesCount: 1,
      width: 400,
      height: 400,
      quality: 80,
      outputType: 1
    };
    this.imagePicker.getPictures(options)
      .then((results) => {
        console.log(results, 'my info for img')
        if (typeof results !== 'undefined' && results.length > 0) {
          this.imgBase = 'data:image/jpeg;base64,' + results;
        } else {
          this.imgBase = null;
        }
      }, (err) => {
        console.log(err)
        alert(err)
        this.imgBase = null;
        console.log(this.imgBase)
      });
  }
}
