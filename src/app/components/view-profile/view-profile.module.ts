import { NgModule } from '@angular/core';
import { ViewProfilePage } from './view-profile';
import { IonicPageModule } from '@ionic/angular';
@NgModule({
  declarations: [ViewProfilePage],
  imports: [IonicPageModule.forChild(ViewProfilePage)],
  exports: [ViewProfilePage]
})
export class ViewProfilePageModule { }
