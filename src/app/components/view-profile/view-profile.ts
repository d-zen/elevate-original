import { Component, OnInit, ViewChild } from "@angular/core";
import {
  NavController,
  NavParams,
  ViewController,
  LoadingController,
  ModalController,
  ToastController,
  AlertController,
  IonicPage,
  MenuController,
} from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { ImagePicker } from "@ionic-native/image-picker/ngx";
import { Crop } from "@ionic-native/crop/ngx";
import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject
} from "@ionic-native/file-transfer/ngx";
import { Events } from "@ionic/angular";
import * as firebase from "firebase";
import { AuthService } from "../../../services/auth.service";
import { environment as ENV } from '../../../environments/environment' ;
import { HttpClient } from '@angular/common/http';

@IonicPage()
@Component({
  selector: "page-view-profile",
  templateUrl: "view-profile.html"
})
export class ViewProfilePage implements OnInit {
  shownGroup = null;
  id: any;
  name: string;
  lname: string;
  job: string;
  img: any;
  company: any;
  user: any = {};
  career: any = [];
  education: any = [];
  hobbies: any = [];
  endtime: any = [];
  loading: any;
  no_hobby: boolean;
  show_c: boolean;
  show_e: boolean;
  show_c_button: boolean;
  options: any;
  o_pwd: any;
  n_pwd: any;
  photos: any;
  time: any;
  question: any;
  answer: any;
  location: any;
  h_location: any;
  details: any = {};
  arr12: any = [];
  skill: any;
  d1: string;
  d2: string;
  d3: string;
  d4: string;
  d5: string;
  d6: string;
  d7: string;

  private subscriptions = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public http: HttpClient,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    private imagePicker: ImagePicker,
    public cropService: Crop,
    private transfer: FileTransfer,
    public events: Events,
    public alertCtrl: AlertController,
    private auth: AuthService,
    private menuCtrl: MenuController
  ) {
    this.menuCtrl.close();
  }
  

  extra_pref() {
    let data = JSON.stringify({ id: this.id });
    this.http
      .post(
        ENV.BASE_URL + "/webservices/Hobby2/profile_question",
        data,
        this.options
      )
      .subscribe(result => {
        let res = JSON.parse(result["_body"]);
        this.details = res[0];

        if (this.details.do_you_have_children == 1) this.d1 = "Yes";
        else if (this.details.do_you_have_children == 0) this.d1 = "No";
        else this.d1 = "Prefer not to say";
        if (this.details.are_you_currently_on_maternity == 1) this.d2 = "Yes";
        else if (this.details.are_you_currently_on_maternity == 0)
          this.d2 = "No";
        else this.d2 = "Prefer not to say";
        if (this.details.can_we_contact == 1) this.d3 = "Yes";
        else this.d3 = "No";
        if (this.details.career_change_and_moved_industry == "no")
          this.d4 = "No";
        else if (this.details.career_change_and_moved_industry == "yes")
          this.d4 = "Yes";
        else this.d4 = "Prefer not to say";
        if (this.details.changed_regions_and_uprooted_family == "no")
          this.d5 = "No";
        else if (this.details.changed_regions_and_uprooted_family == "yes")
          this.d5 = "Yes";
        else this.d5 = "Prefer not to say";
        if (this.details.user_mentor == "0") this.d6 = "No";
        else this.d6 = "Yes";
        if (this.details.additional_qualifications == "no") this.d7 = "No";
        else if (this.details.additional_qualifications == "yes")
          this.d7 = "Yes";
        else this.d7 = "Prefer not to say";
      });
  }

  web_service() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 4000
    });
    this.loading.present();

    let data = JSON.stringify({ id: this.id });

    this.http
      .post(
        ENV.BASE_URL + "/webservices/Hobby2/view",
        data,
        this.options
      )
      .subscribe(result => {
        this.hideLoading();
        this.user = JSON.parse(result["_body"]);
        this.location = this.user.userdata.location;
        this.name = this.user.userdata.firstname;
        this.lname = this.user.userdata.lastname;
        this.img = this.user.userdata.img;
        this.question = this.user.userdata.icequestion;
        this.answer = this.user.userdata.iceanswer;
        this.skill = this.user.userdata.teach_on;
        if (this.user.hobbies != null)
          this.h_location = this.user.hobbies.location;
        if (this.user.career == "") {
          this.show_c = true;
          this.job = "";
        } else {
          this.job = this.user.career[0].job_title;
          this.company = this.user.career[0].company_name;
          this.career = this.user.career;
          this.show_c = false;
        }

        if (this.user.education == "") {
          this.show_e = true;
        } else {
          this.education = this.user.education;
          this.show_e = false;
        }

        if (this.user.hobbies == null || this.user.hobbies.interest == "") {
          this.hobbies = ["No information available"];
          this.no_hobby = true;
        } else {
          this.hobbies = this.user.hobbies.interest.split(",");
          this.no_hobby = false;
        }

        for (let i = 0; i < this.career.length; i++) {
          if (this.career[i].iscurrent == "1") this.endtime[i] = "Present";
          else
            this.endtime[i] =
              this.career[i].end_month + "-" + this.career[i].end_year;
        }
      });
  }

  hideLoading() {
    this.loading.dismiss();
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  edit_career(i) {
    let modal = this.modalCtrl.create("EditCareerPage", {
      id: this.id,
      info: this.career[i],
      skill: this.user.userdata.teach_on,
      skill_arr: this.user.userdata.teach_array
    });
    modal.onDidDismiss(data => {
      if (data.msg == "yes") {
        this.toastCtrl
          .create({
            message: "Updated successfully",
            duration: 3000
          })
          .present();
        this.web_service();
      }
    });
    modal.present();
  }

  edit_edu(i) {
    let modal = this.modalCtrl.create("EditEduPage", {
      id: this.id,
      info: this.education[i]
    });
    modal.onDidDismiss(data => {
      if (data.msg == "yes") {
        this.toastCtrl
          .create({
            message: "Updated successfully",
            duration: 3000
          })
          .present();
        this.web_service();
      }
    });
    modal.present();
  }

  edit_hob() {
    if (this.no_hobby == false) {
      let modal = this.modalCtrl.create("EditHobbyPage", {
        id: this.id,
        info: this.hobbies,
        field: "hobby",
        loc: this.h_location
      });
      modal.onDidDismiss(data => {
        if (data.msg == "yes") {
          this.toastCtrl
            .create({
              message: "Updated successfully",
              duration: 3000
            })
            .present();
          this.web_service();
        }
      });
      modal.present();
    } else {
      this.hobbies = [];
      let modal = this.modalCtrl.create("EditHobbyPage", {
        id: this.id,
        info: this.hobbies,
        field: "hobby"
      });
      modal.onDidDismiss(data => {
        if (data.msg == "yes") {
          this.toastCtrl
            .create({
              message: "Updated successfully",
              duration: 3000
            })
            .present();
          this.web_service();
        }
      });
      modal.present();
    }
  }

  del_career(i) {
    this.alertCtrl
      .create({
        title: "Delete",
        subTitle: "Are you sure you want to delete this data?",
        buttons: [
          {
            text: "Delete",
            handler: () => {
              let data = JSON.stringify({ cid: i });
              this.http
                .post(
                  ENV.BASE_URL + "/webservices/Hobby2/delete_career",
                  data,
                  this.options
                )
                .subscribe(result => {
                  this.web_service();
                  this.toastCtrl
                    .create({
                      message: "Deleted",
                      duration: 3000
                    })
                    .present();
                });
            }
          },
          {
            text: "Cancel",
            handler: () => {}
          }
        ]
      })
      .present();
  }

  del_edu(i) {
    this.alertCtrl
      .create({
        title: "Delete",
        subTitle: "Are you sure you want to delete this data?",
        buttons: [
          {
            text: "Delete",
            handler: () => {
              let data = JSON.stringify({ eid: i });
              this.http
                .post(
                  ENV.BASE_URL + "/webservices/Hobby2/delete_education",
                  data,
                  this.options
                )
                .subscribe(result => {
                  this.web_service();
                  this.toastCtrl
                    .create({
                      message: "Deleted",
                      duration: 3000
                    })
                    .present();
                });
            }
          },
          {
            text: "Cancel",
            handler: () => {}
          }
        ]
      })
      .present();
  }

  edit_name() {
    let modal = this.modalCtrl.create("EditHobbyPage", {
      id: this.id,
      name: this.name,
      lname: this.lname,
      field: "name"
    });
    modal.onDidDismiss(data => {
      if (data.msg == "yes") {
        this.toastCtrl
          .create({
            message: "Updated successfully",
            duration: 3000
          })
          .present();
        this.events.publish("user:created", this.id, Date.now());

        var reptRef = firebase.database().ref();
        reptRef.once("value", personSnapshot => {
          let myPerson = personSnapshot.val();
          let obj = myPerson.list;
          let fetched1 = obj[this.id];
          let arr = [];
          let m = 0;
          for (let i in fetched1) {
            if (fetched1[i] != null) {
              if (fetched1[i].receiver_id == this.id) {
                arr[m] = fetched1[i].sender_id;
                m++;
              } else {
                arr[m] = fetched1[i].receiver_id;
                m++;
              }
            }
          }

          for (let j in arr) {
            let id1 = arr[j];
            let fetched1 = obj[id1];
            for (let i in fetched1) {
              if (fetched1[i] != null) {
                if (
                  fetched1[i].receiver_id == this.id ||
                  fetched1[i].sender_id == this.id
                ) {
                  var repRef = firebase
                    .database()
                    .ref("/list/" + id1 + "/" + i + "/");
                  repRef.update({
                    firstname: data.fname,
                    lastname: data.lname
                  });
                }
              }
            }
          }

          let obj5 = myPerson.questions;
          for (let i in obj5) {
            if (obj5[i].userid == this.id) {
              var repRef5 = firebase.database().ref("/questions/" + i + "/");
              repRef5.update({
                name: data.fname
              });
            }
            let answers = obj5[i].answers;
            for (let j in answers) {
              if (answers[j].userid == this.id) {
                var repRef6 = firebase
                  .database()
                  .ref("/questions/" + i + "/answers/" + j + "/");
                repRef6.update({
                  uname: data.fname
                });
              }
            }
          }

          let obj1 = myPerson.community;
          for (let i in obj1) {
            let cmp = obj1[i].companies;
            for (let j in cmp) {
              let grp = cmp[j].groups;
              for (var k in grp) {
                let questions = grp[k].questions;
                for (var l in questions) {
                  if (questions[k].userid == this.id) {
                    var repRef1 = firebase
                      .database()
                      .ref(
                        "/community/" +
                          i +
                          "/companies/" +
                          j +
                          "/groups/" +
                          k +
                          "/questions/" +
                          l +
                          "/"
                      );
                    repRef1.update({
                      uname: data.fname
                    });
                  }
                }
              }
            }
          }
        });

        this.web_service();
      }
    });
    modal.present();
  }

  add_car() {
    let modal = this.modalCtrl.create("AddCareerPage", { field: "no" });
    modal.onDidDismiss(data => {
      if (data.opt == "data") this.web_service();
    });
    modal.present();
  }

  add_car1() {
    this.navCtrl.push("AddCareerPage", { field: "add" });
  }

  add_edu() {
    let modal = this.modalCtrl.create("AddEduPage");
    modal.onDidDismiss(data => {
      if (data.opt == "data") this.web_service();
    });
    modal.present();
  }

  edit_image() {
    this.imagePicker.requestReadPermission().then(() => {
      var options = {
        maximumImagesCount: 1,
        width: 800,
        height: 800,
        quality: 80
      };

      this.imagePicker.hasReadPermission().then(hasReadPermission => {
        if (hasReadPermission) {
          this.imagePicker.getPictures(options).then(results => {
            this.reduceImages(results).then(croppedImage => {
              this.photos = croppedImage;
              let d = new Date();
              this.time = d.getTime();
              if (this.photos != null) {
                this.transfer_img(this.photos, this.time);
                let data1 = JSON.stringify({
                  id: this.id,
                  img: this.time + ".jpg"
                });
                this.http
                  .post(
                    ENV.BASE_URL + "webservices/Hobby2/change_image",
                    data1,
                    this.options
                  )
                  .subscribe(result => {
                    var reptRef = firebase.database().ref();
                    reptRef.once("value", personSnapshot => {
                      let url =
                        ENV.BASE_URL + "public/userimage/" +
                        this.time +
                        ".jpg";
                      let myPerson = personSnapshot.val();
                      let obj = myPerson.list;

                      let fetched1 = obj[this.id];
                      let arr = [];
                      let m = 0;
                      for (let i in fetched1) {
                        if (fetched1[i].receiver_id == this.id) {
                          arr[m] = fetched1[i].sender_id;
                          m++;
                        } else {
                          arr[m] = fetched1[i].receiver_id;
                          m++;
                        }
                      }

                      for (let j in arr) {
                        let id1 = arr[j];
                        let fetched1 = obj[id1];
                        for (let i in fetched1) {
                          if (fetched1[i] != null) {
                            if (
                              fetched1[i].receiver_id == this.id ||
                              fetched1[i].sender_id == this.id
                            ) {
                              var repRef_o = firebase
                                .database()
                                .ref("/list/" + id1 + "/" + i + "/");
                              repRef_o.update({
                                img: url
                              });
                            }
                          }
                        }
                      }

                      let obj1 = myPerson.questions;
                      for (let i in obj1) {
                        if (obj1[i].userid == this.id) {
                          var repRef = firebase
                            .database()
                            .ref("/questions/" + i + "/");
                          repRef.update({
                            uimg: url
                          });
                        }
                      }

                      let obj2 = myPerson.community;
                      for (let i in obj2) {
                        let cmp = obj2[i].companies;
                        for (let j in cmp) {
                          let grp = cmp[j].groups;
                          for (var k in grp) {
                            let questions = grp[k].questions;
                            for (var l in questions) {
                              if (questions[k].userid == this.id) {
                                var repRef1 = firebase
                                  .database()
                                  .ref(
                                    "/community/" +
                                      i +
                                      "/companies/" +
                                      j +
                                      "/groups/" +
                                      k +
                                      "/questions/" +
                                      l +
                                      "/"
                                  );
                                repRef1.update({
                                  uimg: url
                                });
                              }
                            }
                          }
                        }
                      }
                    });

                    let modal = this.modalCtrl.create("AboutUsPage", {
                      data: "picture"
                    });
                    modal.present();
                    modal.onDidDismiss(dt => {
                      this.web_service();
                      this.events.publish("user:created", this.id, Date.now());
                    });
                  });
              }
            });
          });
        }
      });
    });
  }

  joinMore() {
    this.navCtrl.setRoot("QnaPage");
  }

  reduceImages(selected_pictures: any): any {
    return selected_pictures.reduce((promise: any, item: any) => {
      return promise.then(result => {
        return this.cropService
          .crop(item, { quality: 80 })
          .then(cropped_image => cropped_image);
      });
    }, Promise.resolve());
  }

  transfer_img(path: any, time): any {
    const fileTransfer: FileTransferObject = this.transfer.create();

    let options_f: FileUploadOptions = {
      fileKey: "file",
      fileName: time + ".jpg",
      chunkedMode: false,
      mimeType: "multipart/form-data",
    };

    let url = ENV.BASE_URL + "file_upload.php";

    fileTransfer.upload(path, url, options_f).then(
      data => {
        setTimeout(() => {
          this.web_service();
          this.events.publish("user:created", this.id, Date.now());
        }, 2000);
      },
      err => {
        console.log("upload failed!");
      }
    );
  }

  edit_qst() {
    let modal = this.modalCtrl.create("EditHobbyPage", {
      id: this.id,
      details: this.details,
      field: "questions"
    });
    modal.onDidDismiss(data => {
      if (data.msg == "yes") {
        this.loading = this.loadingCtrl.create({
          content: "Please wait...",
          duration: 10000
        });
        this.loading.present();

        this.toastCtrl
          .create({
            message: "Updated successfully",
            duration: 3000
          })
          .present();
        this.extra_pref();
        this.loading.dismiss();
      }
    });
    modal.present();
  }

  delete_all() {
    this.modalCtrl.create("AboutUsPage", { data: "delete" }).present();
  }

  ch_pw() {
    let modal = this.modalCtrl.create("EditHobbyPage", {
      field: "pass",
      id: this.id
    });
    modal.onDidDismiss(data => {
      if (data.msg == "yes") this.web_service();
    });
    modal.present();
  }

  edit_loc() {
    let modal = this.modalCtrl.create("EditHobbyPage", {
      field: "loc",
      id: this.id,
      location: this.location
    });
    modal.onDidDismiss(data => {
      if (data.msg == "yes") this.web_service();
    });
    modal.present();
  }

  logout() {
    this.auth.logout();
    this.navCtrl.setRoot('HomePage')
    this.navCtrl.popToRoot();
    
  }

  ngOnInit(): void {
    // todo refactor it
    this.subscriptions.push(
      this.auth.user.subscribe(user => {
        this.id = user.id;
        this.web_service();
        this.extra_pref();

        let data1 = JSON.stringify({ id: this.id });
        this.http
          .post(
            ENV.BASE_URL + "/webservices/Hobby2/check_incomplete",
            data1,
            this.options
          )
          .subscribe(result => {
            let res = JSON.parse(result["_body"]);
            if (res.complete == "1") {
              this.show_c_button = true;
            } else {
              this.toggleGroup(1);
              this.show_c_button = false;
            }
          });

        this.show_e = false;
        this.http
          .post(
            ENV.BASE_URL + "/webservices/Hobby2/fetch_user_groups",
            { id: this.id },
            this.options
          )
          .subscribe(result => {
            let res = JSON.parse(result["_body"]);

            var reportRef = firebase.database().ref();
            reportRef.once("value", personSnapshot => {
              let myPerson = personSnapshot.val();
              let obj = myPerson.community;

              for (let a = 0; a < res.length; a++) {
                let tab = res[a].tabid;
                let cid = res[a].cid;
                this.arr12[a] = {};
                for (let i in obj) {
                  if (obj[i].comm_id == tab) {
                    this.arr12[a].sector = obj[i].name;
                    let comp = obj[i].companies;
                    for (let j in comp) {
                      if (comp[j].cid == cid) {
                        this.arr12[a].company = comp[j].company;
                      }
                    }
                  }
                }
              }
            });
          });
      })
    );
  }

  ngOnDestroy () {
    this.subscriptions.map(item => {
      item.unsubscribe()
    })
  }
}
