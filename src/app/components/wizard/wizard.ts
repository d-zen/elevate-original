import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthService } from '../../../services/auth.service';
import { Subscription } from 'rxjs';

@IonicPage()
@Component({
  selector: 'page-wizard',
  templateUrl: 'wizard.html',
})
export class WizardPage implements OnInit, OnDestroy {

	id: any;
  name: any;
  disable: boolean;
	@ViewChild('slides', {static: false}) slides: Slides;
  private subscriptions: Subscription[] = [];

  constructor(private auth: AuthService, public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public modalCtrl: ModalController) {

  	this.disable = false;

  }

  ngOnInit() {
    this.subscriptions.push(
      this.auth.me()
        .subscribe(user => {
          this.id = user.id;
          this.name = user.firstname;
        })
    )
  }

  openPage()
  {
  	this.disable = true;
    let modal = this.modalCtrl.create('AddCareerPage',{field: 'add'});
    modal.onDidDismiss(res=>{
      if(res.page == 'addCar')
      {
        let modal1 = this.modalCtrl.create('EditHobbyPage', {field: 'incomplete', id: this.id});
        modal1.onDidDismiss(res1=>{
          if(res1.page == 'next')
          {
            let modal2 = this.modalCtrl.create('AddEduPage', {field: 'incomplete'});
            modal2.onDidDismiss(res2=>{
              if(res2.page == 'addEdu')
                this.navCtrl.push('AboutUsPage', {data: 'add_more', id: this.id});
              else
                this.navCtrl.setRoot('ProfilesPage');
            });
            modal2.present();
          }
          else
            this.navCtrl.pop();
        });
        modal1.present();
      }
    });
    modal.present();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(item => item.unsubscribe());
  }

}
