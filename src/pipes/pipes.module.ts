import { NgModule } from '@angular/core';
import { ArraySortPipe } from './filter-by-name.pipe';

@NgModule({
  imports: [
    // dep modules
  ],
  declarations: [ 
    ArraySortPipe
  ],
  exports: [
    ArraySortPipe
  ]
})
export class PipesModule {}