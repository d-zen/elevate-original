import { throwError as observableThrowError, BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { map, catchError, filter } from 'rxjs/operators';

import { environment as ENV } from '../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

const apiUrl = `${ENV.BASE_URL}api`;
const ADMIN_ROLE = 'admin';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  private _user: BehaviorSubject<any> = new BehaviorSubject({});
  private _isLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private _token: BehaviorSubject<string> = new BehaviorSubject('');
  private _isCommunityAdmin: BehaviorSubject<string> = new BehaviorSubject('');
  private _communities: BehaviorSubject<string> = new BehaviorSubject('');

  public readonly user = this._user.asObservable();
  public readonly token = this._token.asObservable();
  public readonly isLoggedIn = this._isLoggedIn.asObservable();
  public readonly isCommunityAdmin = this._isCommunityAdmin.asObservable();
  public readonly communities = this._communities.asObservable();

  constructor(private http: HttpClient, private storage: Storage) {
  }

  exchangeToken(token: string) {
    const headers = new HttpHeaders();
    headers.append('Authorization', token);
    headers.append('Content-Type', 'application/json');
    const reqOpts = {
      headers,
    };

    return this.http.get(`${apiUrl}/auth/me`, reqOpts)
      .pipe(
        map((res: any) => {
          return res.json();
        }),
        catchError(this.handleErr)
      );
  }

  setToken(token: string) {
    this._token.next(token);

    return this.storage.set('token', token);
  }

  setLoggedIn(loggedIn) {
    this._isLoggedIn.next(loggedIn);
  }

  setUser(user) {
    const isCommunityAdmin = user.communities.map((item) => item.role === ADMIN_ROLE);
    this._user.next(user);
    this._isCommunityAdmin.next(isCommunityAdmin);
  }

  me() {
    return this.user.pipe(
      filter(user => !!user)
    );
  }

  login(email, password): Observable<string> {
    return this.http.post(`${apiUrl}/auth/login`, {email, password})
      .pipe(
        map((data: any) => {
          return data.json().token;
        }),
        catchError(this.handleErr)
      );
  }

  register(data): Observable<string> {
    return this.http.post(`${apiUrl}/auth/register`, data)
      .pipe(
        map((r: any) => {
          return r.json().token;
        }),
        catchError(this.handleErr)
      );
  }

  logout() {
    return this.storage.remove('token').then(() => {
      this._user.next({});
      this._token.next('');
      this._isLoggedIn.next(false);
    });
  }

  private handleErr(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    console.error(errMsg);
    return observableThrowError(error.json() || error.toString());
  }
}
