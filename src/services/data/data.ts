import { Injectable } from '@angular/core';

import { environment as ENV } from "../../environments/environment";
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class DataProvider {

	items: any = [];
	items1: any = [];
	blogs: any = [];

  constructor(public http: HttpClient) {
    
  	this.http.get(ENV.BASE_URL + 'mentor/webservices/Hobby2/blog').subscribe(result=>{
        this.blogs = JSON.parse(result["_body"]);
        for(let i=0; i<this.blogs.length; i++)
        {
        	this.items1.push(this.blogs[i].title.toLowerCase());
   
        }
    });

  }

  filterItems(searchTerm){

  		this.items = [];
        for(let i=0; i<this.items1.length; i++)
        {
        	if(this.items1[i].includes(searchTerm.toLowerCase()))
        		this.items.push(this.blogs[i]);
        }
        return this.items;
 
    }

}
