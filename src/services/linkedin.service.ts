import { throwError as observableThrowError, BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { InAppBrowser, InAppBrowserEvent, InAppBrowserObject } from '@ionic-native/in-app-browser/ngx';

import { map, filter, catchError } from 'rxjs/operators';
import { environment as ENV } from '../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class LinkedinService {

  private callbackUrl: BehaviorSubject<string> = new BehaviorSubject('');
  private redirectUrl: BehaviorSubject<string> = new BehaviorSubject('');
  private jwtToken: BehaviorSubject<string> = new BehaviorSubject('');

  constructor(private http: HttpClient, private storage: Storage, private browser: InAppBrowser) {
  }

  login(): Observable<string> {
    this.getRedirectUrl()
      .subscribe(res => {
        console.log(res);
        this.redirectUrl.next(res);
      });

    this.redirectUrl.pipe(
      filter(url => !!url)
    )
      .subscribe(this.followRedirectUrl.bind(this));

    this.callbackUrl.pipe(
      filter(url => !!url)
    )
      .subscribe(this.getJwt.bind(this));

    return this.jwtToken.pipe(
      filter(token => !!token),
      map((token) => {
        return token;
      }))
  }

  private getJwt(url) {
    const callbackUrl = `${url}&from_client=true`;
    this.http.get(callbackUrl)
      .pipe(
        map((res: any) => res.json().token),
        catchError(this.handleErr)
      )
      .subscribe(token => this.jwtToken.next(token));
  }

  private async followRedirectUrl(url: string) {
    const browser = this.browser.create(url, '_blank', {toolbar: 'no', location: 'no'});
    browser.on('loadstart').pipe(
      filter(event => event.url.includes('api/linkedin/callback'))
    )
      .subscribe((event: InAppBrowserEvent) => {
        this.callbackUrl.next(event.url);
        browser.close();
      });
  }

  private getRedirectUrl() {
    return this.http.get(`${ENV.BASE_URL}/api/linkedin`)
      .pipe(
        map((res: any) => res.json().redirect),
        catchError(this.handleErr)
      );
  }

  private handleErr(error: Response | any) {
    let errMsg: string;
    console.log(error);
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    console.error(errMsg);
    return observableThrowError(error.json() || error.toString());
  }
}
