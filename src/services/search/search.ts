import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { environment as ENV } from "../../environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class SearchProvider {

	items: any = [];
	items1: any = [];
	users: any = [];
	options: any;

  constructor(public http: HttpClient, public storage: Storage) {

  	let headers = new HttpHeaders();
  	headers.append('Content-Type', 'application/json');
  	headers.append('Accept', 'application/json');
  	this.options = {headers: headers};

  }

  get_all_connections()
  {
  	this.storage.get('id').then(id=>{
  		let data = JSON.stringify({id: id});
	  	this.http.post(ENV.BASE_URL + 'webservices/Hobby2/connected_users',data,this.options).subscribe(result=>{
  			let dt = JSON.parse(result["_body"]);
  		    this.items1 = dt.myconnection;
  		});
  	});
  }

  filterItems(searchTerm)
  {
    this.items = [];
    this.get_all_connections();
    for(let i=0; i<this.items1.length; i++)
    {
      	if(this.items1[i].company_name.toLowerCase().includes(searchTerm.toLowerCase()) || this.items1[i].firstname.toLowerCase().includes(searchTerm.toLowerCase()) || this.items1[i].lastname.toLowerCase().includes(searchTerm.toLowerCase()))
      	{
       		this.items.push(this.items1[i]);
      	}
    }
    return this.items;
  }

  filterNames(searchTerm)
  {
    this.items = [];
    this.get_all_connections();
    for(let i=0; i<this.items1.length; i++)
    {
        if(this.items1[i].firstname.toLowerCase().includes(searchTerm.toLowerCase()) || this.items1[i].lastname.toLowerCase().includes(searchTerm.toLowerCase()))
        {
          this.items.push(this.items1[i]);
        }
    }
    return this.items;
  }

}
