// import {Injectable} from "@angular/core";
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/toPromise';
// import {BehaviorSubject} from "rxjs";
// import {Platform} from "@ionic/angular";
// import { HttpClient, HttpHeaders } from '@angular/common/http';

// const apiUrl = 'https://dev.elevather.com';

// @Injectable({
//   providedIn: 'root',
// })
// export class UserService {

//   private _user: BehaviorSubject<any> = new BehaviorSubject({});
//   private _isLogedIn: BehaviorSubject<boolean> = new BehaviorSubject(false);

//   public readonly user = this._user.asObservable();
//   public readonly isLogedIn = this._isLogedIn.asObservable();

//   constructor(private http: HttpClient) {
//   }

//   exchangeToken(token) {
//     const headers = new HttpHeaders();
//     headers.append('Authorization', token);
//     headers.append('Content-Type', 'application/json');
//     const reqOpts = {
//       headers,
//     };

//     this.http.get(`${apiUrl}/auth/me`, reqOpts)
//       .map(res => {
//         console.log(JSON.stringify(res));
//         return res.json()
//       })
//       .catch(this.handleErr)
//       .subscribe(res => {
//         this._isLogedIn.next(true);
//         this._user.next(res)
//       })
//   }

//   me() {
//     return this.user.filter(user => !!user);
//   }

//   private handleErr(error: Response | any) {
//     let errMsg: string;
//     if (error instanceof Response) {
//       const body = error.json() || '';
//       const err = body.error || JSON.stringify(body);
//       errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
//     } else {
//       errMsg = error.message ? error.message : error.toString();
//     }

//     console.error(JSON.stringify(error));
//     return errMsg;
//   }
// }
